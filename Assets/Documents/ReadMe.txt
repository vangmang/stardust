GameStateLibrary 
written by. Yoonwoo Lee

last modification date 2017.08.22

* namespaces and methods *

- GameStateLibrary.StateMng -
* StateMng : Monobehaviour, IStateMng
    - Fields 
	1) currentState
	   현재 상태
	2) recentState
 	   이전 상태
	3) stateList
	   유니티의 인스펙터에서 보여지는 상태.
	   이 리스트에 유저가 정의한 상태들을 추가한다.
	4) subStateList
    	   현재 상태 외에 추가해서 돌아가는 상태 리스트
	   (아래 부분에서 보충 설명)
    - protected Methods
	1) InitalizeState() 
	   상태매니저의 상태를 초기화해주는 메소드.
	   매니저가 상태를 정의할 때 열거자로 선언할 것을 강제한다.	

	2) StateEnter(Enum E_STATE, params object[] o_Params)
	   상태 진입 메소드.
	   매니저가 처음 상태로 진입할 때 호출한다.

	3) Execute()
	   업데이트 메소드.
	   이 메소드가 호출되지 않으면 State들이 업데이트 되지 않는다.

    - public Methods
	1) ChangeState(Enum E_STATE, params object[] o_Params)
	   현재 상태에서 다른 상태로 상태를 바꿀때 사용하는 메소드.
	   넘겨줘야 하는 인자가 존재할 때 o_Params를 사용한다.

        2) ChangeState(Enum E_STATE, bool isExist, params object[] o_Params)
	   전이할 상태가 현재 상태와 같다면 아무런 일도 하지 않는 메소드.

	3) SubState(Enum E_STATE, bool IsOverlapped, params object[] o_Params)
	   현재 상태에 또 다른 상태를 추가할 때 사용하는 메소드.
	   넘겨줘야 하는 인자가 존재할 때 o_Params를 사용한다.
           IsOverlapped가 true라면 상태 추가 후, 중복으로 추가 가능하다.

	4) RemoveState(Enum E_STATE)
	   선택된 상태를 상태매니저의 리스트에서 삭제하는 메소드.
           선택된 모든 상태를 상태리스트에서 삭제한다.

	5) RemoveAllState()
	   추가된 상태를 상태매니저의 추가 상태 리스트에서 모두 제거한다.

- GameStateLibrary.State -
* State : Monobehaviour, IState
    - public abstract Methods

	* GetState *
	- 상태에 해당하는 매니저의 열거자 - 
	- 상태매니저에서 상태를 열거자로 지정할 것을 강제하는 역할 -

	1) Enter(params object[] o_Params)
	   상태가 호출 되었을 때 한 번만 호출되는 메소드
	   넘겨받은 인자는 o_Params를 사용하여 받는다.

	2) Execute()
	   상태의 Update메소드
	
	3) Exit()
	   상태를 빠져나갈 때 한 번만 호출되는 메소드

a) Instruction
   1) 상태매니저를 정의한다.
	- 열거자로 상태를 우선 정의한다.
	- Awake()에 InitalizeState를 호출한다.
	- Start()에 StateEnter를 호출한다.
	- (선택) Update()에 Execute를 호출한다. -> 호출하지 않으면 상태가 들어와도 Update되지 않음.

   2) 인스펙터에 상태 매니저를 붙인다. 비어있는 상태매니저의 stateList와 subStateList를 확인할 수 있다.
      subStateList와 current, recent State 는 건들지 않는다. 

   3) 상태매니저에 작성된 열거자에 맞게 각 상태를 정의한다.
	- Enter, Execute, Exit는 추상 메소드임으로 무조건 정의한다.

   4) 상태를 전부 정의 한 후, 상태매니저 인스펙터에 상태들을 넣는다. (attach)
      상태매니저의 열거자 순서에 맞게 상태를 stateList에 넣는다.
      굳이 열거자 순서에 맞출 필요는 없지만 보기 좋기 때문.

   5) 완료.

b) about this stateMachine
 - 기존의 FSM은 단 하나의 상태만을 가져야 하는게 정석이다.
   하지만 이 규칙을 따른다면 스크립트가 정말 어마어마하게 많아진다.
   예를 들어서 이동을 하면서 공격을 해야 한다면 '이동 하면서 공격'하는 상태를 만들어야한다.
   이런 조합들은 무수히 많은데, 이를 전부 스크립트로 만든 다면 상당한 자원낭비가 아닐 수 없다.
   이를 방지하기 위해 subStateList와 SubState라는 놈을 추가했는데,
   SubState는 현재 상태에 또 다른 상태를 추가해주는 메소드이다.
   기존 FSM의 규칙에 많이 어긋나지만, 상당히 편해지는 걸 체감할 수 있다.
   기본적인 상태만 만들어 놓고, 우린 그 상태들을 추가해서 조화시킬 수 있다.
   이동을 하면서 공격을 해야 한다면, 이동 중인 상태에서 공격하는 상태를 추가하면 쉽게 구현 가능하다. 
   subStateList에 SubState를 이용해 상태를 추가하여 구현 가능하다.

   (교수님 께서는 잘못 사용하면 상당히 복잡해질 수 있을 것 같다고 말씀하셨지만 
    지금까지 사용했을 때에는 편리하고 간결했으므로 아직 주의할 점은 딱히 모르겠다.)