﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

[AddComponentMenu("Video/Subtitles")]
public sealed class SubtitlesMng : MonoBehaviour {
    
    private SubtitlesMng() { }

    private static SubtitlesMng instance = null;
    public static SubtitlesMng Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(SubtitlesMng)) as SubtitlesMng;
            return instance;
        }
    }

    public Text TextDisplay;
    [SerializeField]
    private TextAsset textAsset;
    private string[] texts;
    [System.NonSerialized]
    public bool isSubtitlesOn = true;
    public TextAsset SetTextAsset { set { textAsset = value; } }

    private int length;
    private int count;

	// Use this for initialization
	void Start () {
        texts = textAsset.text.Split(';');
        length = texts.Length;
        StartCoroutine(display());
	}
    //TEST
    IEnumerator display()
    {
        while(count < length)
        {
            SubtitleDisplayer(2f);
            yield return new WaitForSeconds(2f);
        }
    }

    public void SubtitleDisplayer(float displayingTime)
    {
        IEnumerator display = InvokeSubtitleDisplayer(displayingTime);
        StartCoroutine(display);
    }

    private IEnumerator InvokeSubtitleDisplayer(float displayingTime)
    {
        TextDisplay.text = texts[count];
        count++;
        yield return new WaitForSeconds(displayingTime);       
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
