﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;
using System.Text;

public enum E_RESOURCES
{
    E_1_LOADINGSCENE = 0,
    E_2_MENUSCENE = 1,
    E_3_GAMESCENE = 2,
    E_MAX = 3,
}

// 만들어야 할 매니저들.
// 1. 프리팹 매니저
// 2. 로딩매니저

// 완료한 매니저들
// 1. 프리팹 매니저 완료
// 2. 로딩 매니저는 직접 하면서 배워야 할 듯. 아직 개념이  안잡힘

/// <summary>
/// 게임에 사용할 모든 매니저 스크립트.
/// </summary>
public class Mng : MonoBehaviour
{
    public string[] s_CameraLayer =
    {
        "InGame",
        "InGameUI"
    };


    private string[] s_SceneTitle =
    {
        "1_LoadingScene",
        "2_MenuScene",
        "3_GameScene"
    };

    private static Mng m_Instance = null;


    protected Mng() { }

    // Mng 싱글톤
    public static Mng Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType(typeof(Mng)) as Mng;

                if (m_Instance == null)
                {
                    return null;
                }
            }

            return m_Instance;
        }
    }

    public static bool IsGameStarted;

    // 게임씬에서 불러올 스테이지 정보
    public struct _StageInfo_
    {
        public string StageName;
        public int Level;
        public Vector3 StageDirection;
        public float StageDistance;
    }

    public _StageInfo_ StageInfo;

    ////////////////////////////////////////////////////////////////////////////
    // 로딩 관련 필드

    /// <summary>
    /// 로딩할때 코루틴에 넘겨줄 enum. 기본적으로 게임씬으로 저장되어있음
    /// </summary>
    public E_RESOURCES Scene = E_RESOURCES.E_3_GAMESCENE;

    /// <summary>
    /// 현재 로딩 상황을 알려줄 필드
    /// </summary>
    public float fProgress = 0f;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    //프리팹 매니저 부분
    ////////////////////////////////////////////////////////////////////////////

    // 프리팹생성 메소드
    /// <summary>
    /// 문자열로 찾은 후 프리팹 생성
    /// </summary>
    /// <param name="s_Parent"> 프리팹을 어디에 생성할건지의 여부</param>
    /// <param name="s_Prefab"> 어떤 프리팹을 생성할건지의 여부 </param>
    /// <param name="Position"> 생성할 프리팹의 포지션값 </param>
    /// <param name="Rotation"> 생성할 프리팹의 로테이션값 </param>
    /// <param name="s_PrefabName"> 생성할 프리팹의 이름 </param>
    public GameObject CreatePrefab(string s_Parent,
                             string s_Prefab,
                             E_RESOURCES E_L_SCENE,
                             Vector3 Position,
                             Quaternion Rotation,
                             string s_PrefabName = "Prefab(Clone)")
    {
        StringBuilder PrefabObj = new StringBuilder(s_SceneTitle[(int)E_L_SCENE]).Append("/Prefabs/").Append(s_Prefab);

        // 부모 오브젝트를 먼저 찾는다.
        GameObject parent = GameObject.Find(s_Parent);

        // 리소스 테이블에서 생성할 프리팹을 찾는다
        GameObject Obj = Resources.Load(PrefabObj.ToString()) as GameObject;

        // 찾은 프리팹을 인스턴시에이트 해준다.
        GameObject CreatedPrefab = Instantiate(Obj, Position, Rotation) as GameObject;

        // 생성한 프리팹을 부모의 자식으로 넣어준다.
        CreatedPrefab.transform.SetParent(parent.transform);
        CreatedPrefab.name = s_PrefabName;

        return CreatedPrefab;
    }

    /// <summary>
    /// 게임오브젝트로 찾은 후 프리팹 생성
    /// </summary>
    /// <param name="g_Parent"> 프리팹을 어디에 생성할건지의 여부 </param>
    /// <param name="g_Prefab"> 어떤 프리팹을 생성할건지의 여부 </param>
    /// <param name="Position"> 생성할 프리팹의 포지션값 </param>
    /// <param name="Rotation"> 생성할 프리팹의 로테이션값 </param>
    /// <param name="s_PrefabName"> 생성할 프리팹의 이름 </param>
    public GameObject CreatePrefab(GameObject g_Parent,
                             GameObject g_Prefab,
                             E_RESOURCES E_L_SCENE,
                             Vector3 Position,
                             Quaternion Rotation,
                             string s_PrefabName = "Prefab(Clone)")
    {
        StringBuilder PrefabObj = new StringBuilder(s_SceneTitle[(int)E_L_SCENE]).Append("/Prefabs/").Append(g_Parent.name);

        GameObject Obj = Resources.Load(PrefabObj.ToString()) as GameObject;
        GameObject CreatedPrefab = Instantiate(Obj, Position, Rotation) as GameObject;
        CreatedPrefab.transform.SetParent(g_Parent.transform);

        CreatedPrefab.name = s_PrefabName;
        return CreatedPrefab;
    }

    /// <summary>
    /// 트랜스폼으로 찾은 후 프리팹 생성
    /// </summary>
    /// <param name="g_Parent"> 프리팹을 어디에 생성할건지의 여부 </param>
    /// <param name="g_Prefab"> 어떤 프리팹을 생성할건지의 여부 </param>
    /// <param name="Position"> 생성할 프리팹의 포지션값 </param>
    /// <param name="Rotation"> 생성할 프리팹의 로테이션값 </param>
    /// <param name="s_PrefabName"> 생성할 프리팹의 이름 </param>
    public GameObject CreatePrefab(Transform g_Parent,
                             string s_Prefab,
                             E_RESOURCES E_L_SCENE,
                             Vector3 Position,
                             Quaternion Rotation,
                             string s_PrefabName = "Prefab(Clone)")
    {
        StringBuilder PrefabObj = new StringBuilder(s_SceneTitle[(int)E_L_SCENE]).Append("/Prefabs/").Append(s_Prefab);

        GameObject Obj = Resources.Load(PrefabObj.ToString()) as GameObject;
        GameObject CreatedPrefab = Instantiate(Obj, Position, Rotation) as GameObject;
        CreatedPrefab.transform.SetParent(g_Parent.transform);

        CreatedPrefab.name = s_PrefabName;
        return CreatedPrefab;
    }

    /// <summary>
    /// 월드에 프리팹 생성
    /// </summary>
    /// <param name="s_Prefab"> 어떤 프리팹을 생성할건지의 여부 </param>
    /// <param name="Position"> 생성할 프리팹의 포지션값 </param>
    /// <param name="Rotation"> 생성할 프리팹의 로테이션값 </param>
    /// <param name="s_PrefabName"> 생성할 프리팹의 이름 </param>
    public GameObject CreatePrefab(string s_Prefab,
                             Vector3 Position,
                             E_RESOURCES E_L_SCENE,
                             Quaternion Rotation,
                             string s_PrefabName = "Prefab(Clone)")
    {
        StringBuilder PrefabObj = new StringBuilder(s_SceneTitle[(int)E_L_SCENE]).Append("/Prefabs/").Append(s_Prefab);

        GameObject Obj = Resources.Load(PrefabObj.ToString()) as GameObject;
        GameObject CreatedPrefab = Instantiate(Obj, Position, Rotation) as GameObject;

        CreatedPrefab.name = s_PrefabName;
        return CreatedPrefab;
    }

    ////////////////////////////////////////////////////////////////////////////
    //프리팹 매니저 부분


    //로딩 매니저 부분
    ////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// 로딩중 호출되는 코루틴 메소드
    /// </summary>
    /// <returns></returns>
    private IEnumerator SceneLoading(int Scene)
    {
        AsyncOperation Async = SceneManager.LoadSceneAsync(Scene);

        while (!Async.isDone)
        {
            Debug.Log("Loading");
            fProgress = Async.progress * 100.0f;
            yield return true;
        }
    }

    /// <summary>
    /// 씬 전환 메소드
    /// </summary>
    /// <param name="Scene"> 씬 인덱스 </param>
    public void LoadLevel(int Scene)
    {
        //SceneManager.LoadScene();
        StartCoroutine(SceneLoading(Scene));
    }

    ////////////////////////////////////////////////////////////////////////////
    //로딩 매니저 부분
}
