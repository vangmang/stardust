﻿using System;
using UnityAdsLibrary;
using UnityEngine;

public class StarDustAdsHelper : UnityAdsHelper
{
    public delegate void _ShowVideo();
    public static _ShowVideo ShowViedo;

    void Awake()
    {
        ShowViedo = ShowRewardedAd;
    }

    protected override void Failed()
    {
        
    }

    protected override void Finished()
    {
    }

    protected override void Skipped()
    {
    }
}
