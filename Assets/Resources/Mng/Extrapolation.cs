﻿using UnityEngine;

public struct _Extrapolation_ {
    public static float Extrapolation(Vector2 a, Vector2 b, float t)
    {
        return a.y + (t - a.x) / (b.x - a.x) * (b.y - a.y);
    }
}
