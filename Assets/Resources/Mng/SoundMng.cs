﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public  sealed class SoundMng : MonoBehaviour {
    private SoundMng() { }

    private static SoundMng instance = null;
    public static SoundMng Instance
    {
        get
        {
            if(null == instance)
                instance = FindObjectOfType(typeof(SoundMng)) as SoundMng;            
            return instance;
        }
    }

    private AudioSource[] AudioArr;

    private IEnumerable<AudioSource> BGMSounds;
    private IEnumerable<AudioSource> SFXSounds;
    
    public static bool isSFX_On;
    public static bool isBGM_On;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void InitSounds()
    {
        AudioArr = FindObjectsOfType<AudioSource>();

        BGMSounds =
            from audio in AudioArr
            where audio.clip.name.Contains("[BGM]")
            select audio;
        SFXSounds =
            from audio in AudioArr
            where audio.clip.name.Contains("[SFX]")
            select audio;
    }

    public void BGM_Enable(bool enable)
    {
        foreach (AudioSource audio in BGMSounds)
            audio.enabled = enable;
    }

    public void SFX_Enable(bool enable)
    {
        foreach (AudioSource audio in SFXSounds)
            audio.enabled = enable;
    }

    public void SetBGM_Volume(float volume)
    {
        foreach (AudioSource audio in BGMSounds)
            audio.volume = volume;
    }
}
