﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    private CameraFollow() { }

    public Transform Target;
    public float Speed = 5f;

	// Update is called once per frame
	void LateUpdate () {
        Vector3 wantedPos = Target.position;
        //transform.position = Vector3.Lerp(transform.position, wantedPos, Speed * Time.deltaTime);
        transform.localPosition = wantedPos;
    }

    void OnDestroy()
    {

    }
}
