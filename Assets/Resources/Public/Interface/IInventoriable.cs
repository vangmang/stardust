﻿public interface IInventoriable {
    float GetInventoryVolume { get; }
    float GetInventoryWeight { get; }
}

public interface IInventorialbe<T> : IInventoriable
    where T : class
{
    T ModuleInvenInstance { get; }
}
