﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Chapters : MonoBehaviour {
    [SerializeField]
    private List<AudioSource> dialogs;
    [SerializeField]
    protected SubtitlesMng subtitleMng;

    protected IEnumerable<AudioSource> Dialogs
    {
        get
        {
            return dialogs;
        }
    }

    protected virtual IEnumerator ActivateDialogs()
    {
        foreach (AudioSource audio in Dialogs)
        {
            audio.Play();
            subtitleMng.SubtitleDisplayer(audio.clip.length);
            yield return new WaitForSeconds(audio.clip.length + 0.18f);
        }
    }

}
