﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEditor;

internal class CreateCanvas : EditorWindow
{
    private GameObject cameraObj;
    private GameObject canvasObj;
    private Camera camera;
    private Canvas canvas;

    /// <summary>
    /// 카메라 사이즈 열거자
    /// </summary>
    private enum CameraSize
    {
        _720_1280 = 0,      // 9 : 16
        _768_1280 = 1,      // 3 : 5
        _800_1280 = 2,      // 10 : 16
        _1080_1920 = 3,     // 9 : 16
        _1200_1920 = 4,     // 10 : 16
        _1242_2280 = 5,     // 9 : 16
        _1440_2560 = 6,     // 9 : 16            
    }

    // 오디오 리스너 추가 여부
    private bool isAudioListenerAttached = true;
    // 플레어 레이어 추가 여부
    private bool isFlareLayerAttached = false;
    // 카메라 프로젝션 설정 여부
    private bool isOrthographic = true;
    // 카메라 사이즈 디폴트 설정
    private CameraSize cameraSize = CameraSize._720_1280;
    // 카메라 클리어 플래그 설정
    private CameraClearFlags cameraClearFlags = CameraClearFlags.Depth;
    // 카메라 사이즈별 테이블
    private static Dictionary<CameraSize, float> sizeTable = new Dictionary<CameraSize, float>();

    [MenuItem("Setting/Create Canvas")]
    public static void showWindow()
    {
        init();
        GetWindow(typeof(CreateCanvas)).titleContent = new GUIContent("Canvas Maker");
    }

    void OnGUI()
    {
        cameraSize = (CameraSize)EditorGUILayout.EnumPopup("cameraSize", cameraSize);
        cameraClearFlags = (CameraClearFlags)EditorGUILayout.EnumPopup("clearFlags", cameraClearFlags);
        isOrthographic = EditorGUILayout.Toggle("isOrthographic", isOrthographic);
        isAudioListenerAttached = EditorGUILayout.Toggle("isAudioListenerAttached", isAudioListenerAttached);
        isFlareLayerAttached = EditorGUILayout.Toggle("isFlareLayerAttached", isFlareLayerAttached);

        if (GUILayout.Button("Reset", GUILayout.Width(150f)))
            resetButton();
        if (GUILayout.Button("Create Canvas", GUILayout.Width(150f)))
            createCanvas();
    }

    private static void init()
    {
        if (sizeTable.Count <= 0)
        {
            sizeTable.Add(CameraSize._720_1280, 640f);
            sizeTable.Add(CameraSize._768_1280, 640f);
            sizeTable.Add(CameraSize._800_1280, 640f);
            sizeTable.Add(CameraSize._1080_1920, 960f);
            sizeTable.Add(CameraSize._1200_1920, 960f);
            sizeTable.Add(CameraSize._1242_2280, 1140f);
            sizeTable.Add(CameraSize._1440_2560, 1280f);
        }
    }

    private void resetButton()
    {
        cameraSize = CameraSize._720_1280;
        cameraClearFlags = CameraClearFlags.Depth;
        isOrthographic = true;
        isAudioListenerAttached = true;
        isFlareLayerAttached = false;
    }

    private void createCanvas()
    {
        #region Camera
        cameraObj = new GameObject();
        cameraObj.name = "Main Camera";
        camera = cameraObj.AddComponent<Camera>();
        if (isAudioListenerAttached)
            cameraObj.AddComponent<AudioListener>();
        if (isFlareLayerAttached)
            cameraObj.AddComponent<FlareLayer>();
        camera.clearFlags = cameraClearFlags;
        camera.orthographic = isOrthographic;
        if (camera.orthographic)
            camera.orthographicSize = sizeTable[cameraSize];
        #endregion

        #region Canvas
        canvasObj = new GameObject();
        canvasObj.name = "Canvas";
        canvas = canvasObj.AddComponent<Canvas>();
        canvas.renderMode = RenderMode.WorldSpace;
        canvas.worldCamera = camera;
        canvasObj.AddComponent<GraphicRaycaster>();
        #endregion
    }

    void OnDestroy()
    {
    }

}
#endif