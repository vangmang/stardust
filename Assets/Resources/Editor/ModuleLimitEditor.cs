﻿using UnityEditor;

/// <summary>
/// 모듈 갯수에 대한 오브젝트 생성을 제한해주는 스크립트
/// </summary>
[CustomEditor(typeof(ModuleLimit))]
public class ModuleLimitEditor : Editor {
    
    void OnEnable()
    {
        EditorApplication.update += Execute;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("GetShipStatMng"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("OffenseModulesContainer"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("DefenseModulesContainer"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("UtilityModulesContainer"));
        serializedObject.ApplyModifiedProperties();
    }

    void Execute()
    {
        ModuleLimit moduleLimit = (ModuleLimit)target;
        if (moduleLimit.GetShipStatMng)
        {
            if (moduleLimit.OffenseModulesContainer)
                if (moduleLimit.GetShipStatMng.GetOffensiveModuleCount < moduleLimit.OffenseModulesContainer.childCount)
                    DestroyImmediate(moduleLimit.OffenseModulesContainer.GetChild(moduleLimit.OffenseModulesContainer.childCount - 1).gameObject);

            if (moduleLimit.DefenseModulesContainer)
                if (moduleLimit.GetShipStatMng.GetDefensiveModuleCount < moduleLimit.DefenseModulesContainer.childCount)
                    DestroyImmediate(moduleLimit.DefenseModulesContainer.GetChild(moduleLimit.DefenseModulesContainer.childCount - 1).gameObject);

            if (moduleLimit.UtilityModulesContainer)
                if (moduleLimit.GetShipStatMng.GetUtilityModuleCount < moduleLimit.UtilityModulesContainer.childCount)
                    DestroyImmediate(moduleLimit.UtilityModulesContainer.GetChild(moduleLimit.UtilityModulesContainer.childCount - 1).gameObject);
        }
    }
}
