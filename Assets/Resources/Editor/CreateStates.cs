﻿#if UNITY_EDITOR
using System.Collections.Generic;
using GameStateLibrary.StateMng;
using GameStateLibrary.State;
using UnityEngine;
using UnityEditor;
using System.Text;
using System.Linq;

internal class CreateStates : EditorWindow
{
    public enum Scene
    {
        _LoadingScene = 1,
        _MenuScene = 2,
        _GameScene = 3,
    }

    private GameObject ParentObj;
    private GameObject StateMngObj;

    private string parentName = "StateTransformName";
    private string StateName = string.Empty;
    private bool isParentAvailable = true;
    private Scene currentScene = Scene._GameScene;
    
    [MenuItem("Setting/Create State")]
    public static void showWindow()
    {
        GetWindow(typeof(CreateStates)).titleContent = new GUIContent("State Maker");
    }

    void OnGUI()
    {
        isParentAvailable = EditorGUILayout.Toggle("IsParentAvailable", isParentAvailable);
        currentScene = (Scene)EditorGUILayout.EnumPopup("Path", currentScene);
        if (isParentAvailable)
            parentName = EditorGUILayout.TextField("StateTransformName", parentName);
        StateName = EditorGUILayout.TextField("StateName", StateName);
        
        if (GUILayout.Button("Reset", GUILayout.Width(150f)))
            resetButton();
        if (GUILayout.Button("Create", GUILayout.Width(150f)))
            createButton();
    }

    private void resetButton()
    {
        isParentAvailable = true;
        parentName = "StateTransformName";
    }

    private void createButton()
    {
        GameObject StateMngObj = null;
        if (StateName == string.Empty)
        {
            Debug.LogWarning("You must enter the StateName");
            return;
        }
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.Append((int)currentScene).Append(currentScene).Append("/Scripts/").Append(StateName).Append("StateMng");
            Object[] LoadedObjects = Resources.LoadAll(sb.ToString());
            IEnumerable<Object> objects =
                from obj in LoadedObjects
                where obj.name.Contains(StateName) && 
                ((obj as MonoScript).GetClass().IsSubclassOf(typeof(State)) ||
                (obj as MonoScript).GetClass().IsSubclassOf(typeof(StateMng)))
                select obj;

            StateMngObj = new GameObject("State");
            StateMngObj.AddComponent((objects.FirstOrDefault(obj => ((obj as MonoScript).GetClass().IsSubclassOf(typeof(StateMng)))) as MonoScript).GetClass());

            foreach(Object O in objects)
                if((O as MonoScript).GetClass().IsSubclassOf(typeof(State)))
                    StateMngObj.AddComponent((O as MonoScript).GetClass());

            //----------------------------상태가 성공적으로 만들어짐----------------------------//
            if (isParentAvailable)
            {
                GameObject ParentObj = new GameObject(parentName);
                StateMngObj.transform.parent = ParentObj.transform;
            }
            Debug.Log("Creating State is successfully done: " + sb.ToString());
            LoadedObjects = null;
            objects = null;
        }
        catch (System.NullReferenceException e)
        {
            DestroyImmediate(StateMngObj);
            StringBuilder sb = new StringBuilder();
            sb.Append("There is no StateName: [").Append(StateName).Append("]");
            Debug.Log(sb.ToString());
        }
    }

    void OnDestroy()
    {

    }
}

#endif
