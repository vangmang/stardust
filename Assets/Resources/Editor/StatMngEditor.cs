﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// 함선 스탯 제한 스크립트
/// </summary>
[CustomEditor(typeof(ShipStatMng))]
public class StatMngEditor : Editor {

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        ShipStatMng shipStatMng = (ShipStatMng)target;
        EditorGUILayout.PropertyField(serializedObject.FindProperty("ShipLevel"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Modules"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("ShipStat"), true);
        shipStatMng.Modules.OffensiveModuleCount = Mathf.Clamp(shipStatMng.Modules.OffensiveModuleCount, 0, ModuleBase.LimitModuleCount);
        shipStatMng.Modules.DefensiveModuleCount = Mathf.Clamp(shipStatMng.Modules.DefensiveModuleCount, 0, ModuleBase.LimitModuleCount);
        shipStatMng.Modules.UtilityModuleCount = Mathf.Clamp(shipStatMng.Modules.UtilityModuleCount, 0, ModuleBase.LimitModuleCount);
        serializedObject.ApplyModifiedProperties();
    }
}
