﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class GameIdleState : State<GameStateMng>
{

    public override Enum GetState
    {
        get
        {
            return GameStateMng.GameState.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
