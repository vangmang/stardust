﻿using System;
using GameStateLibrary.State;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;

public class GameExitState : State<GameStateMng>
{

    public override Enum GetState
    {
        get
        {
            return GameStateMng.GameState.exit;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        //TEST
        instance.MiniMap.gameObject.SetActive(false);
        instance.GetCharShipStateMng.ShipVolumeCollider.enabled = false;
        StartCoroutine(FadeIn());
    }

    private IEnumerator FadeIn()
    {
        instance.BackgroundCol.gameObject.SetActive(true);
        float t = 0f;
        float a = 0f;
        float volume = 1f;

        yield return new WaitForFixedUpdate();
        while (t <= 1f)
        {
            t += Time.deltaTime;
            a = Mathf.Lerp(0f, 1f, t);
            volume = Mathf.Lerp(1f, 0f, t);
            var color = instance.BackgroundCol.color;
            color = new Color(0f, 0f, 0f, a);
            SoundMng.Instance.SetBGM_Volume(volume);
            instance.BackgroundCol.color = color;
            yield return null;
        }
        instance.MiniMap.gameObject.SetActive(false);
        if(Mng.Instance)
            DestroyImmediate(Mng.Instance.gameObject);
        SceneManager.LoadScene(0);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
