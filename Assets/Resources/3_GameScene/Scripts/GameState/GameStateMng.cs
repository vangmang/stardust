﻿using GameStateLibrary.StateMng;
using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class GameStateMng : SingletonStateMng<GameStateMng> {
    private GameStateMng() { }

    public enum GameState
    {
        enter,
        idle,
        pause,
        exit,
    }

    public struct _StageInfo_
    {
        public int StageLevel;
        public string StageName;
        public string StageTransformName;
        public Vector3 StageDirection;
        public float StageDistance;
    }

    public _StageInfo_ StageInfo;
    private int stageEnemyCount;
    public int GetStageEnemyCount { get { return stageEnemyCount; } }
    public void TickStageEnemyCount()
    {
        stageEnemyCount++;
    }
    public void ReduceStageEnemyCount()
    {
        stageEnemyCount--;

        if (stageEnemyCount <= 0)
        {
            StarDustAdsHelper.ShowViedo();
            ChangeState(GameState.exit);
        }
    }

    [SerializeField]
    private CharShipStateMng charShipStateMng;

    public CharShipStateMng GetCharShipStateMng { get { return charShipStateMng; } }

    public Camera MainCamera;
    public Camera UICamera;
    public RawImage Background;
    public Image BackgroundCol;
    public Text StageText;
    public Transform MiniMap;
    public Vector3 StageLocation
    {
        get { return StageInfo.StageDirection * StageInfo.StageDistance * 200f; }
    }

    void Awake()
    {
        InitializeState();
        StageInfo.StageLevel = Mng.Instance.StageInfo.Level;
        StageInfo.StageName = Mng.Instance.StageInfo.StageName;
        StageText.text = Mng.Instance.StageInfo.StageName;
        StageInfo.StageDirection = Mng.Instance.StageInfo.StageDirection;
        StageInfo.StageDistance = Mng.Instance.StageInfo.StageDistance;
        StartCoroutine(GenerateStage());
        charShipStateMng.ShipInfo.ShipTransform.localPosition = StageLocation;
    }


    public IEnumerator GenerateStage()
    {
        string parent = "MainCanvas/World";
        StageInfo.StageTransformName = "[Stage: " + StageInfo.StageName + "]";
        Transform stageTransform = Mng.Instance.CreatePrefab(parent, "Stages/stage" + StageInfo.StageLevel,
            E_RESOURCES.E_3_GAMESCENE, Vector3.zero, Quaternion.identity, StageInfo.StageTransformName).transform;
        yield return new WaitForFixedUpdate();
        SoundMng.Instance.InitSounds();
        SoundMng.Instance.BGM_Enable(SoundMng.isBGM_On);
        SoundMng.Instance.SFX_Enable(SoundMng.isSFX_On);
        yield return new WaitForFixedUpdate();
        ChangeState(GameState.idle);
        stageTransform.SetAsFirstSibling();        
    }


    // Use this for initialization
    void Start () {
        StateEnter(GameState.enter);
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(stageEnemyCount);
        Execute();
	}

    void OnDestroy()
    {
        DestroyInstance();
    }
}
