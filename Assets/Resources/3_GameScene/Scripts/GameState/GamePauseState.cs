﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class GamePauseState : State<GameStateMng>
{

    public override Enum GetState
    {
        get
        {
            return GameStateMng.GameState.pause;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        Time.timeScale = 0f;
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
        Time.timeScale = 1f;
    }
}
