﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

public class GameEnterState : State<GameStateMng>
{
    public override Enum GetState
    {
        get
        {
            return GameStateMng.GameState.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeOut()
    {
        float t = 0f;
        float a = 1f;
        yield return new WaitForFixedUpdate();
        while (t <= 1f)
        {
            t += Time.deltaTime;
            a = Mathf.Lerp(1f, 0f, t);
            var color = instance.BackgroundCol.color;
            color = new Color(0f, 0f, 0f, a);
            instance.BackgroundCol.color = color;
            yield return null;
        }
        instance.BackgroundCol.gameObject.SetActive(false);
        instance.MiniMap.gameObject.SetActive(true);
    }
}
