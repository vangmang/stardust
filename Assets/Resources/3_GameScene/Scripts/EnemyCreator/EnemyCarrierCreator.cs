﻿using System.Text;
using UnityEngine;

public sealed class EnemyCarrierCreator : MonoBehaviour {

    private string PrefabName;
    public Transform PositionAllocator;
    public int DroneCount;

    void Awake()
    {
        GameStateMng.Instance.TickStageEnemyCount();
        PositionAllocator.gameObject.SetActive(false);
        PrefabName = "Enemies/EnemyCarrier";
        EnemyCarrier enemyCarrier = 
            Mng.Instance.CreatePrefab(transform, PrefabName, E_RESOURCES.E_3_GAMESCENE, PositionAllocator.localPosition, PositionAllocator.localRotation, "EnemyCarrier").GetComponentInChildren<EnemyCarrier>();
        enemyCarrier.DroneCount = DroneCount;
        enemyCarrier.ShipInfo.ShipTransform.SetAsFirstSibling();
    }

    void OnDestroy()
    {

    }
}
