﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class EnemyTankCreator : MonoBehaviour {

    public WeaponType weaponType;

    private string PrefabName;
    public Transform PositionAllocator;
    public Transform EnemyProjectileContainer;

    // Use this for initialization
    void Awake()
    {
        GameStateMng.Instance.TickStageEnemyCount();

        StringBuilder sb = new StringBuilder("Enemies/EnemyTank");
        switch (weaponType)
        {
            case WeaponType.projectile:
                sb.Append("[Projectile]");
                break;
            case WeaponType.plasma:
                sb.Append("[Plasma]");
                break;
            case WeaponType.hyprid:
                sb.Append("[Hybrid]");
                break;
        }
        PrefabName = sb.ToString();
        PositionAllocator.gameObject.SetActive(false);
        EnemyTank enemyTank =
            Mng.Instance.CreatePrefab(transform, PrefabName, E_RESOURCES.E_3_GAMESCENE, PositionAllocator.localPosition, PositionAllocator.localRotation, PrefabName).GetComponentInChildren<EnemyTank>();
        enemyTank.ProjectileContainer = EnemyProjectileContainer;
    }

    void OnDestroy()
    {

    }
}
