﻿using UnityEngine;
using System.Text;
using System.Collections;

// 적들을 스테이지에 배치하면 알아서 적들을 생성해주는 스크립트
public sealed class EnemyAssaultCreator : MonoBehaviour {

    public WeaponType weaponType;

    private string PrefabName;
    public Transform PositionAllocator;
    public Transform EnemyProjectileContainer;

	// Use this for initialization
	void Awake() {
        GameStateMng.Instance.TickStageEnemyCount();

        StringBuilder sb = new StringBuilder("Enemies/EnemyAssault");
	    switch (weaponType)
        {
            case WeaponType.projectile:
                sb.Append("[Projectile]");
                break;
            case WeaponType.plasma:
                sb.Append("[Plasma]");
                break;
            case WeaponType.hyprid:
                sb.Append("[Hybrid]");
                break;
        }
        PrefabName = sb.ToString();
        PositionAllocator.gameObject.SetActive(false);
        EnemyAssault enemyAssault = 
            Mng.Instance.CreatePrefab(transform, PrefabName, E_RESOURCES.E_3_GAMESCENE, PositionAllocator.localPosition, PositionAllocator.localRotation, PrefabName).GetComponentInChildren<EnemyAssault>();
        enemyAssault.ProjectileContainer = EnemyProjectileContainer;
    }

    void OnDestroy()
    {

    }
}
