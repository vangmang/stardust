﻿
public abstract class UtilityModule<T, U> : ModuleBase, IUtilityModule<T, U>
    where T : class
    where U : class, IShip
{
    public abstract U ShipInstance { get; }
    public abstract UtilityType GetUtilityType { get; }
    public override ModuleType GetModuleType { get { return ModuleType.Utility; } }
    public abstract T ModuleInstance { get; }
}
