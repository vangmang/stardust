﻿using UnityEngine;

[AddComponentMenu("Module/DefensiveModule/LayerPlate")]
public class LayerPlate : DefensiveModule<LayerPlate, ShipBase> {

    public enum LayerPlate_State
    {
        enter,
        idle,
    }

    [SerializeField]
    private ShipBase shipBase;

    public override DefenseType GetDefenseType { get { return DefenseType.layerPlate; } }
    public override ShipBase ShipInstance { get { return shipBase; } }
    public override LayerPlate ModuleInstance { get { return this; } }

    void Awake()
    {
        InitializeState();
    }

    void Start()
    {
        StateEnter(LayerPlate_State.enter);
    }

    public override void Bonus()
    {
    }

    public override void Penalty()
    {
    }

    void OnDestroy()
    {

    }
}
