﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class LayerPlateEnterState : State<LayerPlate>
{

    public override Enum GetState
    {
        get
        {
            return LayerPlate.LayerPlate_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
