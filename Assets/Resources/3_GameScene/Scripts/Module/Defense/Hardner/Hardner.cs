﻿using UnityEngine;

[AddComponentMenu("Module/DefensiveModule/Hardner")]
public class Hardner : DefensiveModule<Hardner, ShipBase>
{

    public enum E_HARDNER_STATE
    {
        enter,
        idle,
        harden,
        overload
    }

    [SerializeField]
    private ShipBase shipBase;

    public override DefenseType GetDefenseType { get { return DefenseType.hardner; } }
    public override ShipBase ShipInstance { get { return shipBase; } }
    public override Hardner ModuleInstance { get { return this; } }
    void Awake()
    {
        InitializeState();
    }

    void Start()
    {
        StateEnter(E_HARDNER_STATE.enter);
    }

    void Update()
    {
        Execute();
    }

    public override void Bonus()
    {
    }

    public override void Penalty()
    {
    }

    void OnDestroy()
    {

    }
}
