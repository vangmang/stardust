﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class HardnerOverloadState : State<Hardner>
{

    public override Enum GetState
    {
        get
        {
            return Hardner.E_HARDNER_STATE.overload;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
