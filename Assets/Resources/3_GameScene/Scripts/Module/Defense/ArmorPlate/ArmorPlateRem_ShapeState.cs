﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class ArmorPlateRem_ShapeState : State<ArmorPlate> {

    public override Enum GetState
    {
        get
        {
            return ArmorPlate.E_ARMORPLATE_STATE.remember_shape;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
