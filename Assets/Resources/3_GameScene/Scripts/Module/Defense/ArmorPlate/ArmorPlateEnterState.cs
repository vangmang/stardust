﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class ArmorPlateEnterState : State<ArmorPlate> {

    public override Enum GetState
    {
        get
        {
            return ArmorPlate.E_ARMORPLATE_STATE.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
