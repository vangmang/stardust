﻿using System;
using UnityEngine;

[AddComponentMenu("Module/DefensiveModule/ArmorPlate")]
public class ArmorPlate : DefensiveModule<ArmorPlate, ShipBase>
{

    public enum E_ARMORPLATE_STATE
    {
        enter,
        idle,
        remember_shape
    }

    [SerializeField]
    private ShipBase shipBase;

    public override DefenseType GetDefenseType { get { return DefenseType.armorPlate; } }
    public override ShipBase ShipInstance { get { return shipBase; } }
    public override ArmorPlate ModuleInstance { get { return this; } }

    void Awake()
    {
        InitializeState();
    }

    void Start()
    {
        StateEnter(E_ARMORPLATE_STATE.enter);
    }

    void Update()
    {
        Execute();
    }

    public override void Bonus()
    {
    }

    public override void Penalty()
    {
    }

    void OnDestroy()
    {

    }
}
