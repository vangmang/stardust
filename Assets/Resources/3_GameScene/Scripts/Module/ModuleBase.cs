﻿using UnityEngine;
using GameStateLibrary.StateMng;
using UnityEngine.UI;

public abstract class ModuleBase : StateMng, IModule {

    public static readonly int LimitModuleCount = 8;
    public static readonly float ModuleRotSpeed = 14f;

    public Transform ModuleTransform;
    [SerializeField]
    private string ModuleName;
    [SerializeField]
    private string ModuleDescription;
    [SerializeField]
    private float CPU_Usage;
    [SerializeField]
    private float Power_Usage;
    [SerializeField]
    private float Volume;
    [SerializeField]
    private float Weight;
    [SerializeField]
    protected Image moduleImage;
    [SerializeField]
    protected float powerConsumption;

    public Image GetModuleImage { get { return moduleImage; } }

    public abstract ModuleType GetModuleType { get; }
    public string GetModuleName { get { return ModuleName; } }
    public string GetModuleDescription { get { return ModuleDescription; } }
    public float GetCPU_Usage { get { return CPU_Usage; } }
    public float GetPower_Usage { get { return Power_Usage; } }
    public float GetVolume { get { return Volume; } }
    public float GetWeight { get { return Weight; } }    
    public float GetPowerConsumption { get { return powerConsumption; } }
    public float GetInventoryVolume { get { return Volume; } }
    public float GetInventoryWeight { get { return Weight; } }

    public IModule ModuleInvenInstance { get { return this; } }

    public abstract void Bonus();
    public abstract void Penalty();
}
