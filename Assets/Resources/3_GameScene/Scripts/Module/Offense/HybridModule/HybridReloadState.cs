﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

public class HybridReloadState : State<HybridModule> {

    public override Enum GetState
    {
        get
        {
            return HybridModule.Hybrid_State.reload;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.isReloading = true;
        StartCoroutine(Reload());
    }

    private IEnumerator Reload()
    {
        yield return new WaitForSeconds(instance.GetReloadingSpeed);
        var state = ((IOffenseShip)instance.ShipInstance).IsShipEngaging ? HybridModule.Hybrid_State.charge : HybridModule.Hybrid_State.idle;
        instance.GetCurrentChargeCount = instance.GetMaxCharges;
        instance.isReloading = false;
        instance.ChangeState(state);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {

    }
}
