﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class HybridEnterState : State<HybridModule> {

    public override Enum GetState
    {
        get
        {
            return HybridModule.Hybrid_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.ChangeState(HybridModule.Hybrid_State.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
