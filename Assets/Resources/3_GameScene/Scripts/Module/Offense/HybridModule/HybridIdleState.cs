﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class HybridIdleState : State<HybridModule> {

    public override Enum GetState
    {
        get
        {
            return HybridModule.Hybrid_State.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {

    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
