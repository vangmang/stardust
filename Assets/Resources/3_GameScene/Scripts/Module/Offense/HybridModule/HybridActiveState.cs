﻿using System;
using GameStateLibrary.State;
using System.Collections;
using System.Linq;
using UnityEngine;

public class HybridActiveState : State<HybridModule> {


    public override Enum GetState
    {
        get
        {
            return HybridModule.Hybrid_State.active;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.isModuleActivated = true;
        StartCoroutine(active());
    }

    private IEnumerator active()
    {
        CameraEffect.DirectCameraAsExplosion(0.1f);
        instance.ModuleEffect.FireSound.Play();
        instance.GetCurrentChargeCount--;
        instance.GetCharges.ElementAtOrDefault(Mathf.Max(instance.GetCurrentChargeCount - 1, 0)).ChangeState(HybridCharge.HybridCharge_State.fire);
        yield return new WaitForSeconds(instance.GetCycleSpeed * 0.55f);
        var state = instance.GetCurrentChargeCount == 0 ? HybridModule.Hybrid_State.reload : HybridModule.Hybrid_State.charge;
        instance.isModuleActivated = false;
        instance.ChangeState(state);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
