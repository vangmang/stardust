﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

public class HybridChargeState : State<HybridModule>
{

    public override Enum GetState
    {
        get
        {
            return HybridModule.Hybrid_State.charge;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        StartCoroutine("Charging");
    }

    private IEnumerator Charging()
    {
        yield return new WaitUntil(() => !instance.isReloading && !instance.isModuleActivated);
        instance.ChargingSound.Play();
        yield return new WaitForSeconds(instance.GetCycleSpeed);
        if (!((IOffenseShip)instance.ShipInstance).IsShipEngaging)
            yield break;
        instance.ModuleEffect.ChargeEffect.brightness = 2f;
        yield return new WaitUntil(() => instance.IsAbleToActivate);
        instance.ChangeState(HybridModule.Hybrid_State.active);
    }


    public override void Execute()
    {
        if (!((IOffenseShip)instance.ShipInstance).IsShipEngaging)
            instance.ChangeState(HybridModule.Hybrid_State.idle);
        if (!instance.isReloading && !instance.isModuleActivated)
        {
            instance.ChargingSound.volume = Mathf.Lerp(instance.ChargingSound.volume, 1f, instance.ChargeSoundControlSpeed * Time.deltaTime);
            instance.ModuleEffect.ChargeEffect.brightness = Mathf.Lerp(instance.ModuleEffect.ChargeEffect.brightness, 1f, instance.ModuleEffect.EffectSpeed * Time.deltaTime);
        }
    }


    public override void Exit()
    {
        StopCoroutine("Charging");
        StartCoroutine(instance.ChargeSoundDisable());
        StartCoroutine(instance.ChargeEffectDisable());
    }
}
