﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[AddComponentMenu("Module/OffensiveModule/Hybrid")]
public class HybridModule : OffensiveModule<HybridModule, ShipBase>
{
    // 공격 / 속도 / 탄창 / 각도 / 거리 / 타겟팅
    //  8      5     5      2     10      2
    // 하이브리드
    // 보너스: 공격력, 거리
    // 페널티: 각도, 타겟팅

    public enum Hybrid_State
    {
        enter,
        idle,
        charge,
        active,
        reload
    }

    [SerializeField]
    private ShipBase shipBase;

    public bool isModuleActivated;
    public float ChargeSoundControlSpeed;

    [Serializable]
    public struct _ModuleEffect_
    {
        public AudioSource FireSound;
        public LensFlare ChargeEffect;
        [NonSerialized]
        public float EffectSpeed;
    }
    public _ModuleEffect_ ModuleEffect;

    private List<HybridCharge> hybridCharges = new List<HybridCharge>();
    public AudioSource ChargingSound;
    public override WeaponType GetWeaponType { get { return WeaponType.hyprid; } }
    public override ShipBase ShipInstance { get { return shipBase; } }


    public IEnumerable<HybridCharge> GetCharges
    {
        get
        {
            return hybridCharges;
        }
    }

    public override HybridModule ModuleInstance
    {
        get
        {
            return this;
        }
    }

    void Awake()
    {
        currentChargeCount = maxCharges;
        InitializeState();
    }

    void Start()
    {
        if (((IOffenseShip)shipBase).GetWeaponType == WeaponType.hyprid)
        {
            Bonus();
            Penalty();
        }

        IsModuleFittedOnChar = ShipInstance.GetType().Equals(typeof(CharShipStateMng));
        ModuleEffect.EffectSpeed = ChargingSound.clip.length * 0.1f;
        GenerateProjectiles();
        StateEnter(Hybrid_State.enter);
    }

    protected override void GenerateProjectiles()
    {
        StartCoroutine(InvokeGenerateProjectiles());
    }

    private IEnumerator InvokeGenerateProjectiles()
    {
        yield return new WaitForFixedUpdate();
        Transform container = ((IOffenseShip)shipBase).ProjectileContainer;
        StringBuilder sb = new StringBuilder();
        sb.Append("MainCanvas/World/");
        if (container.name.Contains("Enemy"))
            sb.Append(GameStateMng.Instance.StageInfo.StageTransformName).Append("/");
        sb.Append(container.parent.name).Append("/").Append(container.name).Append("/Hybrid");
        for (int i = 0; i < maxCharges; i++)
        {
            HybridCharge hybrid =
                Mng.Instance.CreatePrefab(sb.ToString(), "Items/Charges/HybridCharge", E_RESOURCES.E_3_GAMESCENE, Vector3.zero, Quaternion.identity).GetComponentInChildren<HybridCharge>();
            hybrid.setModuleInstance = this;
            hybridCharges.Add(hybrid);
        }
    }

    void Update()
    {
        var wantedRot = ShipInstance.ShipInfo.ShipTransform.localRotation;
        if (CharShipStateMng.GetInstance.targetedShip && IsModuleFittedOnChar)
        {
            CharShipStateMng CharShipInstance = (CharShipStateMng)ShipInstance;
            Vector3 target =
                CharShipInstance.targetedShip.ShipInfo.ShipTransform.localPosition;

            Vector3 targetDir = CharShipInstance.ShipInfo.ShipTransform.InverseTransformPoint(CharShipInstance.targetedShip.ShipInfo.ShipTransform.localPosition);
            float targetDeg = Mathf.Atan2(targetDir.x, targetDir.y) * Mathf.Rad2Deg;

            Vector3 direction = target - ModuleTransform.position;
            float deg = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;

            IsAbleToActivate = Mathf.Abs(targetDeg) < GetAttackableDeg;
            wantedRot = IsAbleToActivate ? Quaternion.Euler(0f, 0f, deg) : ShipInstance.ShipInfo.ShipTransform.localRotation;

            ModuleTransform.rotation =
                Quaternion.Lerp(ModuleTransform.rotation, wantedRot, ModuleRotSpeed * Time.deltaTime);
        }
        else 
        {
            IsAbleToActivate = true;
            if (!IsModuleFittedOnChar)
            {
                CharShipStateMng CharShipInstance = ShipInstance.GetGameStateMng.GetCharShipStateMng;
                Vector3 targetDir = ShipInstance.ShipInfo.ShipTransform.InverseTransformPoint(CharShipInstance.ShipInfo.ShipTransform.localPosition);
                float targetDeg = Mathf.Atan2(targetDir.x, targetDir.y) * Mathf.Rad2Deg;

                Vector3 target = CharShipInstance.ShipInfo.ShipTransform.localPosition;
                Vector3 direction = target - ModuleTransform.position;

                float deg = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
                IsAbleToActivate = Mathf.Abs(targetDeg) < GetAttackableDeg;
                if (IsAbleToActivate)
                    wantedRot = Quaternion.Euler(0f, 0f, deg);
            }
        }
        ModuleTransform.rotation =
                Quaternion.Slerp(ModuleTransform.rotation, wantedRot, ModuleRotSpeed * Time.deltaTime);
        Execute();
    }

    public IEnumerator ChargeSoundDisable()
    {
        yield return new WaitForFixedUpdate();
        float start = ChargingSound.volume;
        float t = 0f;
        while (t <= 1f)
        {
            t += ChargeSoundControlSpeed * 0.5f * Time.deltaTime;
            ChargingSound.volume = Mathf.Lerp(start, 0f, t);
            yield return null;
        }
    }
    public IEnumerator ChargeEffectDisable()
    {
        yield return new WaitForFixedUpdate();
        float start = ModuleEffect.ChargeEffect.brightness;
        float t = 0f;
        while (t <= 1f)
        {
            t += ModuleEffect.EffectSpeed * 5f * Time.deltaTime;
            ModuleEffect.ChargeEffect.brightness = Mathf.Lerp(start, 0f, t);
            yield return null;
        }
    }

    // 발사체 데미지 증가
    // 탄속 대폭 증가
    // 사정거리 대폭 증가
    public override void Bonus()
    {
        damage *= 1.33f;
        objectVelocity *= 2.5f;
        effectiveRange *= 5f;
    }

    // 모듈 공격 가능 각도 감소
    // 함선 최대 속력 감소
    // 함선 기동성 감소
    // 함선 타겟팅 스피드 감소
    // 함선 파워그리드 감소
    public override void Penalty()
    {
        attackableDeg *= 0.85f;
        shipBase.GetShipStat.SetMaxSpeed = shipBase.GetShipStat.GetMaxSpeed * 0.75f;
        shipBase.GetShipStat.SetMobility = shipBase.GetShipStat.GetMobility * 0.75f;
        shipBase.GetShipStat.SetTargetingSpeed = shipBase.GetShipStat.GetTargetingSpeed * 0.925f;
        shipBase.GetShipStat.SetPower = shipBase.GetShipStat.GetPower * 0.875f;
    }

    void OnDestroy()
    {

    }
}
