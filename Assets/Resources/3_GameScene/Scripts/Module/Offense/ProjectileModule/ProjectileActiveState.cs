﻿using System;
using System.Collections;
using GameStateLibrary.State;
using UnityEngine;
using System.Linq;

// 프로젝타일 [재장전 무]
public class ProjectileActiveState : State<ProjectileModule> {

    public override Enum GetState
    {
        get
        {
            return ProjectileModule.Projectile_State.active;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.IsAbleToActivate = instance.IsModuleFittedOnChar;
        StartCoroutine(active());
    }

    private IEnumerator active()
    {
        yield return new WaitForFixedUpdate();
        while (((IOffenseShip)instance.ShipInstance).IsShipEngaging)
        {
            float time = 0f;
            while (time <= instance.GetCycleSpeed)
            {
                time += Time.deltaTime;
                if (!((IOffenseShip)instance.ShipInstance).IsShipEngaging)
                    yield break;
                yield return null;
            }
            //yield return new WaitUntil(() => instance.IsAbleToActivate);
            while (!instance.IsAbleToActivate)
            {
                if (!((IOffenseShip)instance.ShipInstance).IsShipEngaging)
                    yield break;
                yield return null;
            }
            instance.FireSound.Play();
            instance.ShipInstance.isAbleToRecoverPower = false;
            instance.ShipInstance.currentPower -= instance.GetPowerConsumption;
            instance.GetCurrentChargeCount--;
            ChargeBase charge = instance.GetCharges.ElementAtOrDefault(instance.GetCurrentChargeCount);
            if (charge) charge.ChangeState(ProjectileCharge.ProjectileCharge_State.fire);
        }
    }

    public override void Execute()
    {
        var wantedRot = instance.ShipInstance.ShipInfo.ShipTransform.localRotation;
        if (CharShipStateMng.GetInstance.targetedShip && instance.IsModuleFittedOnChar)
        {
            CharShipStateMng CharShipInstance = (CharShipStateMng)instance.ShipInstance;
            // TODO: 시간 = 거리 / 속력 코드를 이용한 예측 샷
            float v1 = CharShipInstance.targetedShip.currentShipSpeed;
            float v2 = instance.GetCharges.ElementAtOrDefault(Mathf.Max(instance.GetCurrentChargeCount - 1, 0)).GetVelocity;
            float u1 = (CharShipInstance.targetedShip.ShipInfo.ShipTransform.localPosition - instance.ModuleTransform.position).magnitude;
            float h = u1 / v2;
            float u2 = h * v1;

            // 적함선이 각도 안에 들어와야 공격 가능
            Vector3 targetDir = CharShipInstance.ShipInfo.ShipTransform.InverseTransformPoint(CharShipInstance.targetedShip.ShipInfo.ShipTransform.localPosition);
            float targetDeg = Mathf.Atan2(targetDir.x, targetDir.y) * Mathf.Rad2Deg;

            Vector3 target =
                CharShipInstance.targetedShip.ShipInfo.ShipTransform.localPosition +
                CharShipInstance.targetedShip.ShipInfo.ShipTransform.up * (u2 + (u2 * 0.65f));// (u2 - Mathf.Clamp(targetDeg, 0f, 90f) * 0.75f); 
            Vector3 direction = target - instance.ModuleTransform.position;
            float deg = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;

            // 적 함선이 공격 가능 각도에 들어와있는지 여부
            instance.IsAbleToActivate = Mathf.Abs(targetDeg) < instance.GetAttackableDeg;
            wantedRot = instance.IsAbleToActivate ? Quaternion.Euler(0f, 0f, deg) : instance.ShipInstance.ShipInfo.ShipTransform.localRotation;
        }
        else {
            instance.IsAbleToActivate = true;
            if (!instance.IsModuleFittedOnChar)
            {
                CharShipStateMng CharShipInstance = instance.ModuleInstance.ShipInstance.GetGameStateMng.GetCharShipStateMng;
                Vector3 targetDir = instance.ShipInstance.ShipInfo.ShipTransform.InverseTransformPoint(CharShipInstance.ShipInfo.ShipTransform.localPosition);
                float targetDeg = Mathf.Atan2(targetDir.x, targetDir.y) * Mathf.Rad2Deg;

                Vector3 target = CharShipInstance.ShipInfo.ShipTransform.localPosition;
                Vector3 direction = target - instance.ModuleTransform.position;

                float deg = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
                instance.IsAbleToActivate = Mathf.Abs(targetDeg) < instance.GetAttackableDeg;
                if (instance.IsAbleToActivate)
                    wantedRot = Quaternion.Euler(0f, 0f, deg);
            }
        }
        instance.ModuleTransform.rotation =
            Quaternion.Slerp(instance.ModuleTransform.rotation, wantedRot, ModuleBase.ModuleRotSpeed * Time.deltaTime);
    }

    public override void Exit()
    {
        StartCoroutine(((IOffenseShip)instance.ShipInstance).ActivePower(true));
    }
}
