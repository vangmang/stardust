﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class ProjectileEnterState : State<ProjectileModule> {

    public override Enum GetState
    {
        get
        {
            return ProjectileModule.Projectile_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
