﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[AddComponentMenu("Module/OffensiveModule/Projectile")]
public class ProjectileModule : OffensiveModule<ProjectileModule, ShipBase>
{
    // 공격 / 속도 / 탄창 / 각도 / 거리 / 타겟팅 
    //  3      7      7     5      5      5
    // 프로젝타일
    // 보너스: 속도, 탄창
    // 페널티: 공격력

    public enum Projectile_State
    {
        enter,
        idle,
        targeting,
        active,
    }

    [SerializeField]
    private ShipBase shipBase;

    // 발사 사운드
    public AudioSource FireSound;
    // 발사체들의 리스트
    private List<ProjectileCharge> projectileCharges = new List<ProjectileCharge>();
    // 무기 타입
    public override WeaponType GetWeaponType { get { return WeaponType.projectile; } }
    // 함선 인스턴스
    public override ShipBase ShipInstance { get { return shipBase; } }
    // IEnumerable로 리스트에 접근
    public IEnumerable<ProjectileCharge> GetCharges
    {
        get
        {
            return projectileCharges;
        }
    }

    public override ProjectileModule ModuleInstance
    {
        get
        {
            return this;
        }
    }

    void Awake()
    {
        currentChargeCount = maxCharges;
        InitializeState();
    }

    void Start()
    {
        // 만약에 필요없으면 지울 것
        if (((IOffenseShip)shipBase).GetWeaponType == WeaponType.projectile)
        {
            Bonus();
            Penalty();
        }

        IsModuleFittedOnChar = ShipInstance.GetType().Equals(typeof(CharShipStateMng));
        GenerateProjectiles();
        StateEnter(Projectile_State.enter);
    }

    protected override void GenerateProjectiles()
    {
        StartCoroutine(InvokeGenerateProjectiles());
    }

    // 프로젝타일 생성 (프로젝타일, 플라즈마, 하이브리드 모든 모듈 발사체 생성 루틴이 같다.)
    private IEnumerator InvokeGenerateProjectiles()
    {
        yield return new WaitForFixedUpdate();
        Transform container = ((IOffenseShip)shipBase).ProjectileContainer;
        StringBuilder sb = new StringBuilder();
        sb.Append("MainCanvas/World/");
        if (container.name.Contains("Enemy"))
            sb.Append(GameStateMng.Instance.StageInfo.StageTransformName).Append("/");
        sb.Append(container.parent.name).Append("/").Append(container.name).Append("/Projectiles");
        for (int i = 0; i < maxCharges; i++)
        {
            ProjectileCharge projectile =
                Mng.Instance.CreatePrefab(sb.ToString(), "Items/Charges/ProjectileCharge", E_RESOURCES.E_3_GAMESCENE, Vector3.zero, Quaternion.identity).GetComponentInChildren<ProjectileCharge>();
            projectile.setModuleInstance = this;
            projectileCharges.Add(projectile);
        }
    }

    // 함선 파워그리드 증가
    // 함선 기동성 증가
    // 함선 타겟팅 스피드 증가
    // 함선 방어력 약간 증가
    // 함선 내구력 약간 증가
    public override void Bonus()
    {
        shipBase.GetShipStat.SetPower = shipBase.GetShipStat.GetPower * 1.15f;
        shipBase.GetShipStat.SetMobility = shipBase.GetShipStat.GetMobility * 1.2f;
        shipBase.GetShipStat.SetTargetingSpeed = shipBase.GetShipStat.GetTargetingSpeed * 1.175f;
        shipBase.GetShipStat.SetArmor = shipBase.GetShipStat.GetArmor * 1.025f;
        shipBase.GetShipStat.SetHitpoints = shipBase.GetShipStat.GetHitpoints * 1.025f;
    }

    // 발사체 데미지 약간 감소
    // 탄속 약간 감소
    // 함선 최대 속력 약간 감소
    public override void Penalty()
    {
        damage *= 0.9f;
        objectVelocity *= 0.9f;
        shipBase.GetShipStat.SetMaxSpeed = shipBase.GetShipStat.GetMaxSpeed * 0.9f;
    }

    void Update()
    {
        Execute();
    }

    void OnDestroy()
    {

    }
}
