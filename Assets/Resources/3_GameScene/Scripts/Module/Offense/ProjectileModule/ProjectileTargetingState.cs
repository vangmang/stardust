﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class ProjectileTargetingState : State<ProjectileModule> {

    public override Enum GetState
    {
        get
        {
            return ProjectileModule.Projectile_State.targeting;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
