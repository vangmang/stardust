﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class ProjectileIdleState : State<ProjectileModule> {

    public override Enum GetState
    {
        get
        {
            return ProjectileModule.Projectile_State.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
