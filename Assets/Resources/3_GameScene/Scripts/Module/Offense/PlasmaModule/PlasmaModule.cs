﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[AddComponentMenu("Module/OffensiveModule/Plasma")]
public class PlasmaModule : OffensiveModule<PlasmaModule, ShipBase>
{
    // 공격 / 속도 / 탄창 / 각도 / 거리 / 타겟팅 
    //  10     3     4      5      3      7
    // 플라즈마
    // 보너스: 공격력, 타겟팅
    // 페널티: 속도, 탄창, 거리

    public enum Plasma_State
    {
        enter,
        idle,
        active,
        reload
    }

    [SerializeField]
    private ShipBase shipBase;

    [Serializable]
    public struct _ModuleEffect_
    {
        public AudioSource FireSound;
        public LensFlare FireEffect;
        [NonSerialized]
        public float EffectSpeed;
    }
    public _ModuleEffect_ ModuleEffect;

    private List<PlasmaCharge> plasmaCharges = new List<PlasmaCharge>();
    public override WeaponType GetWeaponType { get { return WeaponType.plasma; } }
    public override ShipBase ShipInstance { get { return shipBase; } }

    public IEnumerable<PlasmaCharge> GetCharges
    {
        get
        {
            return plasmaCharges;
        }
    }

    public override PlasmaModule ModuleInstance
    {
        get
        {
            return this;
        }
    }

    void Awake()
    {
        currentChargeCount = maxCharges;
        InitializeState();
    }

    void Start()
    {
        if (((IOffenseShip)shipBase).GetWeaponType == WeaponType.plasma)
        {
            Bonus();
            Penalty();
        }

        IsModuleFittedOnChar = ShipInstance.GetType().Equals(typeof(CharShipStateMng));
        ModuleEffect.EffectSpeed = ModuleEffect.FireSound.clip.length * 2f;
        GenerateProjectiles();
        StateEnter(Plasma_State.enter);
    }

    protected override void GenerateProjectiles()
    {
        StartCoroutine(InvokeGenerateProjectiles());
    }

    private IEnumerator InvokeGenerateProjectiles()
    {
        yield return new WaitForFixedUpdate();
        Transform container = ((IOffenseShip)shipBase).ProjectileContainer;
        StringBuilder sb = new StringBuilder();
        sb.Append("MainCanvas/World/");
        if (container.name.Contains("Enemy"))
            sb.Append(GameStateMng.Instance.StageInfo.StageTransformName).Append("/");
        sb.Append(container.parent.name).Append("/").Append(container.name).Append("/Plasma");
        for (int i = 0; i < maxCharges; i++)
        {
            PlasmaCharge plasma =
                Mng.Instance.CreatePrefab(sb.ToString(), "Items/Charges/PlasmaCharge", E_RESOURCES.E_3_GAMESCENE, Vector3.zero, Quaternion.identity).GetComponentInChildren<PlasmaCharge>();
            plasma.setModuleInstance = this;
            plasmaCharges.Add(plasma);
        }
    }

    void Update()
    {
        if(!ShipInstance.IsShipDown)
            Execute();
    }

    // 발사체 데미지 증가
    // 함선 최대 속력 증가
    // 탄속 증가
    // 함선 타겟팅 스피드 대폭 증가
    public override void Bonus()
    {
        damage *= 1.5f;
        shipBase.GetShipStat.SetMaxSpeed = shipBase.GetShipStat.GetMaxSpeed * 1.075f;
        objectVelocity *= 1.33f;
        shipBase.GetShipStat.SetTargetingSpeed = shipBase.GetShipStat.GetTargetingSpeed * 2f;
    }

    // 모듈 활성화시 소모되는 동력 증가
    // 모듈 사정거리 대폭 감소
    public override void Penalty()
    {
        effectiveRange *= 0.33f;
        powerConsumption *= 1.5f;
    }

    public IEnumerator ChargeEffectDisable()
    {
        yield return new WaitForFixedUpdate();
        float start = ModuleEffect.FireEffect.brightness;
        float t = 0f;
        while (t <= 1f)
        {
            t += ModuleEffect.EffectSpeed * Time.deltaTime;
            ModuleEffect.FireEffect.brightness = Mathf.Lerp(start, 0f, t);
            yield return null;
        }
    }

    void OnDestroy()
    {

    }
}
