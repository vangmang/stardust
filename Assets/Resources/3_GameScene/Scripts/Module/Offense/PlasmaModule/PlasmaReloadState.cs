﻿using System;
using GameStateLibrary.State;
using UnityEngine;
using System.Collections;

public class PlasmaReloadState : State<PlasmaModule> {

    public override Enum GetState
    {
        get
        {
            return PlasmaModule.Plasma_State.reload;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.isReloading = true;
        StartCoroutine(Reload());
    }

    private IEnumerator Reload()
    {
        yield return new WaitForSeconds(instance.GetReloadingSpeed);
        instance.isReloading = false;
        var state = ((IOffenseShip)instance.ShipInstance).IsShipEngaging ? PlasmaModule.Plasma_State.active : PlasmaModule.Plasma_State.idle;
        instance.GetCurrentChargeCount = instance.GetMaxCharges;
        instance.InvokeReloadComplete();
        instance.ChangeState(state, false);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
