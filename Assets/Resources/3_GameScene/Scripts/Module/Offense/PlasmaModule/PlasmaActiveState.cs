﻿using System;
using GameStateLibrary.State;
using UnityEngine;
using System.Collections;
using System.Linq;

// 플라즈마 [재장전 유]
public class PlasmaActiveState : State<PlasmaModule>
{
    public override Enum GetState
    {
        get
        {
            return PlasmaModule.Plasma_State.active;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.IsAbleToActivate = true;
        StartCoroutine(active());
    }

    // TODO: 그냥 볼 것
    private IEnumerator active()
    {
        StartCoroutine(rotate());
        yield return new WaitForFixedUpdate();
        // 개뻘짓하며 디버깅 완료
        while(instance.isReloading)
        {
            if (!((IOffenseShip)instance.ShipInstance).IsShipEngaging)
                yield break;
            yield return null;
        }
        // 개뻘짓하며 디버깅 완료
        while (((IOffenseShip)instance.ShipInstance).IsShipEngaging)
        {
            while (!instance.IsAbleToActivate)
            {
                if (!((IOffenseShip)instance.ShipInstance).IsShipEngaging)
                    yield break;
                yield return null;
            }
            float time = 0f;
            while (time <= instance.GetCycleSpeed)
            {
                time += Time.deltaTime;
                if(time >= instance.GetCycleSpeed * 0.99f)
                    instance.ModuleEffect.FireEffect.brightness = 1f;
                if (!((IOffenseShip)instance.ShipInstance).IsShipEngaging)
                    yield break;
                yield return null;
            }
            while (!instance.IsAbleToActivate && instance.isReloading)
            {
                if (!((IOffenseShip)instance.ShipInstance).IsShipEngaging)
                    yield break;
                yield return null;
            }
            instance.ShipInstance.isAbleToRecoverPower = false;
            instance.ShipInstance.currentPower -= instance.GetPowerConsumption;
            instance.ModuleEffect.FireEffect.brightness = 0f;
            instance.ModuleEffect.FireSound.Play();
            instance.GetCurrentChargeCount--;

            ChargeBase charge = instance.GetCharges.ElementAtOrDefault(instance.GetCurrentChargeCount);
            if (charge) charge.ChangeState(PlasmaCharge.PlasmaCharge_State.fire);
            if (instance.GetCurrentChargeCount <= 0f)
            {
                instance.ChangeState(PlasmaModule.Plasma_State.reload);
                yield break;
            }
        }
    }

    private IEnumerator rotate()
    {
        while (((IOffenseShip)instance.ShipInstance).IsShipEngaging)
        {
            var wantedRot = instance.ShipInstance.ShipInfo.ShipTransform.localRotation;
            if (CharShipStateMng.GetInstance.targetedShip && instance.IsModuleFittedOnChar)
            {
                CharShipStateMng CharShipInstance = (CharShipStateMng)instance.ShipInstance;
                float v1 = CharShipInstance.targetedShip.currentShipSpeed;
                float v2 = instance.GetCharges.ElementAtOrDefault(Mathf.Max(instance.GetCurrentChargeCount - 1, 0)).GetVelocity;
                float u1 = (CharShipInstance.targetedShip.ShipInfo.ShipTransform.localPosition - instance.ModuleTransform.position).magnitude;
                float h = u1 / v2;
                float u2 = h * v1;

                // 적함선이 각도 안에 들어와야 공격 가능
                Vector3 targetDir = CharShipInstance.ShipInfo.ShipTransform.InverseTransformPoint(CharShipInstance.targetedShip.ShipInfo.ShipTransform.localPosition);
                float targetDeg = Mathf.Atan2(targetDir.x, targetDir.y) * Mathf.Rad2Deg;
                Vector3 target =
                    CharShipInstance.targetedShip.ShipInfo.ShipTransform.localPosition +
                    CharShipInstance.targetedShip.ShipInfo.ShipTransform.up * (u2 + (u2 * 0.1f));
                Vector3 direction = target - instance.ModuleTransform.position;
                float deg = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;

                instance.IsAbleToActivate = Mathf.Abs(targetDeg) < instance.GetAttackableDeg;
                wantedRot = instance.IsAbleToActivate ? Quaternion.Euler(0f, 0f, deg) : instance.ShipInstance.ShipInfo.ShipTransform.localRotation;
            }

            else {
                instance.IsAbleToActivate = true;
                if (!instance.IsModuleFittedOnChar)
                {
                    CharShipStateMng CharShipInstance = instance.ModuleInstance.ShipInstance.GetGameStateMng.GetCharShipStateMng;
                    Vector3 targetDir = instance.ShipInstance.ShipInfo.ShipTransform.InverseTransformPoint(CharShipInstance.ShipInfo.ShipTransform.localPosition);
                    float targetDeg = Mathf.Atan2(targetDir.x, targetDir.y) * Mathf.Rad2Deg;

                    Vector3 target = CharShipInstance.ShipInfo.ShipTransform.localPosition;
                    Vector3 direction = target - instance.ModuleTransform.position;

                    float deg = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
                    instance.IsAbleToActivate = Mathf.Abs(targetDeg) < instance.GetAttackableDeg;
                    if (instance.IsAbleToActivate)
                        wantedRot = Quaternion.Euler(0f, 0f, deg);
                }
            }
            instance.ModuleTransform.rotation =
                Quaternion.Slerp(instance.ModuleTransform.rotation, wantedRot, ModuleBase.ModuleRotSpeed * Time.deltaTime);
            yield return null;
        }
    }

    public override void Execute()
    {

    }

    private IEnumerator moduleRot()
    {
        float t = 0f;
        var start = instance.ModuleTransform.rotation;
        while (t <= 1f)
        {
            if (instance.IsAbleToActivate)
                yield break;

            t += ModuleBase.ModuleRotSpeed * Time.deltaTime;
            instance.ModuleTransform.rotation = Quaternion.Slerp(start, instance.ShipInstance.ShipInfo.ShipTransform.localRotation, t);
            yield return null;
        }
    }

    public override void Exit()
    {
        instance.IsAbleToActivate = false;
        StartCoroutine(moduleRot());
        StartCoroutine(instance.ChargeEffectDisable());
        StartCoroutine(((IOffenseShip)instance.ShipInstance).ActivePower(true));
    }
}
