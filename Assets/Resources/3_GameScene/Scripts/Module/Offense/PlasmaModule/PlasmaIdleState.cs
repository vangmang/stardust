﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class PlasmaIdleState : State<PlasmaModule> {

    public override Enum GetState
    {
        get
        {
            return PlasmaModule.Plasma_State.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
