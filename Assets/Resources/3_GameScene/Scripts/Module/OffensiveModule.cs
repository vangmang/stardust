﻿using System;
using UnityEngine;

public abstract class OffensiveModule : ModuleBase, IOffensiveModule
{
    public event EventHandler ReloadCompleteEvent;
    public bool isReloading;
    [SerializeField]
    protected int maxCharges;
    [SerializeField]
    protected int currentChargeCount;
    public int GetCurrentChargeCount
    {
        get
        {
            return currentChargeCount;
        }
        set
        {
            currentChargeCount = value > -1 ? value : maxCharges - 1;
        }
    }

    public int GetMaxCharges { get { return maxCharges; } }

    public abstract WeaponType GetWeaponType { get; }
    public void InvokeReloadComplete()
    {
        if (ReloadCompleteEvent != null)
            ReloadCompleteEvent(this, new EventArgs());
    }
}

public abstract class OffensiveModule<T, U> : OffensiveModule, IOffensiveModule<T, U>
    where T : class
    where U : class, IShip
{

    [SerializeField]
    protected float effectiveRange;     // 유효 사정거리
    [SerializeField]    
    protected float damage;             // 데미지
    [SerializeField]
    protected float cycle;              // 공격 주기
    [SerializeField]
    protected float attackableDeg;      // 공격 가능 각도
    [SerializeField]
    protected float reloadingSpeed;     // 재장전 속도 
    [SerializeField]
    protected float objectVelocity;     // 탄속

    public bool IsAbleToActivate;       // 모듈이 활성화 가능한지 여부 
    public bool IsModuleFittedOnChar;   // 모듈이 적들에게 장착되었는지 여부

    public abstract U ShipInstance { get; }
    public abstract T ModuleInstance { get; }
    public override ModuleType GetModuleType { get { return ModuleType.Offense; } }
    public float GetEffectiveRange { get { return effectiveRange; } }
    public float GetDamage { get { return damage; } }
    public float GetCycleSpeed { get { return cycle; } }
    public float GetReloadingSpeed { get { return reloadingSpeed; } }
    public float GetDPS { get { return 60 / cycle * damage; } }
    public float GetAttackableDeg { get { return attackableDeg * 0.5f; } }
    public float GetObjectVelocity { get { return objectVelocity; } }

    protected abstract void GenerateProjectiles();
}
