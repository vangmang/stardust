﻿using UnityEngine;

public class CargoExtension : UtilityModule<CargoExtension, ShipBase>
{

    [SerializeField]
    private ShipBase shipBase;

    public override UtilityType GetUtilityType { get { return UtilityType.sensorUpgrade; } }
    public override ShipBase ShipInstance { get { return shipBase; } }
    public override CargoExtension ModuleInstance { get { return this; } }

    public override void Bonus()
    {
    }

    public override void Penalty()
    {
    }
}
