﻿using System;
using UnityEngine;

public class CPU_Upgrade : UtilityModule<CPU_Upgrade, ShipBase>
{

    [SerializeField]
    private ShipBase shipBase;

    public override UtilityType GetUtilityType { get { return UtilityType.sensorUpgrade; } }
    public override ShipBase ShipInstance { get { return shipBase; } }
    public override CPU_Upgrade ModuleInstance { get { return this; } }

    public override void Bonus()
    {
    }

    public override void Penalty()
    {
    }
}
