﻿using UnityEngine;

public class SensorUpgrade : UtilityModule<SensorUpgrade, ShipBase>
{

    [SerializeField]
    private ShipBase shipBase;

    public override UtilityType GetUtilityType { get { return UtilityType.sensorUpgrade; } }
    public override ShipBase ShipInstance { get { return shipBase; } }
    public override SensorUpgrade ModuleInstance { get { return this; } }
    public override void Bonus()
    {
    }

    public override void Penalty()
    {
    }
}
