﻿using UnityEngine;

public class GridUpgrade : UtilityModule<GridUpgrade, ShipBase>
{

    [SerializeField]
    private ShipBase shipBase;

    public override UtilityType GetUtilityType { get { return UtilityType.sensorUpgrade; } }
    public override ShipBase ShipInstance { get { return shipBase; } }
    public override GridUpgrade ModuleInstance { get { return this; } }
    public override void Bonus()
    {
    }

    public override void Penalty()
    {
    }
}
