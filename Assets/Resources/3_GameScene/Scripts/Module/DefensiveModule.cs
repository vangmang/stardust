﻿using UnityEngine;

public abstract class DefensiveModule<T, U>: ModuleBase, IDefensiveModule<T, U>
    where T : class
    where U : class, IShip
{
    [SerializeField]
    protected float armor;
    [SerializeField]
    protected float hitpoints;

    public abstract U ShipInstance { get; }
    public abstract DefenseType GetDefenseType { get; }
    public override ModuleType GetModuleType { get { return ModuleType.Defense; } }
    public abstract T ModuleInstance { get; } 
    public float GetArmor { get { return armor; } }
    public float GetHitpoints { get { return hitpoints; } }
}
