﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class DroneStateMng : EnemyShipBase<DroneStateMng>, 
    IOffenseShip<DroneStateMng>
{
    public enum Drone_State
    {
        enter,
        idle,
        launcehd,
        active,
        collected,
        down
    }
    public override DroneStateMng Instance { get { return this; } }
    public override ShipType GetShipType { get { return ShipType.drone; } }

    [Serializable]
    public struct _DroneEffect_
    {
        [SerializeField]
        private LensFlare thrusterLens;
        [SerializeField]
        private LensFlare downLens;
        [SerializeField]
        private TrailRenderer thrusterTrail;

        public LensFlare DownLens { get { return downLens; } }
        public LensFlare ThrusterLens { get { return thrusterLens; } }
        public TrailRenderer ThrusterTrail { get { return thrusterTrail; } }
    }

    public _DroneEffect_ DroneEffect;

    #region NotSupport
    // 드론은 별개의 미사일 무기를 사용한다.
    public IEnumerable<IOffensiveModule> OffensiveModules { get { throw new NotSupportedException(); } }
    public WeaponType GetWeaponType { get { throw new NotSupportedException(); } }
    #endregion 
    public ShipBase Target;
    [SerializeField]
    private ShipBase motherShipBase;
    public ShipBase GetMotherShipInstance { get { return motherShipBase; } }
    public ShipBase SetMotherShipInstance { set { motherShipBase = value; } }
    [SerializeField]
    private float Cycle;
    [SerializeField]
    private Transform projectileContainer;

    // 모듈 수를 조정할 수 없도록 함. 두 개로 고정
    [Serializable]
    public struct _DroneMissiles_
    {
        public DroneMissileStateMng droneMissiles1;
        public DroneMissileStateMng droneMissiles2;
    }

    public _DroneMissiles_ DroneMissiles;
    public float GetCycle { get { return Cycle; } }
    public Transform ProjectileContainer { get { return projectileContainer; } }
    public bool IsShipEngaging { get; set; }

    void Awake()
    {
        InitializeState();
    }

	// Use this for initialization
	void Start () {
        StateEnter(Drone_State.enter);
	}

    public IEnumerator ActivePower(bool compulsory)
    {
        float t = 0f;
        while (t <= 0.75f)
        {
            if (IsShipEngaging)
                yield break;
            t += Time.deltaTime;
            yield return null;
        }
        isAbleToRecoverPower = true;
    }

    public override IEnumerator CancelShipSailing()
    {
        IsShipMoving = false;
        float trailStart = DroneEffect.ThrusterTrail.time;
        float startSpeed = currentShipSpeed;
        float brightness = DroneEffect.ThrusterLens.brightness;
        float t = 0f;

        while (t <= 0.99f)
        {
            t += GetShipStat.GetSpeedAcceleration * Time.deltaTime;
            DroneEffect.ThrusterTrail.time = Mathf.Lerp(trailStart, 0f, t);
            currentShipSpeed = Mathf.Lerp(startSpeed, 0f, t);
            DroneEffect.ThrusterLens.brightness = Mathf.Lerp(brightness, 0f, t);
            ShipInfo.ShipTransform.localPosition += ShipInfo.ShipTransform.up * currentShipSpeed * GetShipStat.GetMobility * Time.deltaTime;
            yield return null;
            if (IsShipMoving)
                yield break;
        }
        IsReadyToWarp = true;
    }
    // Update is called once per frame
    void Update () {
        Execute();
	}

    void OnDestroy()
    {

    }

    public override void Init()
    {

    }

}
