﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

public class DroneActiveState : State<DroneStateMng>
{

    public override Enum GetState
    {
        get
        {
            return DroneStateMng.Drone_State.active;
        }
    }

    private Vector3 destination;
    private bool isArrived;
    private float arrivalPoint;
    private ShipBase Target;
    private float AI_initTime;
    float trailSpeed = 0.45f;

    public override void Enter(params object[] o_Params)
    {
        instance.IsShipEngaging = true;
        instance.IsShipMoving = true;
        isArrived = true;
        instance.currentShipSpeed = 0f;
        Target = instance.Target;
        destination = Target.ShipInfo.ShipTransform.localPosition;
        arrivalPoint = Vector3.Distance(destination, instance.ShipInfo.ShipTransform.position) * 0.3f;

        StartCoroutine(MissileActive());
    }

    private IEnumerator MissileActive()
    {
        while (instance.IsShipEngaging)
        {
            yield return new WaitForSeconds(instance.GetCycle);
            instance.DroneMissiles.droneMissiles1.ChangeState(DroneMissileStateMng.DroneMissile_State.fire);
            instance.DroneMissiles.droneMissiles2.ChangeState(DroneMissileStateMng.DroneMissile_State.fire);
        }
    }

    // AI 로직 // Assault 와 비슷함
    // 1) 목적지를 바라보고 계속 이동한다.
    // 2) 목적지는 계속 갱신된다.
    // 3) 목적지에 도달하면 현재 각도를 계산하여 각도에 해당하는 목적지로 다시 이동한다
    // 4) 루프
    public override void Execute()
    {
        if(instance.GetMotherShipInstance.IsShipDown)
        {
            instance.ChangeState(DroneStateMng.Drone_State.down);
            return;
        }
        instance.DroneEffect.ThrusterTrail.time =
            Mathf.Lerp(instance.DroneEffect.ThrusterTrail.time, trailSpeed, instance.GetShipStat.GetSpeedAcceleration * 12.5f * Time.deltaTime);
        instance.DroneEffect.ThrusterLens.brightness =
            Mathf.Lerp(instance.DroneEffect.ThrusterLens.brightness, 0.7f, instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);

        //----------------------AI 부분----------------------//
        Vector3 point = instance.ShipInfo.ShipTransform.InverseTransformPoint(Target.ShipInfo.ShipTransform.localPosition);
        float curDeg = Mathf.Atan2(point.x, point.y) * Mathf.Rad2Deg;

        float deg = 0f;
        Vector3 direction;

        if (isArrived)
        {
            destination = Target.ShipInfo.ShipTransform.localPosition + Target.ShipInfo.ShipTransform.right * ((curDeg > 0) ? 1 : -1) * 210f +
                          Target.ShipInfo.ShipTransform.up * CharShipStateMng.GetInstance.currentShipSpeed * 0.5f;
            arrivalPoint = Vector3.Distance(destination, instance.ShipInfo.ShipTransform.localPosition) * 0.2f;
            AI_initTime = 0f;
            isArrived = false;
        }
        AI_initTime += Time.deltaTime;
        if (AI_initTime >= 0.2f)
            AI_initTime = 0.2f;

        float distance = Vector3.Distance(destination, instance.ShipInfo.ShipTransform.localPosition);
        if (distance <= arrivalPoint || Mathf.Abs(curDeg) >= 135f)
        {
            if (AI_initTime >= 0.2f)
                isArrived = true;
        }
        //---------------------------------------------------//

        direction = (destination - instance.ShipInfo.ShipTransform.position).normalized;
        deg = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        instance.currentShipRot = deg;
        instance.ShipInfo.ShipTransform.localRotation =
            Quaternion.Slerp(instance.ShipInfo.ShipTransform.localRotation,
            Quaternion.Euler(0f, 0f, deg),
            instance.GetShipStat.GetRotAcceleration * Time.deltaTime);

        instance.currentShipSpeed =
            Mathf.Lerp(instance.currentShipSpeed,
            instance.GetShipStat.GetMaxSpeed,
            instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);
        instance.ShipInfo.ShipTransform.position +=
            instance.ShipInfo.ShipTransform.up * instance.currentShipSpeed * Time.deltaTime;
    }

    public override void Exit()
    {
        instance.IsShipEngaging = false;
        StartCoroutine(instance.CancelShipSailing());
        StartCoroutine(instance.CancelShipRotation());
    }
}
