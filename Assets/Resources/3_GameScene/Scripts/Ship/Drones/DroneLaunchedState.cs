﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

public class DroneLaunchedState : State<DroneStateMng> {

    public override Enum GetState
    {
        get
        {
            return DroneStateMng.Drone_State.launcehd;
        }
    }

    private Vector3 launchedDestination;

    public override void Enter(params object[] o_Params)
    {
        instance.ShipInfo.ShipTransform.localPosition = instance.GetMotherShipInstance.ShipInfo.ShipTransform.localPosition;
        instance.ShipInfo.ShipTransform.gameObject.SetActive(true);
        StartCoroutine(Launching());
    }

    private IEnumerator Launching()
    {
        float t = 0f;
        Vector3 start = instance.ShipInfo.ShipTransform.localPosition;
        Vector3 end = instance.GetMotherShipInstance.ShipInfo.ShipTransform.localPosition + instance.GetMotherShipInstance.ShipInfo.ShipTransform.up * -180f;
        Vector3 direction = (end - instance.ShipInfo.ShipTransform.localPosition).normalized;
        float rot = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        instance.ShipInfo.ShipTransform.localRotation = Quaternion.Euler(0f, 0f, rot);
        while (t <= 1f)
        {
            t += instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime;
            instance.ShipInfo.ShipTransform.localPosition = Vector3.Lerp(start, end, t);
            yield return null;
        }
        yield return new WaitForFixedUpdate();
        instance.ChangeState(DroneStateMng.Drone_State.active);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
