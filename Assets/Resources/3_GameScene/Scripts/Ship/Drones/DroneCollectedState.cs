﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class DroneCollectedState : State<DroneStateMng> {

    public override Enum GetState
    {
        get
        {
            return DroneStateMng.Drone_State.collected;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
