﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class DroneEnterState : State<DroneStateMng> {

    public override Enum GetState
    {
        get
        {
            return DroneStateMng.Drone_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.ChangeState(DroneStateMng.Drone_State.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
