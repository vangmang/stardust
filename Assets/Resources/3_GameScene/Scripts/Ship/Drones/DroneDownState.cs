﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

public class DroneDownState : State<DroneStateMng> {

    public override Enum GetState
    {
        get
        {
            return DroneStateMng.Drone_State.down;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        StartCoroutine(disableColor());
    }

    private IEnumerator disableColor()
    {
        float t = 0f;
        float r = instance.ShipInfo.ShipImage.color.r;
        float g = instance.ShipInfo.ShipImage.color.g;
        float b = instance.ShipInfo.ShipImage.color.b;

        while (t <= 1f)
        {
            t += 1.2f * Time.deltaTime;
            var color =
                new Color(Mathf.Lerp(r, 0.4f, t), Mathf.Lerp(g, 0.4f, t), Mathf.Lerp(b, 0.4f, t));
            instance.ShipInfo.ShipImage.color = color;
            yield return null;
        }
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
