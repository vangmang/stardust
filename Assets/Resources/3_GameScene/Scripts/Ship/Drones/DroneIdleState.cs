﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class DroneIdleState : State<DroneStateMng> {

    public override Enum GetState
    {
        get
        {
            return DroneStateMng.Drone_State.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.ShipInfo.ShipTransform.gameObject.SetActive(false);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
