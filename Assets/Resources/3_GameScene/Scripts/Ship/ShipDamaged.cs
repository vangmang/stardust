﻿using UnityEngine;
using System;

public sealed class ShipDamaged : MonoBehaviour {

    //public delegate void _TakeDamage(ICharges causer);
    //public Action<_TakeDamage> TakeDamage;

    [SerializeField]
    private ShipBase shipBase;

    // TODO: 충돌처리 나중에 대리자로 수정할 것
    void TakeDamage(ICharges causer)
    {
        shipBase.TakeDamage(causer);
    }

    void TakeDamage(DroneMissileStateMng droneMissile)
    {
        shipBase.TakeDamage(droneMissile);
    }
}
