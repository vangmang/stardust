﻿using UnityEngine;

public sealed class ModuleLimit : MonoBehaviour {

    public ShipStatMng GetShipStatMng;
    public Transform OffenseModulesContainer;
    public Transform DefenseModulesContainer;
    public Transform UtilityModulesContainer;
}
