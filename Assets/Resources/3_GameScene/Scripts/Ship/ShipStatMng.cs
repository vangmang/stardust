﻿using System;
using UnityEngine;

public class ShipStatMng : MonoBehaviour, IShipStat
{
    // 1AU(1억 5천만 km)로 가정
    public static readonly float WarpSpeed = 15000f;
    private ShipStatMng() { }

    [Serializable]
    public struct _ShipLevel_
    {
        public string ShipName;
        public int ShipLevel;
        public int ValueOfCharges_perUp;
        public float ValueOfHP_perUp;
        public float ValueOfCPU_perUp;
        public float ValueOfPower_perUp;
        public float ValueOfDmg_perUp;
        public float ValueOfArmor_perUp;
        public float ValueOfSensor_perUp;
    }
    [Serializable]
    public struct _Modules_
    {
        public int OffensiveModuleCount;
        public int DefensiveModuleCount;
        public int UtilityModuleCount;
    }
    [Serializable]
    public struct _ShipStat_
    {
        public int charges;
        public float MaxSpeed;
        public float SpeedAcceleration;
        public float RotAcceleration;
        public float CargoVolume;
        public float CargoWeight;
        public float HitPoints;
        public float CPU;
        public float PowerGrid;
        public float PowerRecoverFigure;
        public float Damage;
        public float Armor;
        public float Volume;
        public float Weight;
        public float Mobility;
        public float TargetingSpeed;
    }
    public _ShipLevel_ ShipLevel;
    public _Modules_ Modules;
    public _ShipStat_ ShipStat;

    #region ShipLevel
    public string GetShipName { get { return ShipLevel.ShipName; } }
    public int GetShipLevel { get { return ShipLevel.ShipLevel; } }
    public int GetValueOfCharges_perUp { get { return ShipLevel.ValueOfCharges_perUp; } }
    public float GetValueOfHP_perUp { get { return ShipLevel.ValueOfHP_perUp; } }
    public float GetValueOfCPU_perUp { get { return ShipLevel.ValueOfCPU_perUp; } }
    public float GetValueOfPower_perUp { get { return ShipLevel.ValueOfPower_perUp; } }
    public float GetValueOfDmg_perUp { get { return ShipLevel.ValueOfDmg_perUp; } }
    public float GetValueOfArmor_perUp { get { return ShipLevel.ValueOfArmor_perUp; } }
    public float GetValueOfSensor_perUp { get { return ShipLevel.ValueOfSensor_perUp; } }

    #endregion

    #region Modules
    public int GetOffensiveModuleCount { get { return Modules.OffensiveModuleCount; } }
    public int GetDefensiveModuleCount { get { return Modules.DefensiveModuleCount; } }
    public int GetUtilityModuleCount { get { return Modules.UtilityModuleCount; } }
    #endregion

    #region ShipStat
    public int GetCharges { get { return ShipStat.charges; } }
    public float GetMaxSpeed { get { return ShipStat.MaxSpeed; } }
    public float GetSpeedAcceleration { get { return ShipStat.SpeedAcceleration; } }
    public float GetRotAcceleration { get { return ShipStat.RotAcceleration; } }
    public float GetCargoVolume { get { return ShipStat.CargoVolume; } }
    public float GetCargoWeight { get { return ShipStat.CargoWeight; } }
    public float GetHitpoints { get { return ShipStat.HitPoints; } }
    public float GetCPU { get { return ShipStat.CPU; } }
    public float GetPower { get { return ShipStat.PowerGrid; } }
    public float GetPowerRecoverFigure { get { return ShipStat.PowerRecoverFigure; } }
    public float GetDamage { get { return ShipStat.Damage; } }
    public float GetArmor { get { return ShipStat.Armor; } }
    public float GetVolume { get { return ShipStat.Volume; } }
    public float GetWeight { get { return ShipStat.Weight; } }
    public float GetMobility { get { return ShipStat.Mobility; } }
    public float GetTargetingSpeed { get { return ShipStat.TargetingSpeed; } }
    #endregion

    #region StatSetter
    public float SetMaxSpeed
    {
        set
        {
            ShipStat.MaxSpeed = value;
        }
    }

    public float SetSpeedAcceleration
    {
        set
        {
            ShipStat.SpeedAcceleration = value;
        }
    }

    public float SetRotAcceleration
    {
        set
        {
            ShipStat.RotAcceleration = value;
        }
    }

    public float SetTargetingSpeed
    {
        set
        {
            ShipStat.TargetingSpeed = value;
        }
    }

    public float SetMobility
    {
        set
        {
            ShipStat.Mobility = value;
        }
    }

    public float SetHitpoints
    {
        set
        {
            ShipStat.HitPoints = value;
        }
    }

    public float SetArmor
    {
        set
        {
            ShipStat.Armor = value;
        }
    }

    public float SetPower
    {
        set
        {
            ShipStat.PowerGrid = value;
        }
    }

    public float SetPowerRecover
    {
        set
        {
            ShipStat.PowerRecoverFigure = value;
        }
    }
    #endregion
}
