﻿using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine;
using System;

public sealed class EnemyTargeting : MonoBehaviour, IPointerClickHandler
{
    public GameStateMng gameStateMng;
    public ShipBase EnemyShipInstance; // 타겟팅 될 함선
    public Transform TargetingImage;  
    public Transform Target;
    private bool isTargeted;
    private static event EventHandler targetingActivated;

    IEnumerator Start()
    {
        yield return new WaitForFixedUpdate();
        gameStateMng = EnemyShipInstance.GetGameStateMng;
        // 타겟이 격파되면 자동으로 타겟지정을 해제한다.
        EnemyShipInstance.DownEvent += (sender, e) =>
        {
            isTargeted = false;
            TargetingImage.gameObject.SetActive(false);
            if (gameStateMng.GetCharShipStateMng.targetedShip)
                if (gameStateMng.GetCharShipStateMng.targetedShip.Equals(EnemyShipInstance))
                    gameStateMng.GetCharShipStateMng.targetedShip = null;
        };

        // 먼저 타겟팅된 함선이 있다면 이전에 활성화된 타겟을 지운다.
        targetingActivated += (sender, e) =>
        {
            if (isTargeted)
            {
                isTargeted = false;
                TargetingImage.gameObject.SetActive(false);
                gameStateMng.GetCharShipStateMng.targetedShip = null;
            }
        };
    }

    // 적 함선 이미지를 클릭하면 타겟팅을한다.
    public void OnPointerClick(PointerEventData eventData)
    {
        if (!isTargeted)
        {
            targetingActivated(this, new EventArgs());
            isTargeted = true;
            TargetingImage.gameObject.SetActive(true);
            StartCoroutine(AnimatedTaretingImage());
        }
        else
        {
            isTargeted = false;
            TargetingImage.gameObject.SetActive(false);
            gameStateMng.GetCharShipStateMng.targetedShip = null;
        }
    }

    private IEnumerator AnimatedTaretingImage()
    {
        float t = 0f;
        Vector3 start = new Vector3(1.8f, 1.8f);
        Vector3 end = Vector3.one;
        while (t <= 1f)
        {
            if (!isTargeted)
                yield break;
            t += gameStateMng.GetCharShipStateMng.GetShipStat.GetTargetingSpeed * Time.deltaTime;
            TargetingImage.localScale = Vector3.Lerp(start, end, t);
            yield return null;
        }
        yield return new WaitForFixedUpdate();
        if (!isTargeted)
            yield break;
        gameStateMng.GetCharShipStateMng.targetedShip = EnemyShipInstance;
    }
}
