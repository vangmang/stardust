﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

public class EnemyTankTankingState : State<EnemyTank> {

    public override Enum GetState
    {
        get
        {
            return EnemyTank.EnemyTank_State.tanking;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        StartCoroutine(TankingAI());
    }

    private IEnumerator TankingAI()
    {
        if (!instance.IsShipEngaging)
            yield break;
        instance.IsShipTanking = true;
        yield return new WaitForSeconds(instance.TankingActiveTime);
        instance.IsShipTanking = false;
        yield return new WaitForSeconds(instance.TankingActiveTime * 0.67f);
        StartCoroutine(TankingAI());
    }

    public override void Execute()
    {

    }

    public override void Exit()
    {
        instance.IsShipTanking = false;
    }
}
