﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class EnemyTankEnterState : State<EnemyTank> {

    public override Enum GetState
    {
        get
        {
            return EnemyTank.EnemyTank_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.ChangeState(EnemyTank.EnemyTank_State.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
