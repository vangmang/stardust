﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class EnemyTankMoveState : State<EnemyTank> {

    public override Enum GetState
    {
        get
        {
            return EnemyTank.EnemyTank_State.move;
        }
    }

    private float keepingDistance;
    float trailSpeed = 0.8f;

    public override void Enter(params object[] o_Params)
    {
        keepingDistance = 200f;
    }

    // TODO: 그냥 볼 것
    // AI 로직
    // 1) 탱킹이 활성화 되지않았으면 캐리어처럼 거리를 유지한다.
    // 2) 탱킹이 활성화 되면 캐릭터를 계속 바라보고 움직인다. 캐릭터와 충각 유도
    public override void Execute()
    {
        //----------------------AI 부분----------------------//
        Vector3 destination = (!instance.IsShipTanking) ?
                        instance.GetGameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.localPosition +
                        instance.GetGameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.up * keepingDistance :
                        instance.GetGameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.localPosition;
        Vector3 direction = (destination - instance.ShipInfo.ShipTransform.position).normalized;
        float rot = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;

        float distance =
            Vector3.Distance(instance.GetGameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.localPosition, instance.ShipInfo.ShipTransform.localPosition);
        bool isAbleToMove = (distance <= keepingDistance || distance >= keepingDistance * 2.5f) || instance.IsShipTanking;
        if (isAbleToMove)
            instance.currentShipSpeed =
                Mathf.Lerp(instance.currentShipSpeed,
                           (instance.IsShipTanking) ? instance.GetShipStat.GetMaxSpeed : instance.GetShipStat.GetMaxSpeed * 0.25f,
                           instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);
        else
            instance.currentShipSpeed =
                Mathf.Lerp(instance.currentShipSpeed, 0f, instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);
        //---------------------------------------------------//
        instance.EnemyTankEffect.ThrusterTrail.time =
            Mathf.Lerp(instance.EnemyTankEffect.ThrusterTrail.time, isAbleToMove ? trailSpeed : 0f, instance.GetShipStat.GetSpeedAcceleration * 12.5f * Time.deltaTime);
        instance.EnemyTankEffect.ThrusterLens.brightness =
            Mathf.Lerp(instance.EnemyTankEffect.ThrusterLens.brightness, isAbleToMove ? 1f : 0f, instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);

        var wantedRot = Quaternion.Euler(0f, 0f, rot);
        instance.currentShipRot = rot;
        instance.ShipInfo.ShipTransform.localRotation =
            Quaternion.Slerp(instance.ShipInfo.ShipTransform.localRotation,
            wantedRot,
            instance.GetShipStat.GetRotAcceleration * Time.deltaTime);
        instance.ShipInfo.ShipTransform.position +=
            instance.ShipInfo.ShipTransform.up * instance.currentShipSpeed * Time.deltaTime;
    }

    public override void Exit()
    {
        StartCoroutine(instance.CancelShipSailing());
        StartCoroutine(instance.CancelShipRotation());
    }
}
