﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

public class EnemyTankAttackState : State<EnemyTank> {

    public override Enum GetState
    {
        get
        {
            return EnemyTank.EnemyTank_State.attack;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        foreach (ModuleBase module in instance.OffensiveModules)
        {
            Enum state = default(Enum);
            switch (instance.GetWeaponType)
            {
                case WeaponType.projectile: state = ProjectileModule.Projectile_State.active; break;
                case WeaponType.plasma: state = PlasmaModule.Plasma_State.active; break;
                case WeaponType.hyprid: state = HybridModule.Hybrid_State.charge; break;
                default: break;
            }
            module.ChangeState(state);
        }
        StartCoroutine(EnableTanking());
    }

    private IEnumerator EnableTanking()
    {
        yield return new WaitForSeconds(instance.TankingActiveTime);
        instance.SubState(EnemyTank.EnemyTank_State.tanking);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
        foreach (ModuleBase module in instance.OffensiveModules)
        {
            Enum state = default(Enum);
            switch (instance.GetWeaponType)
            {
                case WeaponType.projectile: state = ProjectileModule.Projectile_State.idle; break;
                case WeaponType.plasma: state = PlasmaModule.Plasma_State.idle; break;
                case WeaponType.hyprid: state = HybridModule.Hybrid_State.idle; break;
                default: break;
            }
            module.ChangeState(state);
        }
    }
}
