﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class EnemyTankIdleState : State<EnemyTank> {

    public override Enum GetState
    {
        get
        {
            return EnemyTank.EnemyTank_State.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
