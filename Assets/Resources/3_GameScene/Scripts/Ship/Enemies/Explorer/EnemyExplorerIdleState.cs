﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class EnemyExplorerIdleState : State<EnemyExplorer> {

    public override Enum GetState
    {
        get
        {
            return EnemyExplorer.EnemyExplorer_State.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
