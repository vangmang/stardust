﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class EnemyExplorerMoveState : State<EnemyExplorer>
{

    public override Enum GetState
    {
        get
        {
            return EnemyExplorer.EnemyExplorer_State.move;
        }
    }

    float trailSpeed = 0.5f;

    public override void Enter(params object[] o_Params)
    {
    }

    // TODO: AI를 짤 것
    public override void Execute()
    {
        //----------------------AI 부분----------------------//
        Vector3 destination = instance.GetGameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.localPosition;
        Vector3 direction = (destination - instance.ShipInfo.ShipTransform.position).normalized;
        float rot = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;

        float distance =
            Vector3.Distance(instance.GetGameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.localPosition, instance.ShipInfo.ShipTransform.localPosition);
        //---------------------------------------------------//

        instance.EnemyExplorerEffect.ThrusterTrail.time =
            Mathf.Lerp(instance.EnemyExplorerEffect.ThrusterTrail.time, trailSpeed, instance.GetShipStat.GetSpeedAcceleration * 12.5f * Time.deltaTime);
        instance.EnemyExplorerEffect.ThrusterLens.brightness =
            Mathf.Lerp(instance.EnemyExplorerEffect.ThrusterLens.brightness, 1f, instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);

        var wantedRot = Quaternion.Euler(0f, 0f, rot);
        instance.currentShipRot = rot;
        instance.currentShipSpeed =
            Mathf.Lerp(instance.currentShipSpeed, instance.GetShipStat.GetMaxSpeed, instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);
        instance.ShipInfo.ShipTransform.localRotation =
            Quaternion.Slerp(instance.ShipInfo.ShipTransform.localRotation,
            wantedRot,
            instance.GetShipStat.GetRotAcceleration * Time.deltaTime);
        instance.ShipInfo.ShipTransform.position +=
            instance.ShipInfo.ShipTransform.up * instance.currentShipSpeed * Time.deltaTime;
    }

    public override void Exit()
    {
        StartCoroutine(instance.CancelShipSailing());
        StartCoroutine(instance.CancelShipRotation());
    }
}
