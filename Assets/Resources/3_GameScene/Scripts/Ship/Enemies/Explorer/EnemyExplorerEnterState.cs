﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class EnemyExplorerEnterState : State<EnemyExplorer> {

    public override Enum GetState
    {
        get
        {
            return EnemyExplorer.EnemyExplorer_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
