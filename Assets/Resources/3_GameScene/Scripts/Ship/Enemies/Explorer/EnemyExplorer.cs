﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyExplorer : EnemyShipBase<EnemyExplorer>, 
    IOffenseShip<EnemyExplorer>,
    IDefenseShip<EnemyExplorer>,
    IUtilityShip<EnemyExplorer>
{
    public enum EnemyExplorer_State
    {
        enter,
        idle,
        attack,
        tanking,
        move,
        down
    }

    [System.Serializable]
    public struct _EnemyExplorerEffect_
    {
        [SerializeField]
        private LensFlare thrusterLens;
        [SerializeField]
        private LensFlare downLens;
        [SerializeField]
        private TrailRenderer thrusterTrail;

        public LensFlare DownLens { get { return downLens; } }
        public LensFlare ThrusterLens { get { return thrusterLens; } }
        public TrailRenderer ThrusterTrail { get { return thrusterTrail; } }
    }

    public _EnemyExplorerEffect_ EnemyExplorerEffect;

    // 적들의 경우 에디터상에서 조정되어야 하므로 인스펙터에 표시한다.
    [SerializeField]
    private WeaponType weaponType;
    [SerializeField]
    private Transform projectileContainer;
    [SerializeField]
    private OffensiveModule[] offensiveModules;
    [SerializeField]
    private ModuleBase[] defensiveModules;
    [SerializeField]
    private ModuleBase[] utilityModules;

    public Transform ProjectileContainer { get { return projectileContainer; } }
    public bool IsShipEngaging { get; private set; }
    public override ShipType GetShipType { get { return ShipType.explorer; } }
    public WeaponType GetWeaponType { get { return weaponType; } set { weaponType = value; } }

    public bool IsSelfDestruction;
    public override EnemyExplorer Instance { get { return this; } }

    public IEnumerable<IOffensiveModule> OffensiveModules { get { return offensiveModules; } }
    public IEnumerable<IModule> DefensiveModules { get { return defensiveModules; } }
    public IEnumerable<IModule> UtilityModules { get { return utilityModules; } }

    void Awake()
    {
        Init();
        InitShip();
        InitializeState();
    }

    void Start()
    {
        StateEnter(EnemyExplorer_State.enter);
        CreateIndicator();
    }

    public override void Init()
    {
        engageDistance = 750f;
        engageEnableDegree = 30f;
        CreateObjOnMinimap("Map/Map_EnemyExplorer");
    }

    public IEnumerator ActivePower(bool compulsory)
    {
        float t = 0f;
        while (t <= 0.75f)
        {
            if (IsShipEngaging)
                yield break;
            t += Time.deltaTime;
            yield return null;
        }
        isAbleToRecoverPower = true;
    }

    void Update()
    {
        Execute(); float curDist = 0f;
        float curDeg = 0f;
        isDetected = CheckDetection(out curDist, out curDeg) || engageEnbale;
        if (!IsShipDown)
        {
            if (isDetected)
            {
                IsShipEngaging = true;
                ChangeState(EnemyExplorer_State.move, false, curDeg);
                SubState(EnemyExplorer_State.attack);
            }
            if (!IsShipEngaging)
            {
                ChangeState(EnemyExplorer_State.idle, false);
            }
        }
    }

    public override void TakeDamage(ICharges causer)
    {
        base.TakeDamage(causer);
        if (GetCurrentState.Equals(EnemyExplorer_State.idle))
            engageEnbale = true;
    }

    public override void Down(ICharges causer)
    {
        base.Down(causer);
        IsShipMoving = false;
        IsShipWarping = false;
        IsShipEngaging = false;
        RemoveAllState();
        ChangeState(EnemyExplorer_State.down);
    }

    public override IEnumerator CancelShipSailing()
    {
        IsShipMoving = false;
        float trailStart = EnemyExplorerEffect.ThrusterTrail.time;
        float startSpeed = currentShipSpeed;
        float brightness = EnemyExplorerEffect.ThrusterLens.brightness;
        float t = 0f;

        while (t <= 0.99f)
        {
            t += GetShipStat.GetSpeedAcceleration * Time.deltaTime;
            EnemyExplorerEffect.ThrusterTrail.time = Mathf.Lerp(trailStart, 0f, t);
            currentShipSpeed = Mathf.Lerp(startSpeed, 0f, t);
            EnemyExplorerEffect.ThrusterLens.brightness = Mathf.Lerp(brightness, 0f, t);
            ShipInfo.ShipTransform.localPosition += ShipInfo.ShipTransform.up * currentShipSpeed * GetShipStat.GetMobility * Time.deltaTime;
            yield return null;
            if (IsShipMoving)
                yield break;
        }
        IsReadyToWarp = true;
    }

    void OnDestroy()
    {

    }
}
