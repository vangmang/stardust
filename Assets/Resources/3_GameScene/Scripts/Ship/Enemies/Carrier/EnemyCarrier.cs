﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class EnemyCarrier : EnemyShipBase<EnemyCarrier>,
    IDefenseShip<EnemyCarrier>,
    IUtilityShip<EnemyCarrier>
{
    public enum EnemyCarrier_State
    {
        enter,
        idle,
        tanking,
        droneLaunch,
        droneCollect,
        move,
        down
    }

    // 적들의 경우 에디터상에서 조정되어야 하므로 인스펙터에 표시한다.
    [SerializeField]
    private DroneStateMng[] drones;
    [SerializeField]
    private ModuleBase[] defensiveModules;
    [SerializeField]
    private ModuleBase[] utilityModules;

    public override ShipType GetShipType { get { return ShipType.carrier; } }
    public override EnemyCarrier Instance { get { return this; } }

    [System.Serializable]
    public struct _EnemyCarrierEffect_
    {
        [SerializeField]
        private LensFlare thrusterLens1;
        [SerializeField]
        private LensFlare thrusterLens2;
        [SerializeField]
        private LensFlare downLens;
        [SerializeField]
        private TrailRenderer thrusterTrail1;
        [SerializeField]
        private TrailRenderer thrusterTrail2;

        public LensFlare DownLens { get { return downLens; } }
        public LensFlare ThrusterLens1 { get { return thrusterLens1; } }
        public LensFlare ThrusterLens2 { get { return thrusterLens2; } }
        public TrailRenderer ThrusterTrail1 { get { return thrusterTrail1; } }
        public TrailRenderer ThrusterTrail2 { get { return thrusterTrail2; } }
    }

    public _EnemyCarrierEffect_ EnemyCarrierEffect;

    public int DroneCount { get; set; }
    public bool IsShipEngaging { get; private set; }
    public DroneStateMng[] Drones { get { return drones; } }
    public IEnumerable<IModule> DefensiveModules { get { return defensiveModules; } }
    public IEnumerable<IModule> UtilityModules { get { return utilityModules; } }


    void Awake()
    {
        Init();
        InitShip();
        InitializeState();
    }

    void Start()
    {
        StateEnter(EnemyCarrier_State.enter);
        StartCoroutine(GenerateDrones());
        CreateIndicator();
    }

    public override void Init()
    {
        engageDistance = 750f;
        engageEnableDegree = 30f;
    }

    private IEnumerator GenerateDrones()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("MainCanvas/World/");
        sb.Append(GameStateMng.Instance.StageInfo.StageTransformName).Append("/");
        sb.Append(ShipInfo.ShipTransform.parent.name).Append("/Drones");
        drones = new DroneStateMng[DroneCount];
        yield return new WaitForFixedUpdate();

        for (int i = 0; i < DroneCount; i++)
        {
            drones[i] =
                Mng.Instance.CreatePrefab(sb.ToString(), "DroneContainer", E_RESOURCES.E_3_GAMESCENE, Vector3.zero, Quaternion.identity, "Drone" + (i + 1)).GetComponentInChildren<DroneStateMng>();
            drones[i].SetMotherShipInstance = this;
            drones[i].Target = GetGameStateMng.GetCharShipStateMng.Instance;
        }
        CreateObjOnMinimap("Map/Map_EnemyCarrier");
    }

    void Update()
    {
        Execute();
        float curDist = 0f;
        float curDeg = 0f;
        isDetected = CheckDetection(out curDist, out curDeg) || engageEnbale;
        if (!IsShipDown)
        {
            if (isDetected)
            {
                if (!IsShipEngaging)
                    SubState(EnemyCarrier_State.droneLaunch);
                IsShipEngaging = true;
                ChangeState(EnemyCarrier_State.move, false, curDeg);
            }
            if (!IsShipEngaging)
            {
                ChangeState(EnemyCarrier_State.idle, false);
            }
        }
    }

    public override void TakeDamage(ICharges causer)
    {
        base.TakeDamage(causer);
        if (GetCurrentState.Equals(EnemyCarrier_State.idle))
            engageEnbale = true;
    }

    public override void Down(ICharges causer)
    {
        base.Down(causer);
        IsShipMoving = false;
        IsShipWarping = false;
        IsShipEngaging = false;
        RemoveAllState();
        ChangeState(EnemyCarrier_State.down);
    }

    public override IEnumerator CancelShipSailing()
    {
        IsShipMoving = false;
        float trailStart1 = EnemyCarrierEffect.ThrusterTrail1.time;
        float brightness1 = EnemyCarrierEffect.ThrusterLens1.brightness;
        float trailStart2 = EnemyCarrierEffect.ThrusterTrail2.time;
        float brightness2 = EnemyCarrierEffect.ThrusterLens2.brightness;
        float startSpeed = currentShipSpeed;
        float t = 0f;

        while (t <= 0.99f)
        {
            t += GetShipStat.GetSpeedAcceleration * Time.deltaTime;
            EnemyCarrierEffect.ThrusterTrail1.time = Mathf.Lerp(trailStart1, 0f, t);
            EnemyCarrierEffect.ThrusterLens1.brightness = Mathf.Lerp(brightness1, 0f, t);
            EnemyCarrierEffect.ThrusterTrail2.time = Mathf.Lerp(trailStart2, 0f, t);
            EnemyCarrierEffect.ThrusterLens2.brightness = Mathf.Lerp(brightness2, 0f, t);
            currentShipSpeed = Mathf.Lerp(startSpeed, 0f, t);
            ShipInfo.ShipTransform.localPosition += ShipInfo.ShipTransform.up * currentShipSpeed * GetShipStat.GetMobility * Time.deltaTime;
            yield return null;
            if (IsShipMoving)
                yield break;
        }
        IsReadyToWarp = true;
    }

    void OnDestroy()
    {

    }

}
