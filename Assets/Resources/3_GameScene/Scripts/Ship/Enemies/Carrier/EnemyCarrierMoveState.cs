﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class EnemyCarrierMoveState : State<EnemyCarrier> {

    public override Enum GetState
    {
        get
        {
            return EnemyCarrier.EnemyCarrier_State.move;
        }
    }

    private float keepingDistance;
    private Vector3 direction;
    private float rot;

    float trailSpeed = 1.7f;

    public override void Enter(params object[] o_Params)
    {
        keepingDistance = 500f;
        rot = instance.ShipInfo.ShipTransform.localRotation.z;
    }

    // 그냥 볼 것 
    // AI 로직
    // 1) 캐릭터와 계속 일정거리를 유지한다.
    public override void Execute()
    {
        //----------------------AI 부분----------------------//
        Vector3 destination = 
            instance.GetGameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.localPosition +
            instance.GetGameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.up *
             keepingDistance;
        float distance = 
            Vector3.Distance(instance.GetGameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.localPosition, instance.ShipInfo.ShipTransform.localPosition);
        bool isEnableToMove = distance <= keepingDistance || distance >= keepingDistance * 2.5f;
        if (isEnableToMove) 
        {
            direction = (destination - instance.ShipInfo.ShipTransform.localPosition).normalized;
            rot = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
            instance.currentShipSpeed = 
                Mathf.Lerp(instance.currentShipSpeed, instance.GetShipStat.GetMaxSpeed, instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);
        }
        else
        {
            instance.currentShipSpeed = 
                Mathf.Lerp(instance.currentShipSpeed, 0f, instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);
        }
        //---------------------------------------------------//
        instance.EnemyCarrierEffect.ThrusterTrail1.time =
            Mathf.Lerp(instance.EnemyCarrierEffect.ThrusterTrail1.time, isEnableToMove ? trailSpeed : 0f, instance.GetShipStat.GetSpeedAcceleration * 12.5f * Time.deltaTime);
        instance.EnemyCarrierEffect.ThrusterTrail2.time =
            Mathf.Lerp(instance.EnemyCarrierEffect.ThrusterTrail2.time, isEnableToMove ? trailSpeed : 0f, instance.GetShipStat.GetSpeedAcceleration * 12.5f * Time.deltaTime);
        instance.EnemyCarrierEffect.ThrusterLens1.brightness =
            Mathf.Lerp(instance.EnemyCarrierEffect.ThrusterLens1.brightness, isEnableToMove ? 1.2f : 0f, instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);
        instance.EnemyCarrierEffect.ThrusterLens2.brightness =
            Mathf.Lerp(instance.EnemyCarrierEffect.ThrusterLens2.brightness, isEnableToMove ? 1.2f : 0f, instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);


        var wantedRot = Quaternion.Euler(0f, 0f, rot);
        instance.currentShipRot = rot;
        instance.ShipInfo.ShipTransform.localRotation = 
            Quaternion.Slerp(instance.ShipInfo.ShipTransform.localRotation, wantedRot, instance.GetShipStat.GetRotAcceleration * Time.deltaTime);
        instance.ShipInfo.ShipTransform.localPosition +=
            instance.ShipInfo.ShipTransform.up * instance.currentShipSpeed * Time.deltaTime;
    }

    public override void Exit()
    {
        StartCoroutine(instance.CancelShipSailing());
        StartCoroutine(instance.CancelShipRotation());
    }
}
