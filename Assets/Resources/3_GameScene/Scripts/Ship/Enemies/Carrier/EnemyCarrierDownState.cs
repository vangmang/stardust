﻿using System;
using GameStateLibrary.State;
using UnityEngine;
using System.Collections;

public class EnemyCarrierDownState : State<EnemyCarrier> {

    public override Enum GetState
    {
        get
        {
            return EnemyCarrier.EnemyCarrier_State.down;
        }
    }
    private Vector3 direction;
    private float currentSpeed;

    public override void Enter(params object[] o_Params)
    {
        direction = instance.ShipInfo.ShipTransform.up;
        currentSpeed = instance.currentShipSpeed;
        instance.EnemyCarrierEffect.DownLens.brightness = 10f;
        instance.ShipInfo.ShipImage.gameObject.SetActive(false);
        instance.ShipVolumeCollider.enabled = false;
        StartCoroutine(Down());
        StartCoroutine(DownEffect());
    }

    private IEnumerator Down()
    {
        float t = 0f;
        float revValue = 1f;
        float startSpeed = currentSpeed;

        while (t <= 0.99f)
        {
            revValue = Mathf.Lerp(1f, 0f, t);
            t += ShipBase.DownEffectSpeed * revValue * Time.deltaTime;
            currentSpeed = Mathf.Lerp(startSpeed, 0f, t);
            instance.ShipInfo.ShipTransform.localPosition += direction * currentSpeed * Time.deltaTime;
            yield return null;
        }
    }

    private IEnumerator DownEffect()
    {
        yield return new WaitForSeconds(1.2f);
        float t = 0f;
        float revValue = 1f;
        float start = instance.EnemyCarrierEffect.DownLens.brightness;
        float thruster1 = instance.EnemyCarrierEffect.ThrusterLens1.brightness;
        float trail1 = instance.EnemyCarrierEffect.ThrusterTrail1.time;
        float thruster2 = instance.EnemyCarrierEffect.ThrusterLens2.brightness;
        float trail2 = instance.EnemyCarrierEffect.ThrusterTrail2.time;

        while (t <= 0.99f)
        {
            revValue = Mathf.Lerp(1f, 0f, t);
            t += (ShipBase.DownEffectSpeed * 2f) * revValue * Time.deltaTime;
            instance.EnemyCarrierEffect.ThrusterLens1.brightness = Mathf.Lerp(thruster1, 0f, t * 2f);
            instance.EnemyCarrierEffect.ThrusterTrail1.time = Mathf.Lerp(trail1, 0f, t * 3f);
            instance.EnemyCarrierEffect.ThrusterLens2.brightness = Mathf.Lerp(thruster2, 0f, t * 2f);
            instance.EnemyCarrierEffect.ThrusterTrail2.time = Mathf.Lerp(trail2, 0f, t * 3f);
            instance.EnemyCarrierEffect.DownLens.brightness = Mathf.Lerp(start, 0f, t);
            yield return null;
        }
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
