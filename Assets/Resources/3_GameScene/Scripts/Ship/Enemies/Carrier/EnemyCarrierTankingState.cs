﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class EnemyCarrierTankingState : State<EnemyCarrier> {

    public override Enum GetState
    {
        get
        {
            return EnemyCarrier.EnemyCarrier_State.tanking;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
