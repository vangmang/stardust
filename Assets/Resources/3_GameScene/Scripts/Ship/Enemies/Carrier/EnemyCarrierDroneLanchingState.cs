﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

public class EnemyCarrierDroneLanchingState : State<EnemyCarrier> {

    public override Enum GetState
    {
        get
        {
            return EnemyCarrier.EnemyCarrier_State.droneLaunch;
        }
    }

    public override void Enter(params object[] o_Params)
    { 
        StartCoroutine(Launching());
    }

    private IEnumerator Launching()
    {
        for (int i = 0; i < instance.DroneCount; i++)
        {
            if (instance.IsShipDown)
            {
                instance.RemoveState(EnemyCarrier.EnemyCarrier_State.droneLaunch);
                yield break;
            }
            instance.Drones[i].ChangeState(DroneStateMng.Drone_State.launcehd);
            yield return new WaitForSeconds(0.5f);
        }
        instance.RemoveState(EnemyCarrier.EnemyCarrier_State.droneLaunch);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
