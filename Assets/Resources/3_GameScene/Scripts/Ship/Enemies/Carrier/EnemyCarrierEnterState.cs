﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class EnemyCarrierEnterState : State<EnemyCarrier> {

    public override Enum GetState
    {
        get
        {
            return EnemyCarrier.EnemyCarrier_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
