﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class EnemyAssaultIdleState : State<EnemyAssault> {

    public override Enum GetState
    {
        get
        {
            return EnemyAssault.EnemyAssault_State.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.RemoveAllState();
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
