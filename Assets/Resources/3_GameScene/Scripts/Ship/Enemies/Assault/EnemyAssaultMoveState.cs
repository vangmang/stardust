﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

public class EnemyAssaultMoveState : State<EnemyAssault>
{

    private float originRotAccel;
    private float encounterDeg;
    private bool encounter;
    private Vector3 destination;
    private Transform CharTransform;

    //private bool isDegLimit;
    private bool isAIEnable;
    private bool isArrived;
    private IEnumerator _RecoverRotAccel;
    private float arrivalPoint;
    private float AI_initTime;
    public override Enum GetState
    {
        get
        {
            return EnemyAssault.EnemyAssault_State.move;
        }
    }

    float trailSpeed = 0.5f;

    public override void Enter(params object[] o_Params)
    {
        instance.IsShipMoving = true;
        AI_initTime = 0f;
        encounterDeg = (float)o_Params.GetValue(0);
        encounter = encounterDeg > -90f && encounterDeg > -180f && encounterDeg < 90f; // Don't fucking change this
        CharTransform = instance.GetGameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform;
        instance.EnemyAssaultEffect.ThrusterTrail.time = 0f;
        originRotAccel = instance.GetShipStat.GetRotAcceleration;
        isAIEnable = encounter;
        if (!encounter)
        {
            instance.GetShipStat.SetRotAcceleration = instance.GetShipStat.GetRotAcceleration * 0.75f;
            if (encounterDeg > -180f && encounterDeg > -90f)
                destination = CharTransform.right * -450f + CharTransform.up * -500f;
            else
                destination = CharTransform.right * 450f + CharTransform.up * -500f;
            _RecoverRotAccel = RecoverRotAccel(0.5f);
        }
        else
        {
            instance.GetShipStat.SetRotAcceleration = instance.GetShipStat.GetRotAcceleration * 0.4f;
            if (encounterDeg > -90f && encounterDeg < 0)
                destination = CharTransform.right * -650f + CharTransform.up * 150f;
            else
                destination = CharTransform.right * 650f + CharTransform.up * 150f;
            _RecoverRotAccel = RecoverRotAccel(0.85f);
        }
        destination += CharTransform.localPosition;
        arrivalPoint = Vector3.Distance(destination, instance.ShipInfo.ShipTransform.localPosition) * 0.3f;
        StartCoroutine(_RecoverRotAccel);
    }

    private IEnumerator RecoverRotAccel(float speed)
    {
        float t = 0f;
        float start = instance.GetShipStat.GetRotAcceleration;
        while (t <= 1f)
        {
            t += speed * Time.deltaTime;
            instance.GetShipStat.SetRotAcceleration = Mathf.Lerp(start, originRotAccel, t);
            yield return null;
        }
    }

    // TODO: 그냥 볼 것
    // AI 로직
    // 1) 목적지를 바라보고 계속 이동한다.
    // 2) 목적지는 계속 갱신된다.
    // 3) 목적지에 도달하면 현재 각도를 계산하여 각도에 해당하는 목적지로 다시 이동한다
    // 4) 루프
    public override void Execute()
    {
        instance.EnemyAssaultEffect.ThrusterTrail.time =
            Mathf.Lerp(instance.EnemyAssaultEffect.ThrusterTrail.time, trailSpeed, instance.GetShipStat.GetSpeedAcceleration * 12.5f * Time.deltaTime);
        instance.EnemyAssaultEffect.ThrusterLens.brightness =
            Mathf.Lerp(instance.EnemyAssaultEffect.ThrusterLens.brightness, 1f, instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);

        //----------------------AI 부분----------------------//
        Vector3 point = instance.ShipInfo.ShipTransform.InverseTransformPoint(CharTransform.localPosition);
        float curDeg = Mathf.Atan2(point.x, point.y) * Mathf.Rad2Deg;

        float deg = 0f;
        Vector3 direction;

        if (isArrived)
        {
            destination = CharTransform.localPosition + CharTransform.right * Mathf.Clamp(curDeg * 6f, -300f, 300f) + 
                          CharTransform.up * CharShipStateMng.GetInstance.currentShipSpeed * 0.375f;
            arrivalPoint = Vector3.Distance(destination, instance.ShipInfo.ShipTransform.localPosition) * 0.25f;
            AI_initTime = 0f;
            isArrived = false;
        }
        AI_initTime += Time.deltaTime;
        if (AI_initTime >= 0.25f)
            AI_initTime = 0.25f;

        float distance = Vector3.Distance(destination, instance.ShipInfo.ShipTransform.localPosition);
        if (!isAIEnable)
        {
            if (distance <= arrivalPoint)
            {
                isArrived = true;
                isAIEnable = true;
            }
        }
        else if (distance <= arrivalPoint || Mathf.Abs(curDeg) >= 135f)
        {
            if(AI_initTime >= 0.25f)
                isArrived = true;
        }
        //---------------------------------------------------//

        direction = (destination - instance.ShipInfo.ShipTransform.localPosition).normalized;
        deg = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        instance.ShipInfo.ShipTransform.localRotation =
            Quaternion.Slerp(instance.ShipInfo.ShipTransform.localRotation,
            Quaternion.Euler(0f, 0f, deg),
            instance.GetShipStat.GetRotAcceleration * Time.deltaTime);
        instance.currentShipRot = deg;
        instance.currentShipSpeed =
            Mathf.Lerp(instance.currentShipSpeed,
            instance.GetShipStat.GetMaxSpeed,
            instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);

        instance.ShipInfo.ShipTransform.localPosition +=
            instance.ShipInfo.ShipTransform.up * instance.currentShipSpeed * Time.deltaTime;
    }

    public override void Exit()
    {
        StartCoroutine(instance.CancelShipSailing());
        StartCoroutine(instance.CancelShipRotation());
    }
}
