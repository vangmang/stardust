﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class EnemyAssaultEnterState : State<EnemyAssault> {

    public override Enum GetState
    {
        get
        {
            return EnemyAssault.EnemyAssault_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.ChangeState(EnemyAssault.EnemyAssault_State.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
