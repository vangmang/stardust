﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class EnemyAssaultAttackState : State<EnemyAssault> {

    public override Enum GetState
    {
        get
        {
            return EnemyAssault.EnemyAssault_State.attack;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        foreach (ModuleBase module in instance.OffensiveModules)
        {
            Enum state = default(Enum);
            switch (instance.GetWeaponType)
            {
                case WeaponType.projectile: state = ProjectileModule.Projectile_State.active; break;
                case WeaponType.plasma: state = PlasmaModule.Plasma_State.active; break;
                case WeaponType.hyprid: state = HybridModule.Hybrid_State.charge; break;
                default: break;
            }
            module.ChangeState(state);
        }
    }

    public override void Execute()
    {
        //instance.CheckDisableEngaging();
    }

    public override void Exit()
    {
        foreach (ModuleBase module in instance.OffensiveModules)
        {
            Enum state = default(Enum);
            switch (instance.GetWeaponType)
            {
                case WeaponType.projectile: state = ProjectileModule.Projectile_State.idle; break;
                case WeaponType.plasma: state = PlasmaModule.Plasma_State.idle; break;
                case WeaponType.hyprid: state = HybridModule.Hybrid_State.idle; break;
                default: break;
            }
            module.ChangeState(state);
        }
    }
}
