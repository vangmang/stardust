﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class EnemyAssaultEvasionState : State<EnemyAssault> {

    public override Enum GetState
    {
        get
        {
            return EnemyAssault.EnemyAssault_State.evade;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
