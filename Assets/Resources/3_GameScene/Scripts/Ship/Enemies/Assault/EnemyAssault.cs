﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAssault : EnemyShipBase<EnemyAssault>,
    IOffenseShip<EnemyAssault>,
    IUtilityShip<EnemyAssault>
{
    public enum EnemyAssault_State
    {
        enter,
        idle,
        evade,
        attack,
        move,
        down
    }

    [Serializable]
    public struct _EnemyAssaultEffect_
    {
        [SerializeField]
        private LensFlare thrusterLens;
        [SerializeField]
        private LensFlare downLens;
        [SerializeField]
        private TrailRenderer thrusterTrail;

        public LensFlare DownLens { get { return downLens; } }
        public LensFlare ThrusterLens { get { return thrusterLens; } }
        public TrailRenderer ThrusterTrail { get { return thrusterTrail; } }
    }

    public _EnemyAssaultEffect_ EnemyAssaultEffect;

    [SerializeField]
    private Transform projectileContainer;
    // 적들의 경우 에디터상에서 조정되어야 하므로 인스펙터에 표시한다.
    [SerializeField]
    private WeaponType weaponType;
    [SerializeField]
    private OffensiveModule[] offensiveModules;
    [SerializeField]
    private ModuleBase[] utilityModules;
    public int Test;

    public Transform ProjectileContainer { get { return projectileContainer; } set { projectileContainer = value; } }
    public bool IsShipEngaging { get; private set; }

    public override ShipType GetShipType { get { return ShipType.assault; } }
    public WeaponType GetWeaponType { get { return weaponType; } set { weaponType = value; } }

    public override EnemyAssault Instance { get { return this; } }

    public IEnumerable<IOffensiveModule> OffensiveModules { get { return offensiveModules; } }
    public IEnumerable<IModule> UtilityModules { get { return utilityModules; } }

    void Awake()
    {
        Init();
        InitShip();
        InitializeState();
        //InitInteraction();
    }

    public override void InitInteraction()
    {
        interactionDistance = 750f;
        interactionEnableDegree = 30f;
        interactionDisableDistance = 2050f;
    }

    public override void Init()
    {
        engageDistance = 750f;
        engageEnableDegree = 30f;
        CreateObjOnMinimap("Map/Map_EnemyAssault");
    }

    void Start()
    {
        StateEnter(EnemyAssault_State.enter);
        CreateIndicator();
    }

    public IEnumerator ActivePower(bool compulsory)
    {
        float t = 0f;
        while (t <= 0.75f)
        {
            if (IsShipEngaging)
                yield break;
            t += Time.deltaTime;
            yield return null;
        }
        isAbleToRecoverPower = true;
    }

    void Update()
    {
        Execute();
        float curDist = 0f;
        float curDeg = 0f;
        isDetected = CheckDetection(out curDist, out curDeg) || engageEnbale; // 현재 각도, 거리가 주인공을 공격할 수 있는 지 체크
        if (!IsShipDown)
        {
            if (isDetected)
            {
                IsShipEngaging = true; // 교전 시작
                ChangeState(EnemyAssault_State.move, false, curDeg); // 이동 상태로 전이하면서 각도를 인자로 넘겨준다.
                SubState(EnemyAssault_State.attack); // 공격상태 추가
            }
            if (!IsShipEngaging)
            {
                ChangeState(EnemyAssault_State.idle, false); 
            }
        }
    }

    // 어느정도 거리를 벗어나면 공격을 중단한다.
    public void CheckDisableEngaging()
    {
        float distance =
            Vector3.Distance(ShipInfo.ShipTransform.localPosition, GetGameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.localPosition);
        // 교전거리를 벗어나면 공격을 중단한다.
        //if (distance >= EngageDisableDistance)
        //{
        //    IsShipEngaging = false;
        //    RemoveState(EnemyAssault_State.attack);
        //}
    }

    // TODO: 이벤트 처리 공부하고 볼 것!
    public override void TakeDamage(ICharges causer)
    {
        base.TakeDamage(causer);
        if (GetCurrentState.Equals(EnemyAssault_State.idle))
            engageEnbale = true;
    }

    // TODO: 이벤트 처리 공부하고 볼 것!
    public override void Down(ICharges causer)
    {
        base.Down(causer);
        IsShipMoving = false;   
        IsShipWarping = false;
        IsShipEngaging = false;
        RemoveAllState();
        ChangeState(EnemyAssault_State.down);
    }

    public override IEnumerator CancelShipSailing()
    {
        IsShipMoving = false;
        float trailStart = EnemyAssaultEffect.ThrusterTrail.time;
        float startSpeed = currentShipSpeed;
        float brightness = EnemyAssaultEffect.ThrusterLens.brightness;
        float t = 0f;

        while (t <= 0.99f)
        {
            t += GetShipStat.GetSpeedAcceleration * Time.deltaTime;
            EnemyAssaultEffect.ThrusterTrail.time = Mathf.Lerp(trailStart, 0f, t);
            currentShipSpeed = Mathf.Lerp(startSpeed, 0f, t);
            EnemyAssaultEffect.ThrusterLens.brightness = Mathf.Lerp(brightness, 0f, t);
            ShipInfo.ShipTransform.localPosition += ShipInfo.ShipTransform.up * currentShipSpeed * GetShipStat.GetMobility * Time.deltaTime;
            yield return null;
            if (IsShipMoving)
                yield break;
        }
        IsReadyToWarp = true;
    }

    void OnDestroy()
    {

    }
}
