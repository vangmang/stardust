﻿using System;
using GameStateLibrary.State;
using UnityEngine;
using System.Collections;

public class EnemyAssaultDownState : State<EnemyAssault>
{

    public override Enum GetState
    {
        get
        {
            return EnemyAssault.EnemyAssault_State.down;
        }
    }

    private Vector3 direction;
    private float currentSpeed;

    public override void Enter(params object[] o_Params)
    {
        direction = instance.ShipInfo.ShipTransform.up;
        currentSpeed = instance.currentShipSpeed;
        instance.EnemyAssaultEffect.DownLens.brightness = 3f;
        instance.ShipInfo.ShipImage.gameObject.SetActive(false);
        instance.ShipVolumeCollider.enabled = false;
        StartCoroutine(Down());
        StartCoroutine(DownEffect());
    }

    private IEnumerator Down()
    {
        float t = 0f;
        float revValue = 1f;
        float startSpeed = currentSpeed;

        while (t <= 0.99f)
        {
            revValue = Mathf.Lerp(1f, 0f, t);
            t += ShipBase.DownEffectSpeed * revValue * Time.deltaTime;
            currentSpeed = Mathf.Lerp(startSpeed, 0f, t);
            instance.ShipInfo.ShipTransform.localPosition += direction * currentSpeed * Time.deltaTime;
            yield return null;
        }
    }

    private IEnumerator DownEffect()
    {
        yield return new WaitForSeconds(1.2f);
        float t = 0f;
        float revValue = 1f;
        float start = instance.EnemyAssaultEffect.DownLens.brightness;
        float thruster = instance.EnemyAssaultEffect.ThrusterLens.brightness;
        float trail = instance.EnemyAssaultEffect.ThrusterTrail.time;

        while (t <= 0.99f)
        {
            revValue = Mathf.Lerp(1f, 0f, t);
            t += ShipBase.DownEffectSpeed * revValue * Time.deltaTime;
            instance.EnemyAssaultEffect.ThrusterLens.brightness = Mathf.Lerp(thruster, 0f, t * 2f);
            instance.EnemyAssaultEffect.ThrusterTrail.time = Mathf.Lerp(trail, 0f, t * 3f);
            instance.EnemyAssaultEffect.DownLens.brightness = Mathf.Lerp(start, 0f, t);
            yield return null;
        }
    }

    public override void Execute()
    {

    }

    public override void Exit()
    {
    }
}
