﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class CharShipMoveState : State<CharShipStateMng>
{
    public override Enum GetState
    {
        get
        {
            return CharShipStateMng.CharShip_State.move;
        }
    }

    float trailSpeed = 0.5f;

    public override void Enter(params object[] o_Params)
    {
        instance.CharShipEffect.ThrusterTrail.time = trailSpeed;
        instance.CharShipEffect.EngineBreakSound.Stop();
        instance.CharShipEffect.EngineStartSound.time = instance.CharShipEffect.currEngineStartSoundTime;
        instance.CharShipEffect.EngineStartSound.Play();
        StartCoroutine(instance.ControlBreakEngineSound());
    }

    public override void Execute()
    {
        instance.SailShip();
        instance.CharShipEffect.ThrusterTrail.time = 
            Mathf.Lerp(instance.CharShipEffect.ThrusterTrail.time, trailSpeed, instance.GetShipStat.GetSpeedAcceleration * 12.5f * Time.deltaTime);
        instance.ShipInfo.ShipTransform.localRotation =
            Quaternion.Slerp(instance.ShipInfo.ShipTransform.localRotation,
            Quaternion.Euler(0f, 0f, instance.currentShipRot),
            instance.GetShipStat.GetRotAcceleration * Time.deltaTime);
        instance.CharShipEffect.ThrusterLens.brightness =
            Mathf.Lerp(instance.CharShipEffect.ThrusterLens.brightness, 1f, instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);
    }
    
    public override void Exit()
    {
        instance.CharShipEffect.EngineStartSound.Stop();
        instance.CharShipEffect.EngineBreakSound.time = instance.CharShipEffect.currEngineBreakSoundTime;
        instance.CharShipEffect.EngineBreakSound.Play();

        StartCoroutine(instance.ControlStartEngineSound());
        StartCoroutine(instance.CancelShipRotation());
        StartCoroutine(instance.CancelShipSailing());
    }
}
