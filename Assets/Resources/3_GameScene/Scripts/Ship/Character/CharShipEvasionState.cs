﻿using GameStateLibrary.State;
using UnityEngine;
using System.Collections;
using System;

public class CharShipEvasionState : State<CharShipStateMng>
{

    public override Enum GetState
    {
        get
        {
            return CharShipStateMng.CharShip_State.evade;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        Vector3 direction = (Vector3)o_Params.GetValue(0);
        IEnumerator evasion = evade(direction);
        instance.ShipVolumeCollider.enabled = false;
        StartCoroutine(evasion);
    }

    private IEnumerator evade(Vector3 direction)
    {
        Vector3 start = instance.ShipInfo.ShipTransform.localPosition;
        Vector3 end = instance.ShipInfo.ShipTransform.localPosition + direction;

        Vector3 point = instance.ShipInfo.ShipTransform.InverseTransformPoint(end);
        float deg = Mathf.Atan2(point.x, point.y) * Mathf.Rad2Deg;
        // 캐릭터의 반대방향으로 회피기동은 불가능하다.
        if (Mathf.Abs(deg) <= 150f)
        {
            // 만약 어중간한 위치의 방향이 뒤쪽 방향과 비슷하다면 오른쪽 혹은 왼쪽으로 회피기동한다.
            if (Mathf.Abs(deg) >= 90f)
            {
                Vector3 clampDir = instance.ShipInfo.ShipTransform.right;
                if (deg < 0f)
                    clampDir *= -1f;
                end = instance.ShipInfo.ShipTransform.localPosition + clampDir * instance.EvasionDistance;
            }
            instance.isEvasionActivated = true;
            instance.currentPower -= 15f;
            instance.isAbleToRecoverPower = false;
            float t = 0f;
            while (t <= 1f)
            {
                t += instance.GetShipStat.GetSpeedAcceleration * 5f * Time.deltaTime;
                instance.ShipInfo.ShipTransform.localPosition =
                    Vector3.Lerp(start, end, t);
                yield return null;
            }
        }
        instance.isEvasionActivated = false;
        StartCoroutine(instance.ActivePower(true));
        instance.RemoveState(CharShipStateMng.CharShip_State.evade);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
        instance.ShipVolumeCollider.enabled = true;
    }
}
