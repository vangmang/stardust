﻿using GameStateLibrary.State;
using System.Collections;
using System;
using UnityEngine;

public class CharShipWarpState : State<CharShipStateMng> {

    public override Enum GetState
    {
        get
        {
            return CharShipStateMng.CharShip_State.warp;
        }
    }

    Vector3 destination;        // 목적지
    Quaternion directionRot;    // 워프 방향에 따른 회전
    float originSpeed;          // 워프 후 원래 스피드로
    float disableDistance;      // 워프 시퀀스가 종료되는 거리
    bool checkDistance;         // 워프 종료 코루틴을 시작하기 위한 필드

    public override void Enter(params object[] o_Params)
    {
        // 목적지를 인자로 받아오고 방향과 회전값을 구한다.
        instance.IsShipWarping = true;
        instance.ActivateShipWarp();
        destination = (Vector3)o_Params.GetValue(0);
        Vector3 direction = destination - instance.ShipInfo.ShipTransform.localPosition;
        float rot = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        directionRot = Quaternion.Euler(0f, 0f, rot);
        // 워프드라이브 코루틴을 시작한다.
        StartCoroutine(ActivateWarpDrive());
        checkDistance = false;
        disableDistance = direction.magnitude * 0.5f;
    }

    private IEnumerator ActivateWarpDrive()
    {
        // 워프가 시작될 수 있을 때까지 기다린다.
        yield return new WaitUntil(() => instance.IsReadyToWarp);
        originSpeed = instance.GetShipStat.GetMaxSpeed;
        float t = 0f;
        float revValue = 1f;
        var rot = instance.ShipInfo.ShipTransform.localRotation;
        instance.GetShipStat.SetMaxSpeed = 0f;
        instance.CharShipEffect.ThrusterTrail.gameObject.SetActive(false);

        while (t <= 0.99f)
        {
            revValue = Mathf.Lerp(1f, 0f, t);
            t += instance.GetShipStat.GetRotAcceleration * revValue * Time.deltaTime;
            instance.ShipInfo.ShipTransform.localRotation = Quaternion.Slerp(rot, directionRot, t);
            yield return null;
        }

        instance.CharShipEffect.ThrusterTrail.time = 0.5f;
        instance.CharShipEffect.WarpActiveSound.Play();
        t = 0f;
        revValue = 0.7f;
        float birghtness = instance.CharShipEffect.ThrusterLens.brightness;
        float end = 1f;
        float startMax = instance.GetShipStat.GetMaxSpeed;
        float endMax = originSpeed * 0.125f;

        while (t <= 0.99f)
        {
            revValue = Mathf.Lerp(0.7f, 0f, t);
            t += instance.GetShipStat.GetSpeedAcceleration * revValue * Time.deltaTime;
            instance.GetShipStat.SetMaxSpeed = Mathf.Lerp(startMax, endMax, t);
            instance.CharShipEffect.ThrusterLens.brightness = Mathf.Lerp(birghtness, end, t);
            yield return null;
        }
        instance.CharShipEffect.WarpthrusterIgnitionSound.Play();
        yield return new WaitForSeconds(instance.CharShipEffect.WarpthrusterIgnitionSound.clip.length * 0.5f);
        instance.GetShipStat.SetMaxSpeed = ShipStatMng.WarpSpeed;
        instance.CharShipEffect.ThrusterTrail.gameObject.SetActive(true);
    }

    private IEnumerator DisableWarpDrive()
    {        
        float t = 0f;
        float start = instance.GetShipStat.GetMaxSpeed;
        float curSpeed = instance.currentShipSpeed;
        float revValue = 1f;
        Vector3 startPos = instance.ShipInfo.ShipTransform.localPosition;
        while (t <= 0.997f)
        {
            revValue = Mathf.Lerp(1f, 0f, t);
            t += instance.GetShipStat.GetSpeedAcceleration * revValue * 0.5f * Time.deltaTime;
            instance.GetShipStat.SetMaxSpeed = Mathf.Lerp(start, originSpeed, t);
            instance.currentShipSpeed = Mathf.Lerp(curSpeed, instance.GetShipStat.GetMaxSpeed, t);
            instance.ShipInfo.ShipTransform.localPosition = Vector3.Lerp(startPos, destination, t);
            yield return null;
        }
        yield return new WaitForSeconds(1.75f);
        instance.ChangeState(CharShipStateMng.CharShip_State.idle);
        StartCoroutine(instance.CancelShipSailing());
    }

    public override void Execute()
    {
        instance.IsShipWarping = true;

        bool disable = Vector3.Distance(instance.ShipInfo.ShipTransform.localPosition, destination) <= disableDistance;
        if (!disable)
        {
            instance.currentShipSpeed = Mathf.Lerp(instance.currentShipSpeed, instance.GetShipStat.GetMaxSpeed, instance.GetShipStat.GetSpeedAcceleration * Time.deltaTime);
            instance.ShipInfo.ShipTransform.localPosition +=
                instance.ShipInfo.ShipTransform.up * instance.currentShipSpeed * instance.GetShipStat.GetMobility * Time.deltaTime;
        }
        else if (disable && !checkDistance)
        {
            checkDistance = true;
            StartCoroutine(DisableWarpDrive());
        }
    }

    public override void Exit()
    {
        instance.GetShipStat.SetMaxSpeed = originSpeed;
        instance.CharShipEffect.ThrusterTrail.time = 0f;
        instance.IsShipWarping = false;
    }
}
