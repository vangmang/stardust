﻿using System;
using GameStateLibrary.State;
public class CharShipIdleState : State<CharShipStateMng>
{

    public override Enum GetState
    {
        get
        {
            return CharShipStateMng.CharShip_State.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
