﻿using System;
using GameStateLibrary.State;

public class CharShipEnterState : State<CharShipStateMng>
{
    public override Enum GetState
    {
        get
        {
            return CharShipStateMng.CharShip_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.ChangeState(CharShipStateMng.CharShip_State.warp, instance.GetGameStateMng.StageLocation * 0.025f);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
