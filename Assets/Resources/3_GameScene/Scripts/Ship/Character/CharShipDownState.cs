﻿using System;
using GameStateLibrary.State;

public class CharShipDownState : State<CharShipStateMng>
{

    public override Enum GetState
    {
        get
        {
            return CharShipStateMng.CharShip_State.down;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
