﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;

public class CharShipStateMng : ShipBase<CharShipStateMng>,
    IInteraction<CharShipStateMng>,
    IOffenseShip<CharShipStateMng>,
    IDefenseShip<CharShipStateMng>,
    IUtilityShip<CharShipStateMng>
{
    private static CharShipStateMng m_instance = null;
    public static CharShipStateMng GetInstance
    {
        get
        {
            if (m_instance == null)
                m_instance = FindObjectOfType(typeof(CharShipStateMng)) as CharShipStateMng;
            return m_instance;
        }
    }

    public enum CharShip_State
    {
        enter,
        idle,
        evade,
        attack,
        move,
        down,
        warp
    }


    #region Fields
    [SerializeField]
    private Transform projectileContainer;
    [SerializeField]
    private CircleCollider2D charInteractionCollider;

    [Serializable]
    public struct _CharShipEffect_
    {
        [Header("----Effects----")]
        [SerializeField]
        private ParticleSystem backgroundParticle;  // 백그라운드 파티클
        [SerializeField]
        private LensFlare thrusterLens;             // 쓰러스터 렌즈플레어
        [SerializeField]        
        private TrailRenderer thrusterTrail;        // 쓰러스터 잔상효과
        [Header("----Sounds----")]
        [SerializeField]
        private AudioSource engineStartSound;       // 엔진 출력 사운드
        [SerializeField]
        private AudioSource engineBreakSound;       // 엔진 브레이크 사운드
        [SerializeField]
        private AudioSource engineSound;            // 엔진 가동 사운드
        [SerializeField]
        private AudioSource warpActiveSound;        // '워프 드라이브' 성우 사운드
        [SerializeField]
        private AudioSource warpthrusterIgnitionSound; // 워프 드라이브 초기화 사운드

        public Text VelocityText;                   // 속도를 표시해주는 텍스트
        public float currEngineStartSoundTime;      
        public float currEngineBreakSoundTime;
        public ParticleSystem BackgroundParticle { get { return backgroundParticle; } }
        public LensFlare ThrusterLens { get { return thrusterLens; } }
        public TrailRenderer ThrusterTrail { get { return thrusterTrail; } }
        public AudioSource EngineStartSound { get { return engineStartSound; } }
        public AudioSource EngineBreakSound { get { return engineBreakSound; } }
        public AudioSource EngineSound { get { return engineSound; } }
        public AudioSource WarpActiveSound { get { return warpActiveSound; } }
        public AudioSource WarpthrusterIgnitionSound { get { return warpthrusterIgnitionSound; } }
    }
    public _CharShipEffect_ CharShipEffect;

    private float interactionDistance;

    [SerializeField]
    private Transform Modules;
    private OffensiveModule[] offensiveModules; // 공격 모듈
    private ModuleBase[] defensiveModules;      // 방어 모듈
    private ModuleBase[] utilityModules;        // 지원 모듈

    private Vector2 imagePos;                   // 백그라운드 스크롤링에 사용되는 벡터 포지션
    private bool isShipAbleToMove;              // 움직일 수 있는지 여부

    [NonSerialized]
    public bool isTargetInDist;                 // 타겟이 유효거리 내에 있는지 
    [NonSerialized]
    public Vector3 joystickDirection;           // 조이스틱에서 받아온 방향
    private Vector3 currentDirection;           // 현재 방향
    #endregion
    public event EventHandler<ShipInteraction<CharShipStateMng>> ShipInteraction;
    public event EventHandler<ShipWarpEvent> ShipWarp;

    public Transform ProjectileContainer { get { return projectileContainer; } }
    public float InteractionDistance { get { return interactionDistance; } }

    public override CharShipStateMng Instance { get { return this; } }
    public override ShipType GetShipType { get { return ShipType.character; } }

    [SerializeField]
    private WeaponType weaponType;
    public WeaponType GetWeaponType { get { return weaponType; } }

    public bool IsShipEngaging { get; private set; }

    [NonSerialized]
    public ShipBase targetedShip;
    [NonSerialized]
    public float speedRevision;

    #region Evasion
    private readonly float activationLength = 0.15f;
    private Vector3 prevPos = Vector3.zero;
    private Vector3 currPos = Vector3.zero;
    public bool isEvasionActivated;

    public float EvasionDistance
    {
        get
        {
            // (공식) => { (스피드 * 가속도) * Sqrt((기동성 * 0.5f) * (질량 * 부피)) * 0.025f }
            return Mathf.Clamp((GetShipStat.GetMaxSpeed * GetShipStat.GetSpeedAcceleration) * 
                Mathf.Sqrt((GetShipStat.GetMobility * 0.5f) * (GetShipStat.GetWeight * GetShipStat.GetVolume)) * 0.025f, 30f, 220f);
        }
    }
    #endregion

    public IEnumerable<IOffensiveModule> OffensiveModules { get { return offensiveModules; } }
    public IEnumerable<IModule> DefensiveModules { get { return defensiveModules; } }
    public IEnumerable<IModule> UtilityModules { get { return utilityModules; } }

    void Awake()
    {
        IsReadyToWarp = true;
        InitializeState();
        initInteraction();
    }

    public void ShipEngagingToggle()
    {
        IsShipEngaging = !IsShipEngaging;
    }

    // Use this for initialization
    void Start()
    {
        isAbleToRecoverPower = true;
        InitShip();
        CharShipEffect.ThrusterLens.brightness = 0f;
        charInteractionCollider.radius = interactionDistance;
        StateEnter(CharShip_State.enter);
        CharShipEffect.currEngineBreakSoundTime = CharShipEffect.EngineBreakSound.clip.length;
        CharShipEffect.currEngineStartSoundTime = 0f;

        switch (weaponType)
        {
            case WeaponType.projectile:
                offensiveModules = Modules.GetComponentsInChildren<ProjectileModule>();
                break;
            case WeaponType.plasma:
                offensiveModules = Modules.GetComponentsInChildren<PlasmaModule>();
                break;
            case WeaponType.hyprid:
                offensiveModules = Modules.GetComponentsInChildren<HybridModule>();
                break;
        }
        //TEST
        foreach (ModuleBase module in offensiveModules)
            if(module.GetModuleImage)
                module.GetModuleImage.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        Execute();
        backgroundRect();
        checkEvasion();
        RecoverPower();
        //TODO: 나중에 코드 고칠것. 오버헤드 발생 가능성
        CharShipEffect.VelocityText.text = IsShipWarping ? "Warping" : Convert.ToString(Mathf.Round(currentShipSpeed)) + "m/s";
    }

    private void initInteraction()
    {
        interactionDistance = 150f;
    }

    public IEnumerator ActivePower(bool compulsory)
    {
        float t = 0f;
        while (t <= 0.75f)
        {
            if(!compulsory && (IsShipEngaging || isEvasionActivated))
                yield break;

            t += Time.deltaTime;
            yield return null;
        }
        isAbleToRecoverPower = true;
    }

    public void ActivateShipWarp()
    {
        RemoveAllState();
        IsShipEngaging = false;
        if (ShipWarp != null)
            ShipWarp(this, new ShipWarpEvent());
    }

    private void backgroundRect()
    {
        float revValue = 0.000001f;
        imagePos += (Vector2)(ShipInfo.ShipTransform.up * currentShipSpeed * revValue * Time.deltaTime);
        Rect bgRect = new Rect(imagePos, Vector2.one);
        GetGameStateMng.Background.uvRect = bgRect;

        var force = CharShipEffect.BackgroundParticle.forceOverLifetime;
        revValue = IsShipWarping ? 10f : 4f;
        Vector3 direction = -ShipInfo.ShipTransform.up * currentShipSpeed * revValue * Time.deltaTime * 10f;
        force.x = direction.x;
        force.y = direction.y;
    }

    private void RecoverPower()
    {
        if (isAbleToRecoverPower)
        {
            currentPower += GetShipStat.GetPowerRecoverFigure * Time.deltaTime;
            currentPower = Mathf.Clamp(currentPower, 0f, GetShipStat.GetPower);
        }
    }

    private void checkEvasion()
    {
        if (Input.touchCount > 1 && IsShipMoving)
        {
            Touch touch = Input.GetTouch(Input.touchCount - 1);
            if (touch.phase == TouchPhase.Began)
            {
                prevPos = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                currPos = touch.position;
                Vector3 direction = currPos - prevPos;
                float length = direction.magnitude * Time.deltaTime;
                float actvation = activationLength;
                if (length >= actvation && !isEvasionActivated)
                    // 현재 캐릭터가 바라보고 있는 방향을 기준으로 회피기동을 하려 했으나 그렇게 하면 너무헷갈림.
                    //Vector3 worldDirection = ShipInfo.ShipTransform.localToWorldMatrix.MultiplyVector(direction).normalized;
                    SubState(CharShip_State.evade, false, direction.normalized * EvasionDistance);

                prevPos = currPos;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                prevPos = Vector3.zero;
                currPos = Vector3.zero;
                isEvasionActivated = false;
            }
        }
    }

    public void InteractWithOther(IShip other)
    {

    }

    public void SailShip()
    {
        IsShipMoving = true;
        IsReadyToWarp = false;
        currentShipSpeed = Mathf.Lerp(currentShipSpeed, GetShipStat.GetMaxSpeed * speedRevision, GetShipStat.GetSpeedAcceleration * Time.deltaTime);
        currentDirection = ShipInfo.ShipTransform.up;
        ShipInfo.ShipTransform.localPosition += ShipInfo.ShipTransform.up * currentShipSpeed * GetShipStat.GetMobility * Time.deltaTime;
    }

    public override IEnumerator CancelShipSailing()
    {
        IsShipMoving = false;
        float trailStart = CharShipEffect.ThrusterTrail.time;
        float startSpeed = currentShipSpeed;
        float brightness = CharShipEffect.ThrusterLens.brightness;
        float t = 0f;

        while (t <= 0.99f)
        {
            t += GetShipStat.GetSpeedAcceleration * Time.deltaTime;
            CharShipEffect.ThrusterTrail.time = Mathf.Lerp(trailStart, 0f, t);
            currentShipSpeed = Mathf.Lerp(startSpeed, 0f, t);
            CharShipEffect.ThrusterLens.brightness = Mathf.Lerp(brightness, 0f, t);
            ShipInfo.ShipTransform.localPosition += currentDirection * currentShipSpeed * GetShipStat.GetMobility * Time.deltaTime;
            yield return null;
            if (IsShipMoving)
                yield break;
        }
        IsReadyToWarp = true;
    }

    public IEnumerator ControlBreakEngineSound()
    {
        float t = 0f;
        float engineStart = CharShipEffect.currEngineStartSoundTime;
        float engineBreak = CharShipEffect.currEngineBreakSoundTime;
        float engineVolume = CharShipEffect.EngineSound.volume;
        CharShipEffect.EngineSound.Play();

        while (t <= 1f)
        {
            t += GetShipStat.GetSpeedAcceleration * Time.deltaTime;
            CharShipEffect.currEngineStartSoundTime = Mathf.Lerp(engineStart, CharShipEffect.EngineStartSound.clip.length, t);
            CharShipEffect.currEngineBreakSoundTime = Mathf.Lerp(engineBreak, 0f, t);
            CharShipEffect.EngineSound.volume = Mathf.Lerp(engineVolume, 1f, t);
            yield return null;
            if (!IsShipMoving)
                yield break;
        }
    }

    public IEnumerator ControlStartEngineSound()
    {
        float t = 0f;
        float engineStart = CharShipEffect.currEngineStartSoundTime;
        float engineBreak = CharShipEffect.currEngineBreakSoundTime;
        float engineVolume = CharShipEffect.EngineSound.volume;

        while (t <= 1f)
        {
            t += GetShipStat.GetSpeedAcceleration * Time.deltaTime;
            CharShipEffect.currEngineStartSoundTime = Mathf.Lerp(engineStart, 0f, t);
            CharShipEffect.currEngineBreakSoundTime = Mathf.Lerp(engineBreak, CharShipEffect.EngineBreakSound.clip.length, t);
            CharShipEffect.EngineSound.volume = Mathf.Lerp(engineVolume, 0f, t);
            yield return null;
            if (IsShipMoving)
                yield break;
        }
        CharShipEffect.EngineSound.Stop();
    }

    void OnDestroy()
    {
        m_instance = null;
    }
}
