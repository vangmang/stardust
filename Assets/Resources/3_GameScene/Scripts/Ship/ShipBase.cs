﻿using GameStateLibrary.StateMng;
using UnityEngine.UI;
using UnityEngine;
using System.Collections;
using System;

public abstract class ShipBase : StateMng, IShip
{
    public static readonly float DownEffectSpeed = 1f;

    [Serializable]
    public struct _ShipInfo_
    {
        public Transform ShipTransform;
        public Image ShipImage;
    }
    public _ShipInfo_ ShipInfo;

    public event EventHandler<ShipDamagedEvent> DamagedEvent;
    public event EventHandler<ShipDownEvent> DownEvent;

    public delegate void DamagedAction();
    public DamagedAction damagedAction;

    [SerializeField]
    private ShipStatMng ShipStat;
    [SerializeField]
    private GameStateMng gameStateMng;
    [NonSerialized]
    public float currentShipSpeed;
    [NonSerialized]
    public float currentShipRot;
    [NonSerialized]
    public float currentPower;
    [NonSerialized]
    public bool isAbleToRecoverPower;
    public float currentHitPoints;


    public CircleCollider2D ShipVolumeCollider;
    public GameStateMng GetGameStateMng { get { return gameStateMng ?? GameStateMng.Instance; } }
    //public ShipBase Instance { get { return this; } }
    public IShipStat GetShipStat { get { return ShipStat; } }
    public bool IsShipMoving { get; protected set; }
    public bool IsShipWarping;
    public bool IsShipDown { get; protected set; }
    public bool IsReadyToWarp { get; protected set; }

    public abstract ShipType GetShipType { get; }

    public virtual void ShipBonus()
    {

    }

    public virtual void ShipPenalty()
    {

    }

    public virtual void InitShip()
    {
        StartCoroutine(InvokeInitShip());
    }

    private IEnumerator InvokeInitShip()
    {
        yield return new WaitForFixedUpdate();
        gameStateMng = GameStateMng.Instance;
        currentPower = ShipStat.GetPower;
        currentHitPoints = ShipStat.GetHitpoints;
        ShipVolumeCollider.radius = GetShipStat.GetVolume;
    }

    public virtual void TakeDamage(ICharges causer)
    {
        InvokeDamaged(new ShipDamagedEvent(causer, ref currentHitPoints, this));
    }

    public virtual void TakeDamage(DroneMissileStateMng droneMissile)
    {
        InvokeDamaged(new ShipDamagedEvent(droneMissile.GetDamage, ref currentHitPoints));
    }

    protected void InvokeDamaged(ShipDamagedEvent args)
    {
        if (DamagedEvent != null)
            DamagedEvent(this, args);
        if (currentHitPoints <= 0f)
            Down(args.Causer);
    }

    public virtual void Down(ICharges causer)
    {
        IsShipDown = true;
        InvokeDown(new ShipDownEvent(causer));
    }

    protected void InvokeDown(ShipDownEvent args)
    {
        if (DownEvent != null)
            DownEvent(this, args);
    }

    // 부드럽게 멈춘다
    public abstract IEnumerator CancelShipSailing();

    // 회전을 부드럽게 멈춘다.
    public IEnumerator CancelShipRotation()
    {
        Quaternion startRot = ShipInfo.ShipTransform.localRotation;
        float t = 0f;
        float revValue = 1f;
        while (t <= 1f)
        {
            revValue = Mathf.Lerp(1f, 0f, t);
            t += GetShipStat.GetRotAcceleration * revValue * Time.deltaTime;
            ShipInfo.ShipTransform.localRotation = Quaternion.Lerp(startRot, Quaternion.Euler(0f, 0f, currentShipRot), t);
            yield return null;
            if (IsShipMoving || IsShipWarping) // 함선이 움직이거나 함선이 워프상태일 때는 코루틴을 중단한다.
                yield break;
        }
    }
}

public abstract class ShipBase<T> : ShipBase, IShip<T>
    where T : class, IShip
{
    public abstract T Instance { get; }
}



public abstract class EnemyShipBase<T> : ShipBase<T>, IEnmeyInteraction<EnemyShipBase<T>>
    where T : class, IShip
{
    public event EventHandler<ShipInteraction<EnemyShipBase<T>>> ShipInteraction;
    [SerializeField]
    protected CircleCollider2D interactionCollider;
    protected bool isDetected;      // 주인공 함선이 적들의 사정거리에 발견되었는지 여부
    protected bool engageEnbale;    // 교전이 시작되었는지 여부

    protected float engageDistance; // 교전 가능 거리
    protected float engageEnableDegree; // 교전 가능 각도

    protected float interactionDistance;
    protected float interactionEnableDegree;
    protected float interactionDisableDistance;

    private bool isDownAlreadyInvoked;
    private GameObject indicatorObj;

    public new bool IsShipMoving { get; set; } 
    public float EngageDistance { get { return engageDistance; } }
    public float EngageEnableDegree { get { return engageEnableDegree; } }


    public float InteractionDistance { get { return interactionDistance; } }
    public float InteractionEnableDegree { get { return interactionEnableDegree; } }
    public float InteractionDisableDistance { get { return interactionDisableDistance; } }

    public abstract void Init();

    public virtual void InitInteraction()
    {
        interactionCollider.radius = interactionDistance;
    }

    // 미니맵에 오브젝트 생성
    public void CreateObjOnMinimap(string prefabName)
    {
        CoordinateMng coord = // TODO: CreatePrefab 메소드들 살펴볼것
            Mng.Instance.CreatePrefab("MiniMapCanvas/Map/Objs", prefabName, E_RESOURCES.E_3_GAMESCENE, Vector3.zero, Quaternion.identity).GetComponent<CoordinateMng>();
        coord.Origin = ShipInfo.ShipTransform; 
    }

    public void CreateIndicator()
    {
        StartCoroutine(WaitForIndicatorInit());
    }

    private IEnumerator WaitForIndicatorInit()
    {
        yield return new WaitForFixedUpdate();
        string s_Parent = "MainCanvas/World/Indicators";
        Indicator indicator =
            Mng.Instance.CreatePrefab(s_Parent, "Map/EnemyIndicator", E_RESOURCES.E_3_GAMESCENE, Vector3.zero, Quaternion.identity, "EnemyIndicator[" + ShipInfo.ShipTransform.name + "]").GetComponentInChildren<Indicator>();
        indicatorObj = indicator.gameObject;
        indicator.gameStateMng = GetGameStateMng;
        indicator.MainCamera = GetGameStateMng.MainCamera;
        indicator.UICamera = GetGameStateMng.UICamera;
        indicator.BaseTransform = ShipInfo.ShipTransform;
    }

    public override void Down(ICharges causer)
    {
        base.Down(causer);
        StartCoroutine(checkIsAlreadyDown());
    }

    // 이미 죽었는지 여부
    private IEnumerator checkIsAlreadyDown()
    {
        yield return new WaitForFixedUpdate();
        if (isDownAlreadyInvoked)
            yield break;
        GameStateMng.Instance.ReduceStageEnemyCount();
        indicatorObj.SetActive(false);
        isDownAlreadyInvoked = true;
    }

    // 현재 거리, 각도를 통해서 주인공을 공격할 수 있는지 여부를 판단하는 메소드
    public bool CheckDetection(out float curDist, out float deg)
    {
        curDist = Vector3.Distance(GetGameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.localPosition, ShipInfo.ShipTransform.localPosition);
        Vector3 point = ShipInfo.ShipTransform.InverseTransformPoint(GetGameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.localPosition);
        deg = Mathf.Atan2(point.x, point.y) * Mathf.Rad2Deg;
        return curDist <= engageDistance && Mathf.Abs(deg) <= engageEnableDegree || curDist <= engageDistance * 0.5f;
    }

    public void InteractWithOther(IShip other)
    {
        InvokeShipInteraction(new ShipInteraction<EnemyShipBase<T>>(other));
    }
    private void InvokeShipInteraction(ShipInteraction<EnemyShipBase<T>> args)
    {
        if (ShipInteraction != null)
            ShipInteraction(this, args);
    }
}
