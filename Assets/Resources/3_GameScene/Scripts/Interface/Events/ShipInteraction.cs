﻿using System;
public class ShipInteraction<T> : EventArgs {
    public IShip OtherShip { get; private set; } 

    public ShipInteraction(IShip other)
    {
        OtherShip = other;
    }
}
