﻿using System;
using UnityEngine;

public class ShipDamagedEvent : EventArgs
{

    public ICharges Causer { get; private set; }

    public ShipDamagedEvent(ICharges causer, ref float hitPoints, ShipBase shipBase)
    {
        Causer = causer;
        // TODO: 데미지 받는 공식 만들 것
        hitPoints -= causer.GetDamage;
        Vector3 force = causer.GetChargeTransform.up;
        CharShipStateMng charShip = shipBase as CharShipStateMng;
        if (charShip)
            if (causer.GetChargeType == Charges.hybrid ||
                causer.GetChargeType == Charges.projectile)
                CameraEffect.DirectCameraAsForce(force);
            else
                CameraEffect.DirectCameraAsExplosion(0.5f);
    }

    public ShipDamagedEvent(float damage, ref float hitPoints)
    {
        hitPoints -= damage;
    }
}
