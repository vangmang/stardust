﻿using System;
using UnityEngine;

public class ShipDownEvent : EventArgs {

    public ICharges Causer { get; private set; }

    public ShipDownEvent(ICharges causer)
    {
        Causer = causer;
    }
}
