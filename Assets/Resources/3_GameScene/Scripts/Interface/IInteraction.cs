﻿using UnityEngine;
using System;
// 아이템이나 적들과 캐릭터의 상호작용

public interface IInteraction<T>
{    
    float InteractionDistance { get; }
    event EventHandler<ShipInteraction<T>> ShipInteraction;
    void InteractWithOther(IShip other);
}

public interface IEnmeyInteraction<T> : IInteraction<T>
{
    float InteractionEnableDegree { get; }
    float InteractionDisableDistance { get; }
}