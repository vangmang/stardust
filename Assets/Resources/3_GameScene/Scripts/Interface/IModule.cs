﻿using System.Collections.Generic;

// TODO: 그냥 볼 것

public enum ModuleType
{
    Offense,
    Defense,
    Utility,
}

public enum WeaponType
{                                   // 공격 / 속도 / 탄창 / 각도 / 거리 / 타겟팅 // 총 32점 // 기준 5점
    plasma,         // 플라즈마          10     3     4     5      3       7
    hyprid,         // 하이브리드         8     5     5     2      10      2
    projectile,     // 발사체            3      7     7     5      5       5
}

public enum DefenseType
{                                   // 방어력 / 체력 / 속도 / 무게 / 부피 // 총 68점 // 기준 12점
    armorPlate,     // 갑판              20      18     8     11     11
    hardner,        // 경화제            30       1     11    14     12
    layerPlate,     // 티타늄 레이어      8       22     12    12     14
}

public enum UtilityType
{
    sensorUpgrade,  // 타겟팅 속도 
    degreeUpgrade,  // 좌우각 업그레이드 
    cargoExtension, // 화물운반 사이즈 증가
    CPU_Upgrade,    // CPU 증가
    GridUpgrade,    // Power 증가
}

public interface IModule : IInventorialbe<IModule>
{
    ModuleType GetModuleType { get; }         // 모듈 타입
    string GetModuleName { get; }             // 모듈 이름
    string GetModuleDescription { get; }      // 모듈 묘사
    float GetCPU_Usage { get; }               // 모듈 CPU 사용량
    float GetPower_Usage { get; }             // 모듈 동력 사용량
    float GetVolume { get; }                // 모듈 부피
    float GetWeight { get; }                // 모듈 무게
    void Bonus();                               // 모듈 보너스
    void Penalty();                             // 모듈 패널티
}

public interface IModule<T> : IModule
    where T : class
{
    T ModuleInstance { get; }                      // 모듈 인스턴스
}

public interface IModule<T, U> : IModule<T>
    where T : class
    where U : class, IShip
{
    U ShipInstance { get; }                  // 함선 인스턴스
}

public interface IOffensiveModule : IModule
{
    WeaponType GetWeaponType { get; }       // 무기 타입
    int GetMaxCharges { get; }              // 무기 최대 탄약
    int GetCurrentChargeCount { get; }      // 현재 장탄수
    float GetPowerConsumption { get; }      // 파워 소비량
}

public interface IOffensiveModule<T, U> : IOffensiveModule, IModule<T, U>
    where T : class
    where U : class, IShip
{
    float GetEffectiveRange { get; }       // 무기 최대사거리
    float GetDamage { get; }                  // 무기 데미지
    float GetDPS { get; }                     // 무기 초당 데미지
    float GetCycleSpeed { get; }              // 무기 공격속도
    float GetReloadingSpeed { get; }            // 무기 재장전 속도
    float GetAttackableDeg { get; }           // 무기 공격 가능 각도 
    float GetObjectVelocity { get; }        // 무기 발사체 공격 속도
}

public interface IDefensiveModule : IModule
{
    DefenseType GetDefenseType { get; }     // 방어 타입
    float GetArmor { get; }                 // 보너스 방어력
    float GetHitpoints { get; }             // 보너스 내구력
}

public interface IDefensiveModule<T, U> : IDefensiveModule, IModule<T, U>
    where T : class
    where U : class, IShip
{
}

public interface IUtilityModule : IModule
{
    UtilityType GetUtilityType { get; }     // 유틸 타입

}

public interface IUtilityModule<T, U> : IUtilityModule, IModule<T, U>
    where T : class
    where U : class, IShip
{
}