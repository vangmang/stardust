﻿using UnityEngine;

public enum Charges
{
    plasma,
    hybrid,
    projectile,
}

public interface IItem : IInventorialbe<IItem>
{
    float GetVolume { get; }
    float GetWeight { get; }
}

public interface IItem<T> : IItem
    where T : class
{
    T Instance { get; }
}

public interface ICharges : IItem
{
    Transform GetChargeTransform { get; }
    Charges GetChargeType { get; }
    float GetDamage { get; }                // 데미지
    float GetVelocity { get; }           // 탄속
    float GetEffectiveRange { get; }     // 유효 거리
}

public interface ICharges<T, U> : ICharges, IItem<T>
    where T : class
    where U : IModule
{
    U ModuleInstance { get; }
}