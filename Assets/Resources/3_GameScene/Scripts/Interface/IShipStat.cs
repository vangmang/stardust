﻿
public interface IShipStatSetter
{
    float SetMaxSpeed { set; }
    float SetSpeedAcceleration { set; }
    float SetRotAcceleration { set; }
    float SetTargetingSpeed { set; }
    float SetMobility { set; }
    float SetHitpoints { set; }
    float SetPower { set; }
    float SetPowerRecover { set; }
    float SetArmor { set; }
}

public interface IShipStat : IShipStatSetter
{
    #region Level
    string GetShipName { get; }             // 함선 이름
    int GetShipLevel { get; }               // 함선 레벨
    int GetValueOfCharges_perUp { get; }    // 레벨업당 올라가는 탄약 수치
    float GetValueOfHP_perUp { get; }         // 레벨업당 올라가는 HP 수치
    float GetValueOfCPU_perUp { get; }        // 레벨업당 올라가는 CPU 수치
    float GetValueOfPower_perUp { get; }      // 레벨업당 올라가는 동력 수치
    float GetValueOfDmg_perUp { get; }      // 레벨업당 올라가는 데미지 수치
    float GetValueOfArmor_perUp { get; }    // 레벨업당 올라가는 방어력 수치
    float GetValueOfSensor_perUp { get; }   // 레벨업당 올라가는 센서 수치
    #endregion

    #region Modules
    int GetOffensiveModuleCount { get; }    // 공격 모듈 개수 
    int GetDefensiveModuleCount { get; }    // 방어 모듈 개수
    int GetUtilityModuleCount { get; }      // 유틸 모듈 개수
    #endregion

    #region ShipStat
    int GetCharges { get; }             // 함선 기본 최대 탄약
    float GetMaxSpeed { get; }              // 함선 최고 속력
    float GetSpeedAcceleration { get; }     // 함선 속력 가속도
    float GetRotAcceleration { get; }       // 함선 회전 가속도
    float GetCargoVolume { get; }             // 함선 화물운반 부피
    float GetCargoWeight { get; }           // 함선 화물운반 무게
    float GetHitpoints { get; }                // 함선 기본 내구력
    float GetCPU { get; }                 // 함선 기본 CPU
    float GetPower { get; }               // 함선 기본 동력
    float GetPowerRecoverFigure { get; }  // 함선 동력 복구량
    float GetDamage { get; }            // 함선 기본 데미지
    float GetArmor { get; }             // 함선 기본 방어력
    float GetVolume { get; }            // 함선 기본 부피
    float GetWeight { get; }            // 함선 기본 무게
    float GetMobility { get; }          // 함선 기본 기동성
    float GetTargetingSpeed { get; }        // 함선 기본 타겟팅 스피드
    #endregion
}
