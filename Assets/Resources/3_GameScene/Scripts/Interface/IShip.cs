﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System;

public enum ShipType
{
    explorer,       // 탐사선
    assault,        // 공격 함선
    tank,           // 탱킹 함선
    carrier,        // 드론 함선
    drone,           // 드론
    character
}

public interface IShip
{
    event EventHandler<ShipDamagedEvent> DamagedEvent;
    event EventHandler<ShipDownEvent> DownEvent;

    ShipType GetShipType { get; }   // 함선 종류
    IShipStat GetShipStat { get; }
    bool IsShipMoving { get; }

    void ShipBonus();
    void ShipPenalty();
    void TakeDamage(ICharges causer);
}

public interface IShip<T> : IShip
    where T : class, IShip
{
    T Instance { get; }
}


public interface IOffenseShip : IShip
{
    WeaponType GetWeaponType { get; }
    bool IsShipEngaging { get; }
    Transform ProjectileContainer { get; }
    IEnumerator ActivePower(bool compulsory);
}

public interface IOffenseShip<T> : IOffenseShip, IShip<T>
    where T : class, IShip
{
    IEnumerable<IOffensiveModule> OffensiveModules { get; }
}

public interface IDefenseShip : IShip
{

}

public interface IDefenseShip<T> : IDefenseShip, IShip<T>
    where T : class, IShip
{
    IEnumerable<IModule> DefensiveModules { get; }
}

public interface IUtilityShip : IShip
{

}

public interface IUtilityShip<T> : IUtilityShip, IShip<T>
     where T : class, IShip
{
    IEnumerable<IModule> UtilityModules { get; }
}

