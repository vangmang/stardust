﻿
public interface IWorld {

    int WorldLevel { get; }
    int EnemyCount { get; }

}
