﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// 화면상에 표시되지 않는 물체를 표시해주는 표시기
public sealed class Indicator : MonoBehaviour
{
    private readonly Vector2 ScreenSize = new Vector2(700f, 384f);

    public GameStateMng gameStateMng;
    public Transform DirectionTransform;    // 표시기 트랜스폼
    public Transform BaseTransform;         // 표시기에 표시되는 함선의 트랜스폼
    public Image Direction;                 // 표시기 이미지

    public Camera MainCamera;           
    public Camera UICamera;
    public Text DistanceIndicator;          // 표시기에 표시되는 거리
    private int distance;
    private Vector2 ScreenPos;
    private float originXpos;

    void Awake()
    {
        DirectionTransform.gameObject.SetActive(false);
    }

    IEnumerator Start()
    {
        yield return new WaitUntil(() => !CharShipStateMng.GetInstance.IsShipWarping);
        DirectionTransform.gameObject.SetActive(true);
        yield return new WaitForFixedUpdate();
        originXpos = 63.2f;
    }

    // LateUpdate 구글링 해서 찾아볼 것
    void LateUpdate()
    {
        distance = (int)Vector3.Distance(BaseTransform.localPosition, gameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.localPosition);

        Vector3 direction = (BaseTransform.localPosition - gameStateMng.GetCharShipStateMng.ShipInfo.ShipTransform.localPosition).normalized;
        float rot = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg + 90f;//- 90f;
        var wantedRot = Quaternion.Euler(0f, 0f, rot);
        Direction.transform.localRotation = wantedRot;
        ScreenPos = BaseTransform.localPosition - MainCamera.transform.localPosition;
        Vector2 CompareSize = ScreenSize;

        // 화면상에 적들이 표시되면 인디케이터를 꺼준다. 알파값을 통해 표시
        if (Mathf.Abs(ScreenPos.x) >= ScreenSize.x || Mathf.Abs(ScreenPos.y) >= ScreenSize.y)
        {
            var color = Direction.color;
            color.a = 1f;
            Direction.color = color;

            color = DistanceIndicator.color;
            color.a = 1f;
            DistanceIndicator.color = color;
        }
        else
        {
            var color = Direction.color;
            color.a = 0f;
            Direction.color = color;

            color = DistanceIndicator.color;
            color.a = 0f;
            DistanceIndicator.color = color;
        }
        // 표시기의 미터기가 일정포시젼을 넘어가게 되면 숫자를 왼쪽에 표시해준다.
        bool isRight = ScreenPos.x >= 620f;
        DistanceIndicator.alignment = isRight ? TextAnchor.MiddleRight : TextAnchor.MiddleLeft;
        Vector3 textPos = DistanceIndicator.transform.localPosition;
        textPos.x = isRight ? -originXpos : originXpos;
        DistanceIndicator.transform.localPosition = textPos;

        DistanceIndicator.text = System.Convert.ToString(distance) + "m"; // 만약 이 코드에서 오버헤드가 발생하면 수정할 것
        // 화면 내에서 표시를 해준다. 표시기는 화면을 벗어날 수 없다.
        DirectionTransform.localPosition = 
            new Vector3(Mathf.Clamp(ScreenPos.x, -CompareSize.x, CompareSize.x), Mathf.Clamp(ScreenPos.y, -CompareSize.y, CompareSize.y)) + MainCamera.transform.localPosition;
    }
}
