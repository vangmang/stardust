﻿using UnityEngine;

public sealed class CoordinateMng : MonoBehaviour {

    public Transform Origin;
    private Vector3 WorldMap;
    private Vector3 MiniMap;

	// Use this for initialization
	void Start () {
        WorldMap = WorldMng.Instance.WorldMap;
        MiniMap = WorldMng.Instance.MiniMap;
        transform.localScale = Vector3.one;
    }

    // Update is called once per frame
    void LateUpdate () {
        // 미니맵 공식 => 원본 포지션 / 월드맵 포지션 * 미니맵 포지션
        float x = Origin.localPosition.x / WorldMap.x * MiniMap.x;
        float y = Origin.localPosition.y / WorldMap.y * MiniMap.y;

        Vector3 coordinate = new Vector3(x + 50f, y);
        transform.localPosition = coordinate;
        transform.localRotation = Origin.localRotation;
    }
    
    void OnDestory()
    {

    }
}
