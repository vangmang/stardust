﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;
using System.Linq;

public sealed class CurrentLeftChargeBar : MonoBehaviour
{

    public CharShipStateMng charShipStateMng;
    public Image LeftChargeBar;
    public bool isLeftModule;

    private OffensiveModule module;

    float Origin_r;
    float Origin_g;
    float Origin_b;

    float RowHP_r;
    float RowHP_g;
    float RowHP_b;

    // Use this for initialization
    IEnumerator Start()
    {
        Origin_r = 57f / 255f;
        Origin_g = 152f / 255f;
        Origin_b = 124f / 255f;
        RowHP_r = 206f / 255f;
        RowHP_g = 15f / 255f;
        RowHP_b = 15f / 255f;
        LeftChargeBar.color = new Color(Origin_r, Origin_g, Origin_b);
        yield return new WaitForFixedUpdate();
        LeftChargeBar.fillAmount = 1f;
        module = (OffensiveModule)(isLeftModule ? charShipStateMng.OffensiveModules.ElementAtOrDefault(0) : charShipStateMng.OffensiveModules.ElementAtOrDefault(1));
        module.ReloadCompleteEvent += (sender, e) =>
        {
            LeftChargeBar.fillAmount = 1f;
        };
        yield return new WaitUntil(() => module != null);
        StartCoroutine(UpdateBar());
    }

    IEnumerator UpdateBar()
    {
        yield return new WaitForFixedUpdate();
        float currentBar = (float)module.GetCurrentChargeCount / module.GetMaxCharges;
        LeftChargeBar.fillAmount = currentBar;
        LeftChargeBar.color =
            new Color(Mathf.Lerp(LeftChargeBar.color.r, Mathf.Clamp(currentBar, RowHP_r, Origin_r), 6f * Time.deltaTime),
            Mathf.Lerp(LeftChargeBar.color.g, Mathf.Clamp(currentBar, RowHP_g, Origin_g), 6f * Time.deltaTime),
            Mathf.Lerp(LeftChargeBar.color.b, Mathf.Clamp(currentBar, RowHP_b, Origin_b), 6f * Time.deltaTime));
        yield return null;
        StartCoroutine(UpdateBar());
    }
}
