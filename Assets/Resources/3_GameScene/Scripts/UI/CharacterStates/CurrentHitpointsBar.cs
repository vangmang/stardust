﻿using UnityEngine.UI;
using UnityEngine;

public sealed class CurrentHitpointsBar : MonoBehaviour
{

    public CharShipStateMng charShipStateMng;
    public Image HitpointsBar;

    float Origin_r;
    float Origin_g;
    float Origin_b;

    float RowHP_r;
    float RowHP_g;
    float RowHP_b;

    void Start()
    {
        Origin_r = 0f;
        Origin_g = 235f / 255f;
        Origin_b = 246f / 255f;
        RowHP_r = 216f / 255f;
        RowHP_g = 238 / 255f;
        RowHP_b = 9f / 255f;
        HitpointsBar.color = new Color(Origin_r, Origin_g, Origin_b);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float currentBar = charShipStateMng.currentHitPoints / charShipStateMng.GetShipStat.GetHitpoints;
        HitpointsBar.fillAmount = Mathf.Lerp(HitpointsBar.fillAmount, currentBar, 3f * Time.deltaTime);
        HitpointsBar.color =
            new Color(Mathf.Lerp(HitpointsBar.color.r, Mathf.Clamp(currentBar, RowHP_r, Origin_r), 3f * Time.deltaTime),
            Mathf.Lerp(HitpointsBar.color.g, Mathf.Clamp(currentBar, RowHP_g, Origin_g), 3f * Time.deltaTime),
            Mathf.Lerp(HitpointsBar.color.b, Mathf.Clamp(currentBar, RowHP_b, Origin_b), 3f * Time.deltaTime));
    }
}
