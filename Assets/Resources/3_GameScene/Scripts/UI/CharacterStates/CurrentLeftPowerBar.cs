﻿using UnityEngine.UI;
using UnityEngine;

public class CurrentLeftPowerBar : MonoBehaviour {

    public CharShipStateMng charShipStateMng;
    public Image PowerBar;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        PowerBar.fillAmount = //charShipStateMng.currentPower / charShipStateMng.GetShipStat.GetPower;
            //Debug.Log(charShipStateMng.currentPower / charShipStateMng.GetShipStat.GetPower);
            Mathf.Lerp(PowerBar.fillAmount, charShipStateMng.currentPower / charShipStateMng.GetShipStat.GetPower, 7f * Time.deltaTime);
	}
}
