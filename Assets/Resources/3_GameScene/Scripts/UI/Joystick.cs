﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

// TODO: 살펴 볼 것
public sealed class Joystick : MonoBehaviour, IPointerDownHandler, IDragHandler, IEndDragHandler, IPointerUpHandler
{
    public Camera mainCamera;
    public CharShipStateMng charShipStateMng;

    public Image Stick;
    public Image Origin;

    private RectTransform stickTransform;
    private RectTransform originTransform;

    private float radian;


    void Start()
    {
        // 이벤트 
        charShipStateMng.ShipWarp +=
            (sender, e) =>
            {
                Stick.raycastTarget = false;
                Origin.raycastTarget = false;
                StartCoroutine(BtnEnable());
            };

        stickTransform = Stick.rectTransform;
        originTransform = Origin.rectTransform;
        radian = originTransform.rect.width * 0.375f;
    }

    private IEnumerator BtnEnable()
    {
        yield return new WaitForSeconds(1f);
        yield return new WaitForFixedUpdate();
        yield return new WaitUntil(() => !charShipStateMng.IsShipWarping);
        stickTransform.localPosition = Vector3.zero;
        Stick.raycastTarget = true;
        Origin.raycastTarget = true;
    }

    private Vector3 Raycasting()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.GetTouch(0).position);
        return ray.origin;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (Input.touchCount >= 1)
        {
            stickTransform.position = Raycasting();
            charShipStateMng.ChangeState(CharShipStateMng.CharShip_State.move);
            OnDrag(eventData);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (Input.touchCount >= 1)
        {
            stickTransform.position = Raycasting();
            Vector3 point = (stickTransform.position - transform.localPosition);
            point.z = 0f;
            float dist = Mathf.Min(point.magnitude, radian);
            Vector3 direction = point.normalized;
            float deg = Mathf.Atan2(direction.y, direction.x);
            Vector3 pos = new Vector3(Mathf.Cos(deg) * dist, Mathf.Sin(deg) * dist);
            charShipStateMng.speedRevision = dist / radian;
            stickTransform.localPosition = pos;
            charShipStateMng.currentShipRot = deg * Mathf.Rad2Deg - 90f;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!charShipStateMng.IsShipWarping)
        {
            if (Input.touchCount >= 1)
            {
                stickTransform.localPosition = Vector3.zero;
                charShipStateMng.ChangeState(CharShipStateMng.CharShip_State.idle);
            }
        }
        else
            stickTransform.localPosition = Vector3.zero;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OnEndDrag(eventData);
    }
}
