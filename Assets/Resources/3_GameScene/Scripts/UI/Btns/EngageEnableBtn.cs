﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public sealed class EngageEnableBtn : IngameButtonBase, IPointerClickHandler
{

    public CharShipStateMng charShipStateMng;
    public Image ProjectileBtn;
    public Image PlasmaBtn;
    public Image HybridBtn;
    private Image Origin;

    void Start()
    {
        switch (charShipStateMng.GetWeaponType)
        {
            case WeaponType.projectile: Origin = ProjectileBtn; break;
            case WeaponType.plasma: Origin = PlasmaBtn; break;
            case WeaponType.hyprid: Origin = HybridBtn; break;
        }
        charShipStateMng.ShipWarp +=
            (sender, e) =>
            {
                Origin.raycastTarget = false;
                StartCoroutine(BtnEnableAfterWarp());
            };
        Origin.color = new Color(1f, 1f, 1f, 0.7f);
        Origin.gameObject.SetActive(true);
    }

    private IEnumerator BtnEnableAfterWarp()
    {
        yield return new WaitForSeconds(1f);
        yield return new WaitForFixedUpdate();
        yield return new WaitUntil(() => !charShipStateMng.IsShipWarping);
        Origin.raycastTarget = true;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        charShipStateMng.ShipEngagingToggle();
        if (charShipStateMng.IsShipEngaging)
        {
            Origin.color = new Color(1f, 1f, 1f, 1f);
            charShipStateMng.SubState(CharShipStateMng.CharShip_State.attack);
        }
        else
        {
            Origin.color = new Color(1f, 1f, 1f, 0.7f);
            charShipStateMng.RemoveState(CharShipStateMng.CharShip_State.attack);
        }
    }
}
