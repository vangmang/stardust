﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;
using System;

public abstract class IngameButtonBase : MonoBehaviour
{
    public static event EventHandler BtnClickEvent;
    public Image BtnImage;
    public Transform BtnTransform;

    protected void InvokeBtnClickEvent()
    {
        if (BtnClickEvent != null)
            BtnClickEvent(this, new EventArgs());
    }
}
