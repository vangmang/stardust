﻿using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;
using System.Collections;
using System;

public sealed class ReloadBtn : IngameButtonBase, IPointerClickHandler
{
    public CharShipStateMng charShipStateMng;

    IEnumerator Start()
    {
        yield return new WaitForFixedUpdate();
        if (charShipStateMng.GetWeaponType == WeaponType.projectile)
            BtnTransform.gameObject.SetActive(false);
    }

    // 프로젝타일은 재장전 버튼이 없다.
    // 탄환이 전부 떨어지면 자동으로 재장전이 되거나 
    // 공격이 중단된 상태이고 탄환이 어느정도 남은 상태라면 재장전을 할 수 있다.
    public void OnPointerClick(PointerEventData eventData)
    {
        if(!charShipStateMng.IsShipEngaging)
        {
            foreach(OffensiveModule module in charShipStateMng.OffensiveModules)
            {
                if (module.GetCurrentChargeCount == module.GetMaxCharges || module.isReloading)
                    return;
                
                Enum state = default(Enum);
                switch (charShipStateMng.GetWeaponType)
                {
                    case WeaponType.plasma: state = PlasmaModule.Plasma_State.reload; break;
                    case WeaponType.hyprid: state = HybridModule.Hybrid_State.reload; break;
                }
                module.ChangeState(state);
            }
            var color = BtnImage.color;
            color.a = 0.7f;
            BtnImage.color = color;
            BtnImage.raycastTarget = false;
            StartCoroutine(WaitForReloading());
        }
    }

    private IEnumerator WaitForReloading()
    {
        foreach (OffensiveModule module in charShipStateMng.OffensiveModules)
        {
            yield return new WaitUntil(() => !module.isReloading);
        }
        var color = BtnImage.color;
        color.a = 1f;
        BtnImage.raycastTarget = true;
        BtnImage.color = color;
    }
}
