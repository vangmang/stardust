﻿using UnityEngine.EventSystems;
using UnityEngine;

public sealed class ResumeBtn : IngameButtonBase, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {

    public GameStateMng gameStateMng;
    public Transform PausePopup;

    public void OnPointerClick(PointerEventData eventData)
    {
        gameStateMng.ChangeState(GameStateMng.GameState.idle);
        OnPointerExit(eventData);
        PausePopup.gameObject.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        var color = BtnImage.color;
        color = new Color(0.7f, 0.7f, 0.7f, 1f);
        BtnImage.color = color;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        var color = BtnImage.color;
        color = new Color(1f, 1f, 1f, 1f);
        BtnImage.color = color;
    }
}
