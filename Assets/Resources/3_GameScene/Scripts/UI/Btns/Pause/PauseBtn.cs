﻿using UnityEngine.EventSystems;
using UnityEngine;

public sealed class PauseBtn : IngameButtonBase, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public GameStateMng gameStateMng;
    public Transform PausePopup;

    public void OnPointerClick(PointerEventData eventData)
    {
        PausePopup.gameObject.SetActive(true);
        gameStateMng.ChangeState(GameStateMng.GameState.pause);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        var color = BtnImage.color;
        color = new Color(0.7f, 0.7f, 0.7f, 1f);
        BtnImage.color = color;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        var color = BtnImage.color;
        color = new Color(1f, 1f, 1f, 1f);
        BtnImage.color = color;
    }
}
