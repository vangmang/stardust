﻿using UnityEngine.EventSystems;
using UnityEngine;

public sealed class MenuBtn : IngameButtonBase, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public GameStateMng gameStateMng;

    public void OnPointerClick(PointerEventData eventData)
    {
        gameStateMng.ChangeState(GameStateMng.GameState.exit);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        var color = BtnImage.color;
        color = new Color(0.7f, 0.7f, 0.7f, 1f);
        BtnImage.color = color;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        var color = BtnImage.color;
        color = new Color(1f, 1f, 1f, 1f);
        BtnImage.color = color;
    }

    void OnDestroy()
    {

    }
}
