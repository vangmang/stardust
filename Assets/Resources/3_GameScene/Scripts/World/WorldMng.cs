﻿using UnityEngine;

public sealed class WorldMng : MonoBehaviour {

    private WorldMng() { }

    private static WorldMng instance = null;
    public static WorldMng Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(WorldMng)) as WorldMng;
            return instance;
        }
    }


    public Vector2 WorldMap;
    public Vector2 MiniMap;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnDestroy()
    {

    }
}
