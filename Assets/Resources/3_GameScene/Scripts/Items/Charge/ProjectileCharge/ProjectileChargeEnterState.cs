﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class ProjectileChargeEnterState : State<ProjectileCharge> {

    public override Enum GetState
    {
        get
        {
            return ProjectileCharge.ProjectileCharge_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.ChangeState(ProjectileCharge.ProjectileCharge_State.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
