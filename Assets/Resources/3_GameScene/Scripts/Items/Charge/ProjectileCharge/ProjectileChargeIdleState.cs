﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

public class ProjectileChargeIdleState : State<ProjectileCharge> {

    public override Enum GetState
    {
        get
        {
            return ProjectileCharge.ProjectileCharge_State.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        StartCoroutine(WaitParticle());
    }

    private IEnumerator WaitParticle()
    {
        yield return new WaitUntil(() => !instance.ChargeEffect.ImpactParticle.isPlaying);
        instance.projectileTransform.gameObject.SetActive(false);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
        
    }
}
