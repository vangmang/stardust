﻿using System.Collections;
using UnityEngine;

public class ProjectileCharge : ChargeBase<ProjectileCharge, ProjectileModule> {

    public enum ProjectileCharge_State
    {
        enter,
        idle,
        fire
    }

    public override ProjectileCharge Instance
    {
        get
        {
            return this;
        }
    }

    [SerializeField]
    private ProjectileModule projectileModule;

    public override Charges GetChargeType { get { return Charges.projectile; } }
    public override ProjectileModule ModuleInstance { get { return projectileModule; } }
    public ProjectileModule setModuleInstance { set { projectileModule = value; } }
    void Awake()
    {
        InitializeState();
    }

    // Use this for initialization
    void Start () {
        damage += projectileModule.GetDamage;
        velocity += projectileModule.GetObjectVelocity;        
        StateEnter(ProjectileCharge_State.enter);
	}

    public override IEnumerator DestroyProjectile()
    {
        yield return new WaitForSeconds(DestroyTime);
        ChangeState(ProjectileCharge_State.idle);
        projectileTransform.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
        Execute();
	}

    void OnDestroy()
    {

    }
}
