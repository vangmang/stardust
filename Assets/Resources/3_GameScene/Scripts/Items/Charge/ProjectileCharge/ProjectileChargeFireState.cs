﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class ProjectileChargeFireState : State<ProjectileCharge> {

    public override Enum GetState
    {
        get
        {
            return ProjectileCharge.ProjectileCharge_State.fire;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        Vector3 direction = instance.ModuleInstance.ModuleTransform.up;
        float deg = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        var wantedRot = Quaternion.Euler(0f, 0f, deg);
        instance.projectileTransform.localRotation = wantedRot;
        instance.projectileTransform.localPosition = instance.ModuleInstance.ModuleTransform.position;
        instance.projectileTransform.gameObject.SetActive(true);
        StartCoroutine(instance.DestroyProjectile());
    }

    public override void Execute()
    {
        instance.projectileTransform.localPosition += instance.projectileTransform.up * instance.GetVelocity * Time.deltaTime;
    }

    public override void Exit()
    {
    }
}
