﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneMissileImpact : MonoBehaviour {

    [SerializeField]
    private DroneMissileStateMng droneMissile;
    string Tag;

	// Use this for initialization
	void Start () {
        Tag = "Player";	
	}

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals(Tag))
        {
            // TODO: 리플렉션 구글링 해볼것
            other.gameObject.SendMessage("TakeDamage", droneMissile, SendMessageOptions.DontRequireReceiver);
            StartCoroutine(waitForFixedUpdate());
        }
    }

    private IEnumerator waitForFixedUpdate()
    {
        yield return new WaitForFixedUpdate();
        if (droneMissile.MissileEffect.ImpactParticle)
            droneMissile.MissileEffect.ImpactParticle.Play();        
        droneMissile.ChangeState(DroneMissileStateMng.DroneMissile_State.idle);
    }
}
