﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class PlasmaChargeEnterState : State<PlasmaCharge> {

    public override Enum GetState
    {
        get
        {
            return PlasmaCharge.PlasmaCharge_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.ChangeState(PlasmaCharge.PlasmaCharge_State.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
