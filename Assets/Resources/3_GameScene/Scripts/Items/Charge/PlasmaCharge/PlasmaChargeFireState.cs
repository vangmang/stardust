﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class PlasmaChargeFireState : State<PlasmaCharge> {

    public override Enum GetState
    {
        get
        {
            return PlasmaCharge.PlasmaCharge_State.fire;
        }
    }

    //private float currentVelocity;

    public override void Enter(params object[] o_Params)
    {
        Vector3 direction = instance.ModuleInstance.ModuleTransform.up;
        float deg = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        var wantedRot = Quaternion.Euler(0f, 0f, deg);
        instance.projectileTransform.localRotation = wantedRot;
        instance.projectileTransform.localPosition = instance.ModuleInstance.ModuleTransform.position;
        instance.projectileTransform.gameObject.SetActive(true);
        StartCoroutine(instance.DestroyProjectile());
        //currentVelocity = 0f;
    }

    public override void Execute()
    {
        //currentVelocity = Mathf.Lerp(currentVelocity, instance.GetVelocity, instance.GetVelocity * 0.002f * Time.deltaTime);
        instance.projectileTransform.localPosition += instance.projectileTransform.up * instance.GetVelocity * Time.deltaTime;
    }

    public override void Exit()
    {
    }
}
