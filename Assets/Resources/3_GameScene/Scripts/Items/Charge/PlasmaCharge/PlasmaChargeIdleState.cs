﻿using System;
using GameStateLibrary.State;
using UnityEngine;
using System.Collections;

public class PlasmaChargeIdleState : State<PlasmaCharge> {

    public override Enum GetState
    {
        get
        {
            return PlasmaCharge.PlasmaCharge_State.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        StartCoroutine(WaitParticle());
    }
    private IEnumerator WaitParticle()
    {
        yield return new WaitUntil(() => !instance.ChargeEffect.ImpactParticle.isPlaying);
        instance.projectileTransform.gameObject.SetActive(false);
    }
    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
