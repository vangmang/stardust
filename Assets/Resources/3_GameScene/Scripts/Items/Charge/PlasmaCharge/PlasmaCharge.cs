﻿using System;
using System.Collections;
using UnityEngine;

public class PlasmaCharge : ChargeBase<PlasmaCharge, PlasmaModule> {

    public enum PlasmaCharge_State
    {
        enter,
        idle,
        fire
    }

    public override PlasmaCharge Instance
    {
        get
        {
            return this;
        }
    }

    [SerializeField]
    private PlasmaModule plasmaModule;

    public override Charges GetChargeType { get { return Charges.plasma; } }
    public override PlasmaModule ModuleInstance { get { return plasmaModule; } }
    public PlasmaModule setModuleInstance { set { plasmaModule = value; } }
    void Awake()
    {
        InitializeState();
    }

    // Use this for initialization
    void Start () {
        damage += plasmaModule.GetDamage;
        velocity += plasmaModule.GetObjectVelocity;
        StateEnter(PlasmaCharge_State.enter);
	}

    public override IEnumerator DestroyProjectile()
    {
        yield return new WaitForSeconds(DestroyTime);
        ChangeState(PlasmaCharge_State.idle);
        projectileTransform.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
        Execute();
	}

    void OnDestroy()
    {
        
    }
}
