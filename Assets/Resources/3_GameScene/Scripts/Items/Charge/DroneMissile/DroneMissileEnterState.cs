﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class DroneMissileEnterState : State<DroneMissileStateMng>
{
    public override Enum GetState
    {
        get
        {
            return DroneMissileStateMng.DroneMissile_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.ChangeState(DroneMissileStateMng.DroneMissile_State.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
