﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

public class DroneMissileFireState : State<DroneMissileStateMng>
{
    public override Enum GetState
    {
        get
        {
            return DroneMissileStateMng.DroneMissile_State.fire;
        }
    }

    private Vector3 destination;
    private bool isHomingEnable;

    public override void Enter(params object[] o_Params)
    {
        isHomingEnable = false;
        instance.MissileTransform.localPosition = instance.MissileBase.position;
        instance.MissileTransform.localRotation = instance.MissileBase.rotation;
        instance.currentVelocity = 0f;
        float v1 = instance.GetDroneStateMng.Target.currentShipSpeed;
        float v2 = instance.GetVelocity;
        float u1 = (instance.GetDroneStateMng.Target.ShipInfo.ShipTransform.localPosition - instance.MissileBase.position).magnitude;
        float h = u1 / v2;
        float u2 = h * v1;
        destination = instance.GetDroneStateMng.Target.ShipInfo.ShipTransform.localPosition + instance.GetDroneStateMng.Target.ShipInfo.ShipTransform.up * u2 * 0.66f;
        instance.MissileTransform.gameObject.SetActive(true);
        StartCoroutine(HomingEnable());
        StartCoroutine(DestroyMissile());
    }

    public IEnumerator DestroyMissile()
    {
        yield return new WaitForSeconds(instance.DestroyTime);
        instance.ChangeState(DroneMissileStateMng.DroneMissile_State.idle);
        instance.MissileTransform.gameObject.SetActive(false);
    }

    private IEnumerator HomingEnable()
    {
        yield return new WaitForSeconds(0.1f);
        isHomingEnable = true;
        StartCoroutine(Impact());
        Vector3 direction = (destination - instance.MissileTransform.localPosition).normalized;
        float rot = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        var start = instance.MissileTransform.localRotation;
        float t = 0f;
        while(t <= 1f)
        {
            t += instance.GetRotAccel * Time.deltaTime;
            var wantedRot = Quaternion.Euler(0f, 0f, rot);
            instance.MissileTransform.localRotation = 
                Quaternion.Slerp(start, wantedRot, t);
            yield return null;
        }
    }

    private IEnumerator Impact()
    {
        float t = 0f;
        Vector3 start = instance.MissileBase.position;
        while (t <= 1f)
        {
            t += (instance.GetVelocity * instance.GetSpeedAccel) * 0.0015f * Time.deltaTime;
            instance.MissileTransform.localPosition = Vector3.Lerp(start, destination, t);
            yield return null;
        }
    }

    public override void Execute()
    {
        if(!isHomingEnable)
            instance.MissileTransform.localPosition += instance.MissileTransform.up * instance.GetVelocity * Time.deltaTime;
    }

    public override void Exit()
    {
    }
}
