﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class DroneMissileIdleState : State<DroneMissileStateMng>
{
    public override Enum GetState
    {
        get
        {
            return DroneMissileStateMng.DroneMissile_State.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
