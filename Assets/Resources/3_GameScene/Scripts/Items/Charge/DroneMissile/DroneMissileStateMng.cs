﻿using GameStateLibrary.StateMng;
using UnityEngine;

// ChargeBase와 별개인 드론 전용 미사일
public class DroneMissileStateMng : StateMng
{
    public enum DroneMissile_State
    {
        enter,
        idle,
        fire
    }

    [System.Serializable]
    public struct _MissileEffect_
    {
        [SerializeField]
        private ParticleSystem impactParticle;
        public ParticleSystem ImpactParticle { get { return impactParticle; } }
    }
    public _MissileEffect_ MissileEffect;

    public Transform MissileBase;
    public Transform MissileTransform;
    public float currentVelocity;
    public float DestroyTime;

    [SerializeField]
    private DroneStateMng droneStateMng;
    [SerializeField]
    private float velocity;
    [SerializeField]
    private float rotAccel;
    [SerializeField]
    private float speedAccel;
    [SerializeField]
    private float damage;

    public DroneStateMng GetDroneStateMng { get { return droneStateMng; } }
    public float GetVelocity { get { return velocity; } }
    public float GetRotAccel { get { return rotAccel; } }
    public float GetSpeedAccel { get { return speedAccel; } }
    public float GetDamage { get { return damage; } }


    void Awake()
    {
        DestroyTime = droneStateMng.GetCycle * 0.9f;
        InitializeState();
    }

    // Use this for initialization
    void Start()
    {
        StateEnter(DroneMissile_State.enter);
    }

    // Update is called once per frame
    void Update()
    {
        Execute();
    }

    void OnDestroy()
    {

    }
}
