﻿using GameStateLibrary.StateMng;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public abstract class ChargeBase : StateMng, ICharges
{
    public abstract Charges GetChargeType { get; }

    [System.Serializable]
    public struct _ChargeEffect_
    {
        [SerializeField]
        private ParticleSystem impactParticle;
        public ParticleSystem ImpactParticle { get { return impactParticle; } }
    }
    public _ChargeEffect_ ChargeEffect;

    public Transform projectileTransform;
    [SerializeField]
    protected float volume;
    [SerializeField]
    protected float weight;
    [SerializeField]
    protected float damage;
    [SerializeField]
    protected float velocity;
    [SerializeField]
    protected float effectiveRange;
    public float DestroyTime;

    public IItem ModuleInvenInstance { get { return this; } }
    public float GetVolume { get { return volume; } }
    public float GetWeight { get { return weight; } }
    public float GetInventoryVolume { get { return volume; } }
    public float GetInventoryWeight { get { return weight; } }
    public float GetDamage { get { return damage; } }
    public float GetVelocity { get { return velocity; } }
    public float GetEffectiveRange { get { return effectiveRange; } }
    public Transform GetChargeTransform { get { return projectileTransform; } }

    public abstract IEnumerator DestroyProjectile();
}

public abstract class ChargeBase<T, U> : ChargeBase, ICharges<T, U>
    where T : class
    where U : IModule
{
    public abstract T Instance { get; }
    public abstract U ModuleInstance { get; }
}
