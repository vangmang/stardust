﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class HybridChargeEnterState : State<HybridCharge>
{

    public override Enum GetState
    {
        get
        {
            return HybridCharge.HybridCharge_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.ChangeState(HybridCharge.HybridCharge_State.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
