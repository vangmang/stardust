﻿using System;
using System.Collections;
using UnityEngine;

public class HybridCharge : ChargeBase<HybridCharge, HybridModule> {

    public enum HybridCharge_State
    {
        enter,
        idle,
        fire,
        increase_fire,
    }

    public override HybridCharge Instance
    {
        get
        {
            return this;
        }
    }

    [SerializeField]
    private HybridModule hybridModule;

    public override Charges GetChargeType { get { return Charges.hybrid; } }
    public override HybridModule ModuleInstance { get { return hybridModule; } }
    public HybridModule setModuleInstance { set { hybridModule = value; } }

    void Awake()
    {
        InitializeState();
    }

    // Use this for initialization
    void Start () {
        StateEnter(HybridCharge_State.enter);
	}

    public override IEnumerator DestroyProjectile()
    {
        yield return new WaitForSeconds(DestroyTime);
        ChangeState(HybridCharge_State.idle);
        projectileTransform.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
        Execute();
	}

    void OnDestroy()
    {

    }
}
