﻿using System;
using System.Collections.Generic;
using GameStateLibrary.State;
using UnityEngine;

public class HybridChargeFireState : State<HybridCharge> {

    public override Enum GetState
    {
        get
        {
            return HybridCharge.HybridCharge_State.fire;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        Vector3 direction = instance.ModuleInstance.ModuleTransform.up;
        float deg = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        var wantedRot = Quaternion.Euler(0f, 0f, deg);
        instance.projectileTransform.localRotation = wantedRot;
        Vector3 wantedPos = instance.ModuleInstance.ModuleTransform.position;
        wantedPos.z = -1f;
        instance.projectileTransform.localPosition = wantedPos;        
        instance.projectileTransform.gameObject.SetActive(true);
        StartCoroutine(instance.DestroyProjectile());
    }

    public override void Execute()
    {
        instance.projectileTransform.localPosition += instance.projectileTransform.up * instance.GetVelocity * Time.deltaTime;
    }

    public override void Exit()
    {
    }
}
