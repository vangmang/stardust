﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class HybridChargeIncreaseFireState : State<HybridCharge> {

    public override Enum GetState
    {
        get
        {
            return HybridCharge.HybridCharge_State.increase_fire;
        }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
