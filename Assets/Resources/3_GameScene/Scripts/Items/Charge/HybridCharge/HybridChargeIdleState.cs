﻿using System;
using GameStateLibrary.State;
using UnityEngine;

public class HybridChargeIdleState : State<HybridCharge> {

    public override Enum GetState
    {
        get
        {
            return HybridCharge.HybridCharge_State.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.projectileTransform.gameObject.SetActive(false);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
