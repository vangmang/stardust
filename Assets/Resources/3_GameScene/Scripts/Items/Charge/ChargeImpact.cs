﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeImpact : MonoBehaviour
{
    [SerializeField]
    private ChargeBase chargeBase;
    private System.Enum state;
    private CharShipStateMng charShipStateMng;
    bool isModuleFittedOnChar;
    string Tag;

    void Start()
    {
        switch (chargeBase.GetChargeType)
        {
            case Charges.projectile:
                state = ProjectileCharge.ProjectileCharge_State.idle;
                charShipStateMng = ((ChargeBase<ProjectileCharge, ProjectileModule>)chargeBase).ModuleInstance.ShipInstance as CharShipStateMng;
                break;
            case Charges.plasma:
                state = PlasmaCharge.PlasmaCharge_State.idle;
                charShipStateMng = ((ChargeBase<PlasmaCharge, PlasmaModule>)chargeBase).ModuleInstance.ShipInstance as CharShipStateMng;
                break;
            case Charges.hybrid:
                state = HybridCharge.HybridCharge_State.idle;
                charShipStateMng = ((ChargeBase<HybridCharge, HybridModule>)chargeBase).ModuleInstance.ShipInstance as CharShipStateMng;
                break;
        }
        isModuleFittedOnChar = charShipStateMng;
        // 발사체의 모듈이 캐릭터 모듈인지 아닌지 확인 후 트리거할 태그를 선택해준다.
        Tag = isModuleFittedOnChar ? "Enemy" : "Player";
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals(Tag))
        {
            // TODO: 리플렉션 구글링 해볼것
            other.gameObject.SendMessage("TakeDamage", chargeBase, SendMessageOptions.DontRequireReceiver);
            StartCoroutine(waitForFixedUpdate());
        }
    }
    private IEnumerator waitForFixedUpdate()
    {
        yield return new WaitForFixedUpdate();
        if(chargeBase.ChargeEffect.ImpactParticle)
            chargeBase.ChargeEffect.ImpactParticle.Play();
        chargeBase.ChangeState(state);
    }
}
