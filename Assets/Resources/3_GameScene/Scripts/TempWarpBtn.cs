﻿//using System;
using UnityEngine.EventSystems;
using UnityEngine;

public sealed class TempWarpBtn : MonoBehaviour, IPointerClickHandler {

    public CharShipStateMng charShipStateMng;

    public void OnPointerClick(PointerEventData eventData)
    {
        charShipStateMng.ActivateShipWarp();
        charShipStateMng.ChangeState(CharShipStateMng.CharShip_State.warp, 
            new Vector3(Random.Range(-100000f, 100000f), Random.Range(-100000f, 100000f)));
    }
}
