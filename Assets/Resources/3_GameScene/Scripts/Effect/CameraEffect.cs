﻿using UnityEngine;
using System.Collections;

public sealed class CameraEffect : MonoBehaviour
{
    public Transform setCameraBase;

    public float setKnockback;
    public float setKnockbackDistance;
    public float setAmplitude;
    public float setPower;


    public static Transform cameraBase;
    static float knockback;
    static float amplitude;
    static float knockbackDistance;
    static float power;

    static CameraEffect instance = null;
    static CameraEffect Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(CameraEffect)) as CameraEffect;

            return instance;
        }
    }

    void Awake()
    {
        cameraBase = setCameraBase;
    }

    void Update()
    {
        knockback = Mathf.Max(0.01f, setKnockback);
        knockbackDistance = Mathf.Max(0.01f, setKnockbackDistance);
        amplitude = Mathf.Max(0.01f, setAmplitude);
        power = Mathf.Clamp(setPower, 0.01f, 1f);
    }

    public static void DirectCameraAsForce(Vector3 force)
    {
        IEnumerator invokeEffect = DamagedEffect(force);
        Instance.StartCoroutine(invokeEffect);
    }

    public static void DirectCameraAsExplosion()
    {
        IEnumerator invokeEffect = ExplosionEffect(power);
        Instance.StartCoroutine(invokeEffect);
    }

    public static void DirectCameraAsExplosion(float power)
    {
        IEnumerator invokeEffect = ExplosionEffect(power);
        Instance.StartCoroutine(invokeEffect);
    }

    private static IEnumerator DamagedEffect(Vector3 force)
    {
        float t = 1f;
        Vector3 origin = Vector3.zero;
        Vector3 start = Vector3.zero;
        Vector3 cameraForce = force * knockbackDistance;
        float rot = ((Mathf.Atan2(force.y, force.x) * Mathf.Rad2Deg) - 90f) * Mathf.Deg2Rad;
        cameraForce.z = origin.z;
        while (t >= 0.3f)
        {
            t -= knockback * Time.deltaTime;
            cameraBase.localPosition = Vector3.Lerp(origin, -cameraForce, t);
            start = cameraBase.localPosition;
            yield return null;
        }
        while(t <= 1f)
        {
            t += knockback * Time.deltaTime;
            cameraBase.localPosition = Vector3.Lerp(start, origin, t);
            yield return null;
        }
        cameraBase.localPosition = origin;
    }

    private static IEnumerator ExplosionEffect(float power)
    {
        float t = 1f;
        float x = 0f;
        float y = 0f;
        float randomDeg;
        Vector3 origin = Vector3.zero;
        Vector3 start = Vector3.zero;
        Vector3 cameraForce = Vector3.zero;
        while (t >= 0.1f)
        {
            t -= amplitude * Time.deltaTime;
            randomDeg = Random.Range(0f, 360f);
            x = Mathf.Cos(randomDeg) * Mathf.Rad2Deg * power;
            y = Mathf.Sin(randomDeg) * Mathf.Rad2Deg * power;
            cameraForce = new Vector3(x, y);
            cameraBase.localPosition = Vector3.Lerp(origin, cameraForce, t);
            start = cameraBase.localPosition;
            yield return null;
        }

        while (t >= 0f)
        {
            t -= amplitude * Time.deltaTime;
            cameraBase.localPosition = Vector3.Lerp(start, origin, t);
            yield return null;
        }
        cameraBase.localPosition = origin;
    }
}
