﻿using UnityEngine;

public sealed class CameraController : MonoBehaviour {

    public CharShipStateMng charShipStateMng;
    public Transform Target;
    public Transform CameraTransform;
    public float Acceleration;

    //TEST
    public Transform BG;

    float WarpAccel;
    float accel;
	// Use this for initialization
	void Start () {
        WarpAccel = Acceleration * 10f;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        Vector3 wantedPos = Target.position; 

        CameraTransform.localPosition = charShipStateMng.IsShipWarping ? 
            wantedPos : Vector3.Lerp(CameraTransform.localPosition, wantedPos, Acceleration * Time.deltaTime);

        BG.localPosition = wantedPos;
    }

    void OnDestroy()
    {

    }
}
