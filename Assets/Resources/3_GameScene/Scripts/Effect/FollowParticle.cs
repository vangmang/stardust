﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class FollowParticle : MonoBehaviour {

    public CharShipStateMng charShipStateMng;
    public float distance;

	// Update is called once per frame
	void LateUpdate () {
        Vector3 wantedPos = charShipStateMng.ShipInfo.ShipTransform.up * distance;
        wantedPos.z = -20f;
        transform.localPosition = wantedPos; 
	}
}
