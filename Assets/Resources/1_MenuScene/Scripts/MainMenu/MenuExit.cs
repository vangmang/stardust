﻿using GameStateLibrary.State;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine;
using System;

public class MenuExit : State<MenuStateMng>
{
    public override Enum GetState
    {
        get
        {
            return MenuStateMng.MainMenu_State.exit;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.BtnEvent = null;
        instance.Background.transform.localPosition = instance.MainCamera.transform.localPosition;
        instance.Buttons.StarMapBtn.StarMap.StarMapCanvas.gameObject.SetActive(false);
        foreach (ButtonBase btn in instance.BtnBases)
            btn.BtnImage.raycastTarget = false;

        Mng.IsGameStarted = true;
        Mng.Instance.StageInfo.Level = instance.StageInfo.SelectedLevel;
        Mng.Instance.StageInfo.StageName = instance.StageInfo.StageName;
        Mng.Instance.StageInfo.StageDirection = instance.StageInfo.DirOfStageLocation;
        Mng.Instance.StageInfo.StageDistance = instance.StageInfo.DistOfStageLocation;
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeOut()
    {
        float t = 0f;
        float a = 0f;
        float revVal = 1f;
        while (t <= 0.99f)
        {
            revVal = Mathf.Lerp(1f, 0f, t);
            t += instance.FadeSpeed * revVal * Time.deltaTime;
            a = Mathf.Lerp(0f, 1f, t);
            var color = instance.Background.color;
            color = new Color(0f, 0f, 0f, a);
            instance.Background.color = color;
            yield return null;
        }
        SceneManager.LoadScene((int)E_RESOURCES.E_3_GAMESCENE);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }

    void OnDestroy()
    {

    }
}
