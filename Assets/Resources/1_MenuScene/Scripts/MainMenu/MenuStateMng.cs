﻿using System.Collections;
using GameStateLibrary.StateMng;
using UnityEngine;
using UnityEngine.UI;

public class MenuStateMng : StateMng
{

    public enum MainMenu_State
    {
        enter,
        idle,
        exit
    }

    [System.Serializable]
    public struct _Buttons_
    {
        public StarMapBtn StarMapBtn;
        public OptionBtn OptionBtn;
        public CreditBtn CreditBtn;
    }

    [System.Serializable]
    public struct _Popup_
    {

    }

    public struct _StageInfo_
    {
        [System.NonSerialized]
        public int SelectedLevel;
        [System.NonSerialized]
        public string StageName;
        [System.NonSerialized]
        public Transform StageTransform;
        [System.NonSerialized]
        public Vector3 DirOfStageLocation;
        [System.NonSerialized]
        public float DistOfStageLocation;
    }

    public _StageInfo_ StageInfo;

    public _Buttons_ Buttons;
    public _Popup_ Popup;
    public delegate void OnPointerEvent();
    public OnPointerEvent BtnEvent;         // esc버튼 을 이용한 뒤로가기 대리자

    public Image Background;
    public Text IntroText;
    public LensFlare MenuSunLens;
    public float FadeSpeed;
    public float SunLensBrightness;
    public Camera MainCamera;
    public Transform CanvasTransform;

    private readonly float MoveSpeed = 4f;
    public bool IsCameraMoving = true;
    public ParticleSystem MenuParticle;

    [System.NonSerialized]
    public ButtonBase[] BtnBases;
    [System.NonSerialized]
    public bool isSceneShowingStarMap;

    void Awake()
    {
        StageInfo.SelectedLevel = -1;
        InitializeState();
    }

    // Use this for initialization
    void Start()
    {
        BtnBases = CanvasTransform.GetComponentsInChildren<ButtonBase>();
        StateEnter(MainMenu_State.enter);
        BtnEvent = PopupExit;
    }

    // Update is called once per frame
    void Update()
    {
        Execute();
        if (Application.isMobilePlatform)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (BtnEvent != null)
                    BtnEvent();
            }
        }
    }

    public void PopupExit()
    {
    }

    // 카메라 이동
    public IEnumerator MoveCamera(Vector3 destination)
    {
        float t = 0f;
        float rev = 1f;
        Vector3 start = MainCamera.transform.localPosition;
        Vector3 end = destination;
        while (t <= 0.997f)
        {
            rev = Mathf.Lerp(1f, 0f, t);
            IsCameraMoving = true;
            t += MoveSpeed * rev * Time.deltaTime;
            MainCamera.transform.localPosition = Vector3.Lerp(start, end, t);
            yield return null;
        }
        IsCameraMoving = false;
        MainCamera.transform.localPosition = destination;
    }


    void OnDestroy()
    {

    }
}
