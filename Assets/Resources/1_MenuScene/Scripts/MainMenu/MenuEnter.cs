﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;


public class MenuEnter : State<MenuStateMng>
{
    public override Enum GetState
    {
        get
        {
            return MenuStateMng.MainMenu_State.enter;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        SoundMng.Instance.InitSounds();
        SoundMng.Instance.BGM_Enable(SoundMng.isBGM_On);
        SoundMng.Instance.SFX_Enable(SoundMng.isSFX_On);

        if (Mng.IsGameStarted)
        {
            instance.ChangeState(MenuStateMng.MainMenu_State.idle);
            instance.IntroText.color = new Color(1f, 1f, 1f, 0f);
            return;
        }

        instance.MainCamera.orthographicSize = 450f;
        instance.IntroText.color = new Color(1f, 1f, 1f, 0f);
        instance.Background.color = new Color(0f, 0f, 0f, 1f);

        StartCoroutine(fadeIn());
    }

    // 연출 순서: 텍스트 -> 백그라운드 -> 태양 렌즈 -> 메뉴 UI
    private IEnumerator fadeIn()
    {
        float t = 0f;
        float alpha = 0f;
        foreach (ButtonBase btn in instance.BtnBases)
            btn.BtnImage.raycastTarget = false;
        yield return new WaitForSeconds(1f);

        while (t <= 1f) {
            t += instance.FadeSpeed * Time.deltaTime;
            alpha = Mathf.Lerp(0f, 1f, t);
            instance.IntroText.color = new Color(1f, 1f, 1f, alpha);
            yield return null;
        }
        yield return new WaitUntil(() => Input.GetMouseButtonDown(0));
        instance.ChangeState(MenuStateMng.MainMenu_State.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }

    void OnDestroy()
    {

    }
}
