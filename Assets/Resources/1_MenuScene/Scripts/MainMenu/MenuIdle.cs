﻿using GameStateLibrary.State;
using UnityEngine;
using System.Collections;
using System;

public class MenuIdle : State<MenuStateMng>
{

    public override Enum GetState
    {
        get
        {
            return MenuStateMng.MainMenu_State.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        if (Mng.IsGameStarted)
        {
            StartCoroutine(fadeOutWhemGameHasBeenStarted());
            foreach (ButtonBase btn in instance.BtnBases)
                btn.BtnImage.raycastTarget = false;
            instance.IntroText.color = new Color(1f, 1f, 1f, 0f);
            instance.MainCamera.orthographicSize = 400f;
        }
        else
            StartCoroutine(fadeOut());
    }

    private IEnumerator fadeOutWhemGameHasBeenStarted()
    {
        yield return new WaitForFixedUpdate();
        float t = 0f;
        float background = 1f;
        float revValue = 1f;
        while (t <= 0.99f)
        {
            revValue = Mathf.Lerp(1f, 0f, t);
            t += instance.FadeSpeed * revValue * Time.deltaTime;
            background = Mathf.Lerp(1f, 0f, t);
            instance.Background.color = new Color(0f, 0f, 0f, background);
            instance.MenuSunLens.brightness = Mathf.Lerp(0f, instance.SunLensBrightness, t);
            yield return null;
        }
        foreach (ButtonBase btn in instance.BtnBases)
            btn.BtnImage.raycastTarget = true;
    }

    // 연출 순서: 텍스트 -> 백그라운드 -> 태양 렌즈 -> 메뉴 UI
    private IEnumerator fadeOut()
    {
        float t = 0f;
        float alpha = 0f;
        float background = 1f;
        float size = instance.MainCamera.orthographicSize;
        float revValue = 1f;
        while (t <= 0.99f)
        {
            revValue = Mathf.Lerp(1f, 0f, t);
            t += instance.FadeSpeed * revValue * Time.deltaTime;
            alpha = Mathf.Lerp(1f, 0f, t * 1.5f);
            background = Mathf.Lerp(1f, 0f, t);

            instance.IntroText.color = new Color(1f, 1f, 1f, alpha);
            instance.Background.color = new Color(0f, 0f, 0f, background);
            instance.MainCamera.orthographicSize = Mathf.Lerp(size, 400f, t);
            instance.MenuSunLens.brightness = Mathf.Lerp(0f, instance.SunLensBrightness, t);
            yield return null;
        }
        foreach (ButtonBase btn in instance.BtnBases)
            btn.BtnImage.raycastTarget = true;
    }

    public override void Execute()
    {

    }

    public override void Exit()
    {
    }

    void OnDestroy()
    {

    }
}
