﻿using UnityEngine;

public class Lens_FollowingCamera : MonoBehaviour {

    public Transform MainCamera;

	// Update is called once per frame
	void LateUpdate () {
        transform.localPosition = MainCamera.transform.localPosition * 0.975f;
    }

    void OnDestroy()
    {

    }
}
