﻿using UnityEngine.UI;
using UnityEngine;

public class BG_FollowingCamera : MonoBehaviour {

    public Transform MainCamera;
    public RawImage Background;

	// Update is called once per frame
	void LateUpdate () {
        transform.localPosition = MainCamera.transform.localPosition;
        Vector2 rectPos = (Vector2)transform.localPosition * 0.00005f;
        Rect rect = new Rect(rectPos, Vector2.one);
        Background.uvRect = rect;
	}

    void OnDestroy()
    {

    }
}
