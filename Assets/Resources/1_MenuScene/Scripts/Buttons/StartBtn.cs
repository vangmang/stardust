﻿using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

public class StartBtn : ButtonBase, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {

    public override void BtnInit()
    {
        BtnImage.raycastTarget = false;
        var color = BtnImage.color;
        color = new Color(1f, 1f, 1f, 0.5f);
        BtnImage.color = color;
        StartCoroutine(BtnEnable());
    }

    private IEnumerator BtnEnable()
    {
        yield return new WaitUntil(() => menuStateMng.StageInfo.SelectedLevel != -1);
        BtnImage.raycastTarget = true;
        var color = BtnImage.color;
        color = new Color(1f, 1f, 1f, 1f);
        BtnImage.color = color;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //TEST
        if (menuStateMng.StageInfo.SelectedLevel != -1)
            menuStateMng.ChangeState(MenuStateMng.MainMenu_State.exit);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (menuStateMng.StageInfo.SelectedLevel != -1)
        {
            var color = BtnImage.color;
            color = new Color(0.7f, 0.7f, 0.7f, 1f);
            BtnImage.color = color;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (menuStateMng.StageInfo.SelectedLevel != -1)
        {
            var color = BtnImage.color;
            color = new Color(1f, 1f, 1f, 1f);
            BtnImage.color = color;
        }
    }

    void OnDestroy()
    {

    }
}
