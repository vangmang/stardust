﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;
using System;

public abstract class ButtonBase : MonoBehaviour {

    public static event EventHandler BtnClickEvent;
    public MenuStateMng menuStateMng;
    public Image BtnImage;
    public Transform BtnTransform;
    protected IEnumerator cameraMove;

    void Start()
    {
        BtnInit();
    }

    public abstract void BtnInit();    
    protected void InvokeBtnClickEvent()
    {
        if (BtnClickEvent != null)
            BtnClickEvent(this, new EventArgs());
    }
}
