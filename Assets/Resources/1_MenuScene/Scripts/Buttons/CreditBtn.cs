﻿using UnityEngine.EventSystems;
using UnityEngine;

public sealed class CreditBtn : ButtonBase, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Transform CreditAnchor;
    public BackBtn backBtn;

    public override void BtnInit()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!menuStateMng.IsCameraMoving)
        {
            cameraMove = menuStateMng.MoveCamera(CreditAnchor.localPosition);
            StartCoroutine(cameraMove);
            menuStateMng.BtnEvent = backBtn.Click;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        var color = BtnImage.color;
        color = new Color(0.7f, 0.7f, 0.7f, 1f);
        BtnImage.color = color;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        var color = BtnImage.color;
        color = new Color(1f, 1f, 1f, 1f);
        BtnImage.color = color;
    }

    void OnDestroy()
    {

    }
}
