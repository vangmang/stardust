﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class BackBtn : ButtonBase, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Transform BackAnchor;

    public override void BtnInit()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Click();
    }

    public void Click()
    {
        if (!menuStateMng.IsCameraMoving)
        {
            Vector3 pos = BackAnchor ? BackAnchor.localPosition : Vector3.zero;
            cameraMove = menuStateMng.MoveCamera(pos);
            StartCoroutine(cameraMove);
            InvokeBtnClickEvent();
            menuStateMng.BtnEvent = menuStateMng.PopupExit;
            // 처음에는 이벤트로 처리했었는데 게임씬에서 메뉴씬으로 넘어와서는 자꾸 이상하게 오브젝트가
            // 디스트로이 됐다고 떠서 그냥 조건문으로 처리함. 시발...
            if (menuStateMng.isSceneShowingStarMap)
                menuStateMng.Buttons.StarMapBtn.BackBtnClicked();
        }
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        var color = BtnImage.color;
        color = new Color(0.7f, 0.7f, 0.7f, 1f);
        BtnImage.color = color;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        var color = BtnImage.color;
        color = new Color(1f, 1f, 1f, 1f);
        BtnImage.color = color;
    }

    void OnDestroy()
    {

    }
}
