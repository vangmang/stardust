﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public sealed class StarMapBtn : ButtonBase, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    float FadeSpeed;
    [System.Serializable]
    public struct _StarMap_
    {
        public Transform StarMapAnchor;
        public Transform StarMapCanvas;
        [System.NonSerialized]
        public Image[] StarMapImages;
        [System.NonSerialized]
        public RawImage[] StarMapRawImages;
    }

    public event EventHandler StarMapBtnClickEvent;
    public _StarMap_ StarMap;
    public BackBtn backBtn;
    public StartBtn startBtn;

    public override void BtnInit()
    {
        FadeSpeed = 5f;
        StarMap.StarMapImages = StarMap.StarMapCanvas.GetComponentsInChildren<Image>();
        StarMap.StarMapRawImages = StarMap.StarMapCanvas.GetComponentsInChildren<RawImage>();
    }

    public void BackBtnClicked()
    {
        StartCoroutine(InvokeBackBtnClicked());
    }

    private IEnumerator InvokeBackBtnClicked()
    {
        yield return new WaitForFixedUpdate();
        menuStateMng.isSceneShowingStarMap = false;
        StarMap.StarMapCanvas.gameObject.SetActive(false);
        StartCoroutine(disableStarMap());
        foreach (Image image in StarMap.StarMapImages)
            image.raycastTarget = false;
        foreach (RawImage image in StarMap.StarMapRawImages)
            image.raycastTarget = false;
        startBtn.BtnImage.raycastTarget = false;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!menuStateMng.IsCameraMoving)
        {
            menuStateMng.isSceneShowingStarMap = true;
            cameraMove = menuStateMng.MoveCamera(StarMap.StarMapAnchor.localPosition);
            foreach (Image image in StarMap.StarMapImages)
            {
                var alpha = image.color;
                alpha.a = 0f;
                image.color = alpha;
            }
            StartCoroutine(disableSunLens());
            StartCoroutine(cameraMove);
            StartCoroutine(enableStarMap());
            menuStateMng.BtnEvent = backBtn.Click;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        var color = BtnImage.color;
        color = new Color(0.7f, 0.7f, 0.7f, 1f);
        BtnImage.color = color;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        var color = BtnImage.color;
        color = new Color(1f, 1f, 1f, 1f);
        BtnImage.color = color;
    }

    private IEnumerator enableStarMap()
    {
        yield return new WaitUntil(() => !menuStateMng.IsCameraMoving);
        StarMap.StarMapCanvas.gameObject.SetActive(true);
        float t = 0f;
        while (t <= 1f)
        {
            t += FadeSpeed * Time.deltaTime;
            if (!menuStateMng.isSceneShowingStarMap)
                yield break;
            foreach (Image image in StarMap.StarMapImages)
            {
                var alpha = image.color;
                alpha.a = Mathf.Lerp(0f, 1f, t);
                image.color = alpha;
            }
            foreach (RawImage image in StarMap.StarMapRawImages)
            {
                var alpha = image.color;
                alpha.a = Mathf.Lerp(0f, 1f, t);
                image.color = alpha;
            }
            yield return null;
        }
        foreach (Image image in StarMap.StarMapImages)
            image.raycastTarget = true;
        foreach (RawImage image in StarMap.StarMapRawImages)
            image.raycastTarget = true;
        yield return new WaitForFixedUpdate();
        if (StarMapBtnClickEvent != null)
            StarMapBtnClickEvent(this, new EventArgs());
        startBtn.BtnImage.raycastTarget = true;
    }

    private IEnumerator disableSunLens()
    {
        yield return new WaitForFixedUpdate();
        float t = 0f;
        float lensStart = menuStateMng.SunLensBrightness;
        while (t <= 1f)
        {
            t += FadeSpeed * Time.deltaTime;
            menuStateMng.MenuSunLens.brightness = Mathf.Lerp(lensStart, 0f, t);
            yield return null;
        }
    }

    private IEnumerator disableStarMap()
    {
        float t = 0f;
        while (t <= 1f)
        {
            t += menuStateMng.FadeSpeed * Time.deltaTime;
            menuStateMng.MenuSunLens.brightness = Mathf.Lerp(0f, menuStateMng.SunLensBrightness, t);
            yield return null;
        }
    }

    void OnDestroy()
    {

    }
}
