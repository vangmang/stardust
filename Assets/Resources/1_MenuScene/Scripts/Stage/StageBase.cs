﻿using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine;
using System.Text;
using UnityEngine.UI;

public sealed class StageBase : MonoBehaviour, IPointerClickHandler
{
    public StageSelector stageSelector;
    public TextAsset textAsset;
    public StageBase PrevStage;
    public StageBase NextStage;

    public TrailRenderer trail1;
    public TrailRenderer trail2;
    public Image btnImage;
    public Image currentLevelImage;

    [SerializeField]
    private int stageLevel;
    public int GetStageLevel { get { return stageLevel; } }
    [SerializeField]
    private string stageName;
    public string GetStageName { get { return stageName; } }

    private string StageDescription;

    // Use this for initialization
    void Awake()
    {
        stageSelector.starMapBtn.StarMapBtnClickEvent += (sender, e) =>
        {
            if (NextStage)
                if (stageSelector.currentStage + 1 >= NextStage.GetStageLevel)
                    StartCoroutine(EnableWarpableLoad());
        };
    }

    void Start()
    {
        if (stageLevel > stageSelector.currentStage + 1)
            gameObject.SetActive(false);
        StringBuilder sb = new StringBuilder();
        string text = textAsset.text.Split(';')[0];
        sb.Append(text);
        if (NextStage)
        {
            if (stageSelector.currentStage + 1 >= NextStage.GetStageLevel)
                sb.Append(textAsset.text.Split(';')[1]);
        }
        else if (stageSelector.currentStage >= stageLevel)
            sb.Append(textAsset.text.Split(';')[1]);

        if (stageSelector.currentStage == stageLevel)
            currentLevelImage.gameObject.SetActive(true);

        StageDescription = sb.ToString();
    }

    // 스테이지와 스테이지 사이의 트레일렌더러를 활성화 시켜주는 루틴
    private IEnumerator EnableWarpableLoad()
    {
        yield return new WaitForFixedUpdate();
        Vector3 trail1Start = transform.position;
        Vector3 trail2Start = NextStage.transform.position;
        Vector3 end = (transform.position + NextStage.transform.position) * 0.5f;
        Vector3 dir = (transform.position - NextStage.transform.position).normalized;
        dir *= (transform.position - NextStage.transform.position).magnitude * 0.008f;
        trail1Start.z = -10f;
        trail2Start.z = -10f;
        end.z = -10f;

        // 메인 메뉴 화면이 스테이지 선택 화면을 비추고 있을 때
        while (stageSelector.menuStateMng.isSceneShowingStarMap)
        {
            trail1.transform.position = trail1Start;
            trail2.transform.position = trail2Start;
            trail1.transform.gameObject.SetActive(true);
            trail2.transform.gameObject.SetActive(true);
            float t = 0f;
            while (t <= 1f)
            {
                t += 0.75f * Time.deltaTime;
                trail1.transform.position = Vector3.Lerp(trail1Start, end - dir, t);
                trail2.transform.position = Vector3.Lerp(trail2Start, end + dir, t);
                yield return null;
            }
            yield return new WaitForSeconds(1.5f);
            trail1.transform.gameObject.SetActive(false);
            trail2.transform.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        stageSelector.StageExplain.text = StageDescription;
        stageSelector.menuStateMng.StageInfo.SelectedLevel = stageLevel;
        stageSelector.menuStateMng.StageInfo.StageName = stageName;
        stageSelector.menuStateMng.StageInfo.StageTransform = transform;
        // 이전 스테이지와 현재 스테이지의 거리와 방향을 구해서 배정해준다.
        if (PrevStage)
        {
            stageSelector.menuStateMng.StageInfo.DirOfStageLocation = (PrevStage.transform.localPosition - transform.localPosition).normalized;
            stageSelector.menuStateMng.StageInfo.DistOfStageLocation = (PrevStage.transform.localPosition - transform.localPosition).magnitude;
        }
    }
}
