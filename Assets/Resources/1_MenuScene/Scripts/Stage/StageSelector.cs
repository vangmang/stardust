﻿using UnityEngine.UI;
using UnityEngine;
using System;

public sealed class StageSelector : MonoBehaviour
{
    //TEST
    public int currentStage;

    public MenuStateMng menuStateMng;
    public Camera starMapCamera;
    public float Sensitivity;
    public float ZoomSensitivity;
    public RawImage StageBackground;
    public Transform CameraTransform;
    public Text StageExplain;
    public StarMapBtn starMapBtn;

    private float deltaMagnitude;

    private float deltaZoom;
    private float prevDeltaZoom;

    private Vector2 deltaDirection;

    private Vector2 imagePos;
    private int layerMask;
    private bool isZooming;
    Vector3 wantedPos = Vector3.zero;
    // Update is called once per frame
    //TODO: 레이어 마스크로 카메라가 비추는 부분만 스크롤링 처리
    // !< 이상하게 레이어 처리가 안된다. 일단 마스크 처리는 보류
    void Update()
    {
        if (!isZooming)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Moved)
                {
                    deltaDirection = -touch.deltaPosition.normalized;
                    deltaMagnitude = touch.deltaPosition.magnitude;
                    imagePos += deltaDirection.normalized * deltaMagnitude * Sensitivity * 0.0000125f * Time.deltaTime;
                    wantedPos = CameraTransform.localPosition + (Vector3)(deltaDirection * deltaMagnitude * Sensitivity * Time.deltaTime);
                }
            }
        }
        CameraTransform.localPosition = wantedPos;//Vector3.Lerp(CameraTransform.localPosition, wantedPos, (Sensitivity * 0.25f) * Time.deltaTime);
        // 백그라운드
        float t = Sensitivity * 0.125f * Time.deltaTime;
        Rect rect = new Rect(Vector2.Lerp(StageBackground.uvRect.position, imagePos, t), Vector2.one);
        StageBackground.uvRect = rect;

    }

    void LateUpdate()
    {
        if (Input.touchCount == 2)
        {
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);
            deltaZoom = (touch1.position - touch2.position).magnitude;
            prevDeltaZoom = ((touch1.position - touch1.deltaPosition) - (touch2.position - touch2.deltaPosition)).magnitude;
            float zoom = deltaZoom - prevDeltaZoom;
            starMapCamera.orthographicSize -= zoom * ZoomSensitivity * Time.deltaTime;
            starMapCamera.orthographicSize = Mathf.Clamp(starMapCamera.orthographicSize, 200f, 500f);
            isZooming = true;
            return;
        }
        isZooming = false;
    }
}
