﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public sealed class SFXToggleBtn : MonoBehaviour, IPointerClickHandler
{
    public Image OnImage;
    public Image OffImage;
    private static bool isOn = true;

    void Start()
    {
        SoundMng.isSFX_On = isOn;
        OnImage.gameObject.SetActive(isOn);
        OffImage.gameObject.SetActive(!isOn);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        isOn = !isOn;
        SoundMng.isSFX_On = isOn;
        OnImage.gameObject.SetActive(isOn);
        OffImage.gameObject.SetActive(!isOn);
    }

    void OnDestroy()
    {

    }
}
